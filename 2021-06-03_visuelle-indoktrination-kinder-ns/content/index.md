---
title: Visuelle Indoktrination
subtitle: von Kindern im Nationalsozialismus
author: Florian Kohrt
date: 13. Juni 2021
lang: de-DE
transition: none
title-slide-attributes:
  data-audio-src: "media/01.mp4"
  data-notes: "Klicke auf das *Play*-Symbol im unteren Bereich der Seite, um die Wiedergabe zu starten."
---

# RWU-Unterrichtsfilme {data-audio-src="media/02_01.mp4"}

::: columns
:::: {.column .fragment data-audio-src="media/02_02.mp4"}
<video data-audio-controls src="media/RöntgenfilmI.mp4"></video>
Quelle: @Janker1936
::::

:::: {.column .fragment data-audio-src="media/02_03.mp4"}
- Darstellung von Vorgängen
- Werkzeugcharakter
- Ausgangspunkt für Ideologisierung
- filmische Mittel wo nötig, wenig Montage
- "Neutralität" → sachliches Fundament der Ideologie

Quellen: @Niethammer2016, @Imai2013
::::
:::

::: notes
__Formalia__

- RWU = Reichsanstalt für Film und Bild in Wissenschaft und Unterricht
- RWU stellt im NS-Deutschland Unterrichtsfilme für allgemeinbildende Schulen her
- schwarz-weiß und ohne Ton, zw. 5 und 20min lang
- statt Musik/Kommentar
  - Texttafeln in Frakturschrift, die eine bestimmte Auslegung des Films nahelegen
  - manchmal auch Texte direkt im Filmbild
  - manchmal Trickgrafiken, die vom Filmvorführenden erläutert werden müssen
- Nähe zu Kulturfilmen: gleiche Produzent:innen, Altmaterial
- aber nicht nur Bild selbst ist entscheidend
  - Beihefte: Nacherzählung des Filmgeschehens, ergänzende Informationen und Deutungen
  - Schriftenreihe
  - Zeitschriften
  - Besprechung durch Lehrperson

__Filmausschnitt__

- fünfteilige Reihe medizinischer Röntgenfilme
- an Universitätsklinik Bonn nach einem neuen Verfahren gedreht und stellt eine filmtechnische Innovation
- Filme zeigen bewegte Bilder vom Körperinneren, wie das Herz, die Atmung und das Verdauungssystem

__Hintergrund__

- genutzt für Bewegungen und Vorgänge (im Gegensatz zu Ruhenden oder statischen Dingen)
- Werkzeug in der Hand der Lehrenden (daher kein Ton)
- bilden Ausgangspunkt, um an ideologische Themen heranzuführen
- filmische Mittel: Zeitraffer, -lupe, Makroaufnahmen wo nötig, um darzustellen; wenig Montagearbeit
- dadurch Eindruck der Neutralität → Ideologie erscheint sachlich
- Joseph Goebbels (Reichsminister für Volksaufklärung und Propaganda): Kunst als wirkungsmächtige Propaganda soll nicht durch plakativen Einsatz nationalsozialistischer Symbole und Zeichen agieren, sondern möglichst unauffällig sein und durch ihre innere Haltung wirken
- dokumentarisches Abbild der Wirklichkeit vs. "Wirklichkeitsraumgestaltung" (künstlerische, subjektive Sicht, immer Ausschnitte, Produktion von Welten)

__Weiterverwendung__

- nach 1945 werden die Filme zunehmend durch Tonfilme ersetzt
- Weitervertrieb mancher Filme auch nach 1945
  - praktisch alle UF im Vereich der Volkskunde bis in die 1970er-Jahre weitergenutzt
  - Argument: fehlende Tonspur soll für Neutralität sorgen
  - Zensur orientierte sich an der Kategorisierung (G/NPE) und offensichtlichen NS-Symbolen
  - alte Idealbilder und Stereotype konnten so weiterwirken
:::

# Fallbeispiel _Neutralität_ {data-audio-src="media/03_01.mp4"}

::: {.columns align=center}
:::: {.column .fragment width="40%"}
__"Urwaldzwerge in Zentralafrika"__
<video data-audio-controls src="media/Urwaldzwerge.mp4"></video>
Quelle: @Lieberenz1941
::::

:::: {.column .fragment width="40%"}
__Beiheft__
<figure><img src="media/Urwaldzwerge_S06.jpg" alt="„Die im Film gezeigten Ituri-Pygmäen gelten unter den verschiedenen Hauptgruppen als die rassereinsten (Abb. 1). Andere Gruppen haben sich im Laufe der Jahrhunderte mit ihren großwüchsigen Negernachbarn mehr oder weniger vermischt, sind aber auch jetzt noch kultur- und rassemäßig deutlich als Sondergruppe zu erkennen.“" style="width:100%;height:100%;"><img src="media/Urwaldzwerge_S15.jpg" alt="„Man versteht, daß die Flüsse die natürlichen Verkehrsadern des Landes sind. Auf ihnen sind vor Jahrhunderten die Neger, Afrikas Hauptbevölkerungselement, in das alte Wohngebiet der Urwaldzwerge einngedrungen, seit dem 19. Jahrhundert auch in steigendem Maße die Europäer, die jetzigen Herren des Landes.“" style="width:100%;height:100%;"><figcaption>Quelle: @Spannaus1949</figcaption></figure>
::::
:::

[]{.fragment data-audio-src="media/03_02.mp4"}

::: notes
Aufgabe: Guckt den Filmausschnitt an und bildet euch eine Meinung zu seiner Neutralität. (Lest erst anschließend weiter.)

__Inszenierte Neutralität des Films__

- Hierarchie
  - Kamera: folgt dem Blick der Weißen
    - beginnt mit Überflug, dann Expedition der Weißen
    - alternativ: beide Seiten zunächst für sich zeigen
  - Geschichte: Weiße beforschen Schwarze
    - symbolisch durch Notizblock und fehlendes Herstellen der Augenhöhe
  - Interaktion: Weiße werden von Schwarzen gefahren
    - alternativ: Weiße fahren selbst
- Heilsbringer
  - Ankunft der Weißen wird winkend erwartet
  - Weißer verteilt etwas an wartende Schwarze (Medikamente, Essen)
- Kindlichkeit, Wildheit
  - während der Medikamentenverteilung werden die Schwarzen wie Kinder inszeniert
  - High-Angle-Shots nach der erfolgreichen Jagd auf den Elefanten sorgt für wilden und ungeordneten Eindruck
- Quelle: @Annegarn-Glaess2020, S. 116--118

__Offensichtliche Ideologie des Beihefts__

- Rassifizierung
- europäische Dominanz
- Diskussion bereits damals veralteter Theorien
  - Bewohner:innen seien "Kindheitsform der Menschheit", Überreste einer alten Rasseschicht
  - "Degenerationstheorie"
- kolonialistisches Denken

__Anmerkungen__

- nicht der Originalfilm, es wurde mindestens gekürzt
- Heft wurde nach 1945 (!) ausgegeben
:::

# Visuelle Stereotype {data-audio-src="media/04.mp4"}

![Hände fügen Bleistiftminen in Holzhülsen ein](media/WieEinBleistiftEntsteht_SchaffendeHände.png){style="width:24.5%;"} ![Mädchen stehen mit Hitlergruß in gleicher Kleidung in Reihen](media/MädelImLandjahr_Stereotyp.png){style="width:24.5%;"} ![Bäuerliche Großfamilie beim Abendbrot](media/VomKornZumBrot_GemeinsamesMahl.png){style="width:24.5%;"} ![Ein Bauer arbeitet auf einer Wiese](media/MorgenAufEinemSchwarzwälderBauernhof_BlutUndBoden.png){style="width:24.5%;"}

::: columns
:::: column
1. "schaffende Hände"
2. "deutsches Kind"
::::

:::: column
3. gemeinsames Mahl
4. Handwerk / Bauerntum
::::
:::

Quellen: @Niethammer2016, @Bauernhof1938

::: notes
__"schaffende Hände"__

- Großaufnahme der Hände unter Vernachlässigung anderer Körperpartien
- auch sinnbildlich für "gute deutsche Handarbeit"
- kennt man aber auch aus der _Sendung mit der Maus_ oder _Willi wills wissen_

__"deutsches Kind"__

- es wurde darauf geachtet, "dass bestimmte rassisch geprägte Menschen erscheinen, wie sie der deutschen Landschaft etwa des Voralpenlands, des Schwarzwaldes oder Niedersachsen eigentümlich sind"
- Geschlechtsstereotype ("typische" Männer- und Frauenarbeit)
- Beiheft zu "Mädel im Landjahr" (1936) in erster Person Plural geschrieben, was Idenfikation anregt

__gemeinsames Mahl__

- Idealbild von Familie

__Handwerk / Bauerntum__

- ländliche, traditionelle Lebens- und Arbeitsformen
- viele Aufnahmen zeigen alte Handwerksberufe und des Bauernleben
- pittoresk und idealisierend dargestellt
- größtenteils nicht mehr der Lebenspraxis entsprechend
- → "Blut-und-Boden"-Ideologie
:::

# Analyse eines Wandbilds {data-audio-src="media/05_01.mp4"}

[]{.fragment data-audio-src="media/05_02.mp4" data-audio-advance="-1"}

<iframe width="750" height="600" data-src="media/wandbildanalyse.html" frameborder="0" allow="autoplay; clipboard-write; picture-in-picture" allowfullscreen></iframe>

Quelle: @Vaupel2018

::: notes
- Knochen waren im 19. und frühen 20. Jahrhundert ein unverzichtbarer, größtenteils importabhängiger Rohstoff
- im Zuge der Kriegsvorbereitungen wollte man unabhängiger vom Ausland werden
- die Autarkiepolitik sollte auch im Schulunterricht thematisiert werden
- dafür wurden Wandtafeln als Lehrmittel erstellt
- im Folgenden wird eine Wandtafel analysiert, die der Reichskommissar für Altmaterialverwertung 1938 veröffentlicht hat
:::

# Literaturangaben {.allowframebreaks data-audio-src="media/06.mp4"}

::: {#refs style="text-align:left;height:17.5em;overflow-y:scroll;padding-left: 1.5em;"}
:::

# Zusammenfassung

::: columns
:::: column
__Unterrichtsfilme__

- scheinbar neutrale Gestaltung
- weder bloße Vereinnahmung der Zuschauer:innen für Ideologie noch dokumentarisches Abbild der Wirklichkeit
- können Ausgangspunkt bieten, um an ideologische Themen heranzuführen
- durch neutrale Präsentation der Dingwelt  erscheint die Ideologie als etwas Sachliches
- Weitervertrieb nach 1945: alte Idealbilder und Stereotype konnten weiterwirken
::::

:::: column
__Lehrtafeln__

- Bedarf an Seife und Glycerin sollte durch Knochenverarbeitung gedeckt werden
- Schüler:innen wurden daher aufgefordert, Knochen zu sammeln
- durch Vereinfachungen, Beschönigungen, Auslassungen und Falschdarstellungen entlarven sich die dafür konzipierten Lehrtafeln als Propagandamittel
::::
:::

# Klausurfragen

Bezogen auf Unterrichtsfilme, die im nationalsozialistischen Deutschland produziert wurden:

- Wie lässt sich die Indoktrination von Kindern durch Unterrichtsfilme charakterisieren?
- Erläutere die mögliche Indoktrination durch Unterrichtsfilme anhand eines Beispiels.
- Welche visuellen Stereotype lassen sich in den Unterrichtsfilmen ausmachen?
