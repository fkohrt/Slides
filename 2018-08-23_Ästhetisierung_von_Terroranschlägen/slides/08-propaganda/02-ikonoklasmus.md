### Aufnahmen von Kulturzerstörungen
<div class="text-vid-left">
    <ul class="vid-annotation">
        <li class="fragment">Schwarzweiß</li>
        <li class="fragment">Zeitlupe</li>
        <li class="fragment">Zeitraffer-Effekte</li>
        <li class="fragment">Ton-Effekte</li>
        <li class="fragment">Naschid-Untermalung</li>
        <li class="fragment">Funktion: Stärke und Erfolg demonstrieren</li>
    </ul>
</div>
<div class="text-vid-right">
    <video type="video/mp4" data-src="media/Ikonoklasmus_Kloster_Mar_Elian.mp4" controls="controls" playsinline="playsinline"></video><br>
    <div class="footnote">Zywietz (2016); Video: <i>Die Zerstörung des heidnischen Tempels in der Stadt al-Qaraytayn</i>, <a class="rawURL" href="https://clarionproject.org/islamic-state-release-video-monastery-destruction/">clarionproject.org</a>.</div>
</div>

Note:
- Aufnahmen von Kulturzerstörungen: zunächst weniger inszeniert und ästhetisiert
- volle propagandistische Instrumentalisierung erst nach westlichen Reaktionen

**Beispiel**
- März 2015: Meldungen des irakischen Ministeriums für Tourismus und Antike über Zerstörungen in Hatra (Irak)
- Juni 2015: Zerstörung des Klosters Mar Elian in Syrien
- September 2015: Veröffentlichung eines Videos
