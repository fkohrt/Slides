### Militär-Videospiel-Optik
<div class="text-vid-left">
    <ul class="vid-annotation">
        <li class="fragment"><mark>animierte Einblendungen</mark>, die Rasterungen, Ziel- bzw. Abseh- und andere Maßmarkierungen auf Okular- oder Objektivebene simulieren</li>
        <li class="fragment"><mark>Computergeräusche</mark>, die Umschalten oder Kanalwechsel suggerieren</li>
        <li class="fragment"><mark>leichtes Rauschen und ein wiederkehrendes brummendes Summen</mark>, das wie ein Antriebs- oder Telemetriemodul distante Höhe und (Flug-)Bewegung assoziiert</li>
        <li class="fragment">Imitation eines springenden, abtastenden Satelliten- oder Drohnenblicks</li>
        <li class="fragment">Funktion: Verunsichern, Furcht verbreiten</li>
    </ul>
</div>
<div class="text-vid-right">
    <video type="video/mp4" data-src="media/KillThemWhereverYouFindThem.mp4" controls="controls" playsinline="playsinline">Bearbeiteter Ausschnitt aus IS-Propaganda-Video</video><br>
    <div class="footnote">Zywietz (2018); Video: <i>Bearbeiteter Ausschnitt aus IS-Propaganda-Video</i>, ebd., <a class="rawURL" href="https://vimeo.com/265874403">vimeo.com/265874403</a>.</div>
</div>