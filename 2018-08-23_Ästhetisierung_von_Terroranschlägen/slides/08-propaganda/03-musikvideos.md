### Musikvideos
<div class="text-vid-left">
    <ul class="vid-annotation">
        <li>Animationstechnik: Kinetic Typography</li>
        <li>Kriegs- und Kämpferbilder, Waffen und Posen: ausschmückend, martialische Stimmung</li>
        <li>Serialität: Markenkonsistenz<ul>
            <li>Logos</li>
            <li>Fonts</li>
            </ul></li>
    </ul>
</div>
<div class="text-vid-right">
    <video type="video/mp4" data-src="media/Oh_Soldiers_of_Truth_Go_Forth.mp4" controls="controls" playsinline="playsinline"></video><br>
    <div class="footnote">Zywietz (2016); Video: <i>Oh Soldiers of Truth Go Forth</i>, <a class="rawURL" href="https://jihadology.net/2014/06/02/al-%E1%B8%A5ayat-media-center-presents-a-new-video-message-from-the-islamic-state-of-iraq-and-al-sham-oh-soldiers-of-truth-go-forth/">jihadology.net</a>.</div>
</div>

Note:
Ansatzpunkte einer Ästhetisierung
- Bildaufbau / -komposition
- Schnitt / Montage (z. B. Schnittrhythmus)
- Farbgebung (etwa über Farbfilter in der Postproduktion)