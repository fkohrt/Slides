### Framing
Frames liefern Bezugssysteme zur (Re-)Konstruktion von Wirklichkeit.
<ul>
    <li class="fragment">Definition von Wirklichkeit (<mark>Problemdefinition</mark>)</li>
    <li class="fragment">Bewertung von dargestellter Wirklichkeit (<mark>Bewertung</mark>)</li>
    <li class="fragment">Erklärungen von Ursachen der dargestellten Wirklichkeit (<mark>Kausalattribution</mark>)</li>
    <li class="fragment">Möglichkeiten zur Bewältigung der dargestellten Wirklichkeit (<mark>Handlungsaufforderung</mark>)</li>
    <li class="fragment">Ausmaß und Effekte der medialen Dramatisierung</li>
</ul>

Note:
Medien bilden die Wirklichkeit nicht ab. **Sie stellen vielmehr Formen oder Rahmen bereit, um die Wirklichkeit zu interpretieren.** Wir sprechen in diesem Zusammenhang von Inszenierung. Wenn Medien kein objektives Abbild der Wirklichkeit liefern, sondern Formen oder Rahmen zur **Konstruktion von Wirklichkeit** anbieten, dann gibt es auch **keinen Referenten, keinen Bezugspunkt, auf den sich die medialen Konstruktionen eindeutig beziehen lassen**. Und wenn es keinen Referenten, kein Kriterium für die medialen Konstruktionen gibt, lassen sich mediale Berichte auch kaum am Wahrheitskriterium messen. Aus konstruktivistischer Sicht lassen sich mediale, aber auch andere soziale und individuelle Konstruktionen über die Welt nicht mehr danach bewerten, ob und inwiefern sie mit einer objektiven Realität übereinstimmen. An die Stelle von Wahrheitskriterien treten die **Nützlichkeit und Passfähigkeit medialer Konstruktionen im Hinblick auf die von sozialen Gemeinschaften oder einzelnen Personen jeweils angestrebten Ziele oder Wertvorstellungen**.