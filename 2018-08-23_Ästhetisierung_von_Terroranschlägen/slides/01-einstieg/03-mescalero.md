### „Buback – ein Nachruf“ (2)
> „Unser Zweck, eine Gesellschaft ohne Terror und Gewalt (wenn auch nicht ohne Aggression und Militanz), […] dieser Zweck heiligt eben nicht jedes Mittel, sondern nur manches. Unser Weg zum Sozialismus (wegen mir: Anarchie) kann nicht mit Leichen gepflastert werden. […] Einen Begriff und eine Praxis zu entfalten von Gewalt/Militanz, die fröhlich sind und den Segen der beteiligten Massen haben, das ist (zum praktischen Ende gewendet) unsere Tagesaufgabe.“

Note:
- Nachdruck durch eine Reihe von deutschen Professoren
- meist Freisprüche oder kleinere Geldstrafen
- aber auch Gefängnis und Suspendierung vom Dienst
- 2001: Verfasser Klaus Hülbrock (Literaturwissenschaftler und Deutschlehrer) gibt sich in der taz zu erkennen