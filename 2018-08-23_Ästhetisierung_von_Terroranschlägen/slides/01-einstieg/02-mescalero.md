### „Buback – ein Nachruf“ (1)
> „Meine unmittelbare Reaktion, meine ‚Betroffenheit‘ nach dem Abschuß von Buback ist schnell geschildert: Ich konnte und wollte (und will) eine klammheimliche Freude nicht verhehlen. Ich habe diesen Typ oft hetzen hören. Ich weiß, daß er bei der Verfolgung, Kriminalisierung, Folterung von Linken eine herausragende Rolle spielte.“

Note:
- Göttinger Mescalero
- 25. April 1977
- Zeitung des AStA der Universität Göttingen
- unterzeichnet mit „Mescalero“
- Strafantrag, Ermittlungsverfahren