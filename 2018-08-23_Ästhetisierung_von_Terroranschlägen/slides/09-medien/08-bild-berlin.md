#### Bild (Berlin)
![Bild Titelseite Berlin](images/Bild_Titelseite_2016-12-21_Angst_Berlin_edit.jpg)

<div class="footnote">Bild-Titelseite vom 21. Dezember 2016, <a class="rawURL" href="https://www.blogrebellen.de/2016/12/21/bild-vs-berliner-morgenpost-angst-vs-besonnenheit/">www.blogrebellen.de</a></div>

Note:
Bild Titelseite Berlin, 21. Dezember 2016

Günter Wallraff

Gonzo-Journalismus: Der Vormarsch des Kalifats (VICE)

- diffuse Atmosphäre von Angst und Schrecken
- Verständnis der Problematik wird verhindert
    - beeinflusst Informationsverarbeitung
    - aktiviert Stereotypisierung
    - aktiviert Vorurteile
    - aktiviert vorschnelle Freund-Feind-Schemata