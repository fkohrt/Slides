#### Bild (Paris, 1)
![Bild Titelseite Paris Samstag](images/Bild_Titelseite_2015-11-14_Terror-Krieg_Paris.jpg)

<div class="footnote">Bild-Titelseite vom 14. November 2015, <a class="rawURL" href="https://www.welt.de/debatte/henryk-m-broder/article157531197/Ich-verstehe-jeden-der-sich-der-Realitaet-verweigert.html">www.welt.de</a></div>

Note:
Bild Titelseite Paris Samstag, 14. November 2015