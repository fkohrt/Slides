### Umgang mit Propagandamaterial
<div class="text-vid-left">
    <ul class="vid-annotation">
        <li>filterbedingte Einfärbung (Gelb)</li>
        <li>Bildschärfereduktion</li>
        <li>erhöhter Kontrast (Detailverlust)</li>
        <li>Einblendung („Propagandavideo“)</li>
        <li>Bildbeschnitt (Ausschnittverkleinerung)</li>
        <li>Vignettierung (Rand- u. Eckabdunkelung)</li>
    </ul>
</div>
<div class="text-vid-right">
    <video type="video/mp4" data-src="media/ARD_Anne_Will_2016-06-02_Einspieler.mp4" controls="controls" playsinline="playsinline">Einspieler aus Anne Will: Islamisten auf dem Vormarsch – Tödliche Gefahr für Deutschland?</video><br>
    <div class="footnote">Zywietz (2018); Video: Einspieler aus <i>Anne Will: Islamisten auf dem Vormarsch – Tödliche Gefahr für Deutschland?</i> (ARD, 2. Juli 2014), ebd., <a class="rawURL" href="https://vimeo.com/265958726">vimeo.com/265958726</a>.</div>
</div>