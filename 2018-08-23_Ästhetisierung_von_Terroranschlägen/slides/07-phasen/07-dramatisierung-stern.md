### Dramatisierung (2)

<iframe src="media/Stern_Philipp_Weber_2016-03-23.html" width="850" height="500" allowfullscreen="true" title="Philipp Weber, Stern, 23. März 2016"></iframe>
<div class="footnote" style="line-height:1.2!important;"><i>Brüssel trauert am Place de la Bourse</i> (Philipp Weber, Stern, 23. März 2016), <a class="rawURL" href="https://youtu.be/qdNLcNYtXBU">youtu.be/qdNLcNYtXBU</a>.</div>