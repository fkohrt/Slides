### Historisierung
<table>
    <tr>
        <th>Film</th><th>Presse</th><th>Buch</th>
    </tr>
    <tr>
        <td>![Der Baader Meinhof Komplex (2008)](images/Der_Baader_Meinhof_Komplex.jpg)</td><td>![Spiegel 35/2017 vom 28. August 2017](images/Spiegel_35_2017-08-28.jpg)</td><td>![Die verlorene Ehre der Katharina Blum von Heinrich Böll](images/Die_verlorene_Ehre_der_Katharina_Blum.jpg)</td>
    </tr>
    <tr>
        <td><div class="footnote"><a class="rawURL" href="https://imdb.com/media/rm3442182656/tt0765432">imdb.com/media/rm3442182656/tt0765432</a></div></td>
        <td><div class="footnote"><a class="rawURL" href="https://magazin.spiegel.de/SP/2017/35/">magazin.spiegel.de/SP/2017/35/</a></div></td>
        <td><div class="footnote"><a class="rawURL" href="https://www.dtv.de/buch/heinrich-boell-die-verlorene-ehre-der-katharina-blum-14605/">www.dtv.de</a></div></td>
    </tr>
</table>

Note:
**Historisierung: Rückbesinnung auf das Thema**
- Jahrestage
- Gedenkfeiern zu Ehren der Opfer und Retter
- Ereignisartefakte mit aufgeladener Bedeutung
- Archivaufnahmen des Ereignisses als historisches Wissen
- nicht Krise selbst hat historisches Bewusstsein geprägt, sondern die apokalyptische Medienbilder