### Ästhetisierung
<div class="text-vid-left">
    <ul class="vid-annotation">
        <li>Emotionalisierung
            <ul>
                <li>implizit: „Chaos, Zerstörung, Tote“, Aufnahmen von Verletzten, Trümmern und Zerstörung</li>
                <li>explizit: „in den Gesichtern: blankes Entsetzen“</li>
            </ul>
        </li>
        <li>Musikbett
            <ul>
                <li>tiefe, wabernde Synthiesounds, die an- und abschwellen, mit viel Hall unterlegt</li>
                <li>in einer höheren Lage klagende Streicherklänge</li>
            </ul>
        </li>
    </ul>
</div>
<div class="text-vid-right">
    <video type="video/mp4" data-src="media/ARD_Brennpunkt_2016-03-22_edit.mp4" controls="controls" playsinline="playsinline">ZDF heute+ vom 22. März 2016</video><br>
    <div class="footnote">Dribbusch (2016); Video: <i>Brennpunkt</i> (ARD, 22. März 2016), <a class="rawURL" href="https://www.daserste.de/information/nachrichten-wetter/brennpunkt/videosextern/terror-in-bruessel-102.html">www.daserste.de</a>, 0′ 23′′ – 1′ 43′′.</div>
</div>

Note:
**Ästhetisierung: Fernsehen verleiht Interpretationsrahmen und bietet mediales Äquivalent zum Geschehen an**
- eigens entworfene Logos ("Terror gegen Amerika", "Krieg gegen den Terror")
- Trailer
- Montagen
- Split-Screen-Verfahren
- mehrere Einzelereignisse, die womöglich zur Krise geführt haben, erscheinen als zusammenhängendes, kontinuierliches Metaereignis
- Einschätzungen von Moderatoren, Korrespondenten und Kommentatoren führen zwangsläufig zu einer voreingenommenen Sicht auf die Dinge und suggerieren: Das Fernsehen hat alles fest im Griff