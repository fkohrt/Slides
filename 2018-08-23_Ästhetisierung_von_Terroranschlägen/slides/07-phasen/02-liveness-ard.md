### Liveness (1)
<div class="text-vid-left">
    <ul class="vid-annotation">
        <li>in Sendung fälschlich gezeigtes Video: Terroranschlag am Flughafen Moskau-Domodedowo 2011</li>
        <li>Personalisierung
            <ul>
                <li>Herausstellen persönlicher Erlebnisse, Erfahrungen oder Empfindungen von Dritten</li>
                <li>Distanz zwischen abstraktem Ereignis und Zuschauern wird gebrochen</li>
                <li>evoziert Betroffenheit und Involviertheit</li>
            </ul>
        </li>
    </ul>
</div>
<div class="text-vid-right">
    <video type="video/mp4" data-src="media/ARD_Tagesschau_2016-03-22_edit.mp4" controls="controls" playsinline="playsinline">ARD Tagesschau vom 22. März 2016, 10:50 Uhr</video><br>
    <div class="footnote">Schönauer (2016); Video: <i>Tagesschau</i> (ARD, 22. März 2016, 10:50 Uhr), <a class="rawURL" href="https://www.tagesschau.de/multimedia/video/video-167649.html">www.tagesschau.de</a>, 1′ 47′′ – 2′ 39′′.</div>
</div>

Note:
**Liveness: unmittelbare zeitliche Nähe der Berichterstattung zum tatsächlichen Ereignis**
- Live-Schalten
- Korrespondenten-Anrufungen
- Experten-Befragungen im Studio
- Marginalisierung anderer Geschehnisse
- eigens produzierte Sonder- und Spezialsendungen
- Zeitlupen und Wiederholungen verstärken Live-Effekt