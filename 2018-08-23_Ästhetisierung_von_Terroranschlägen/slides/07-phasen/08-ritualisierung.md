### Ritualisierung
<table>
    <tr>
        <th>1977</th><th>Jan. 2015</th><th>Nov. 2015</th>
    </tr>
    <tr>
        <td>![Deutschland im Herbst (1978)](images/Deutschland_im_Herbst.jpg)</td><td>![Je suis Charlie](images/Je_suis_Charlie.jpg)</td><td>![Pray for Paris](images/PrayForParis13Novembre.png)</td>
    </tr>
    <tr>
        <td><div class="footnote"><a class="rawURL" href="https://imdb.com/media/rm1053753344/tt0077427">imdb.com/media/rm1053753344/tt0077427</a></div></td>
        <td><div class="footnote"><a class="rawURL" href="https://politeka.net/news/888-v-irane-projdet-mezhdunarodnyj-konkurs-karikatur-na-holokost/">politeka.net</a></div></td>
        <td><div class="footnote"><a class="rawURL" href="https://commons.wikimedia.org/wiki/File:PrayForParis13Novembre.svg?uselang=de">commons.wikimedia.org</a></div></td>
    </tr>
</table>

Note:
**Ritualisierung: Etablierung von Begriffen**
- Schlüsselbilder und -begriffe aus Ereigniskontext herausgelöst
- werden zum Medien-Ritual: anekdotenhafte Zusammenfassungen
- komplizierte Hintergründe auf einzelne Symbole, Etiketten und Phrasen ("Nichts ist mehr, wie es war") verkürzt
- Illusion der Aufklärung