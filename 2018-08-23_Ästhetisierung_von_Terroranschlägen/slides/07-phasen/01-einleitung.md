### Phasen mediatisierter Krisenereignisse

<div class="footnote">Weichert (2003), Frindte und Haußecker (2010)</div>

Note:
- ÖR berichten öfter
- Private dramatisieren mehr
- ereignisbezogene Primärberichterstattung (weniger Aufklärung über Terrorismus und seine Hintergründe)
- Kampf gegen Terrorismus (es dominieren die Maßnahmen, kaum thematisiert werden die Konsequenzen)