### Liveness (2)
<div class="text-vid-left">
    <blockquote class="vid-quote">„Im Pariser Vorort Saint-Denis läuft ein Anti-Terroreinsatz. <mark>Mindestens zwei Menschen starben</mark> – eine Frau soll sich in die Luft gesprengt haben. stern-Reporter Philipp Weber verfolgt den brisanten Polizeieinsatz.“</blockquote>
</div>
<div class="text-vid-right">
    <video type="video/mp4" data-src="media/Stern_Philipp_Weber_2015-11-18_edit.mp4" controls="controls" playsinline="playsinline">Philipp Weber, Stern, 18. November 2015</video><br>
    <div class="footnote"><i>Plötzlich mitten im Anti-Terroreinsatz</i> (Philipp Weber, Stern, 18. November 2015), <a class="rawURL" href="https://www.stern.de/panorama/weltgeschehen/paris-terror--stern-reporter-mittendrin-beim-anti-terroreinsatz-saint-denis-6561662.html">www.stern.de</a>, 0′ 51′′ – 1′ 32′′.</div>
</div>

Note:
Terroranschläge in Paris am 13. November 2015