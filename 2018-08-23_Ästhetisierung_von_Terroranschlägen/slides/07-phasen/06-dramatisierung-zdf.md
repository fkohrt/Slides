### Dramatisierung (1)
<div class="text-vid-left">
    <ul class="vid-annotation">
        <li class="fragment">Sprachliche Mittel
            <ul>
                <li class="fragment">Narrativierung: „Brüssel, vor vier Tagen“</li>
                <li class="fragment">spannungserzeugende Syntax: „mal wieder“ vs. „diesmal“</li>
                <li class="fragment">rhetorische Fragen</li>
                <li class="fragment">Verwendung des Superlativs: „der meistgesuchte Terrorist Europas“</li>
            </ul>
        </li>
        <li class="fragment">Musikbett
            <ul>
                <li class="fragment">verhallender Gong</li>
                <li class="fragment">tiefe, düstere Wabersounds</li>
                <li class="fragment">atmosphärisches Vokalisieren</li>
                <li class="fragment">tiefes Brummen</li>
            </ul>
        </li>
    </ul>
</div>
<div class="text-vid-right">
    <video type="video/mp4" data-src="media/ZDF_heute+_2016-03-22_Christian_von_Rechenberg.mp4" controls="controls" playsinline="playsinline">ZDF heute+ vom 22. März 2016</video><br>
    <div class="footnote" style="line-height:1.2!important;"><i>heute+</i> (Christian von Rechenberg, ZDF, 22. März 2016), <a class="rawURL" href="https://youtu.be/HOjivjtHv8o">youtu.be/HOjivjtHv8o</a>, 6′ 30′′ – 8′ 27′′.</div>
</div>

Note:
**Dramatisierung: Aufbau eines medialen Spannungsbogens**
- kaum mehr Neues zu berichten, weniger wichtige Nebenaspekte werden neue Top-Themen
- schicksalhaften Geschichten von Tätern, Rettern und Opfern
- Etablierung polarisierende Freund-Feind-Bilder
- patriotischen Selbstdarstellungen über Triumph und Niederlage
- archetypischen Erzählungen von Gut und Böse
- offizielle Lesart des Ereignisses