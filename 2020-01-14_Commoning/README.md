# Readme

- `sudo dnf -y install bzip2-devel`
- ``sudo ln -s `find /usr/lib64/ -type f -name "libbz2.so.1*"` /usr/lib64/libbz2.so.1.0``
- in order to be able to build a pdf
  - if chromium is installed: `sudo ln -s /usr/bin/chromium-browser /usr/bin/chrome`
  - otherwise install NPM's [chromium-binary](https://www.npmjs.com/package/chromium-binary): `npm install --save chromium-binary`

## Struktur

- Elinor Ostrom's principles; [Nobel Lecture](https://www.nobelprize.org/prizes/economic-sciences/2009/ostrom/lecture/)
- commons = resources + community + rules
- Grundlage: Freiwilligkeit + kollektive Verfügung
- Bedürfnisorientierung, Inklusionslogik?
- Beispiele!
- common pool resource
  - exclusion problem: exclusion or the control of access of potential users is difficult
  - subtractability problem: each user is capable of subtracting from the welfare ofall other users
