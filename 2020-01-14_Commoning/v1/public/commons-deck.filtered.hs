[HorizontalRule
,Header 1 ("",[],[("layout","columns")]) []
,Div ("",["multi-column-row","multi-column-row-3"],[])
 [Div ("",["grow-1","column","column-1"],[])
  [Div ("",["box","left"],[])
   [Header 2 ("",["left"],[]) []
   ,Div ("",[],[])
    [Plain [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/marx.jpg\" alt=\"Karl Marx (1875)\" title=\"fig:\">",RawInline (Format "html") "<figcaption>",Str "Karl",Space,Str "Marx",Space,Str "(1875)",RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"]]]]
 ,Div ("",["grow-1","column","column-3"],[])
  [Div ("",["box","right","fragment"],[])
   [Header 2 ("",["right"],[]) []
   ,Div ("",[],[])
    [Plain [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/lenin.jpg\" alt=\"Wladimir Iljitsch Lenin (1920)\" title=\"fig:\">",RawInline (Format "html") "<figcaption>",Str "Wladimir",Space,Str "Iljitsch",Space,Str "Lenin",Space,Str "(1920)",RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"]]]]]
,HorizontalRule
,Header 1 ("",[],[]) [Str "Test"]
,Para [Str "Inhalt"]
,HorizontalRule
,Header 1 ("",[],[("layout","columns")]) [Str "Tragedy",Space,Str "of",Space,Str "the",Space,Str "Commons"]
,Div ("",["multi-column-row","multi-column-row-3"],[])
 [Div ("",["grow-1","column","column-1"],[])
  [Div ("",["box","left","fragment"],[])
   [Header 2 ("",["left"],[]) []
   ,Para [RawInline (Format "html") "<img data-src=\"img/tragedy1.png\">"]]]
 ,Div ("",["grow-1","column","column-2"],[])
  [Div ("",["box","center","fragment"],[])
   [Header 2 ("",["center"],[]) []
   ,Para [RawInline (Format "html") "<img data-src=\"img/tragedy2.png\">"]]]
 ,Div ("",["grow-1","column","column-3"],[])
  [Div ("",["box","right","fragment"],[])
   [Header 2 ("",["right"],[]) []
   ,Para [RawInline (Format "html") "<img data-src=\"img/tragedy3.png\">"]]]]
,Div ("",["single-column-row"],[])
 [Div ("",["box","bottom","fragment"],[])
  [Header 2 ("",["bottom"],[]) []
  ,Para [Str "Menschen",Space,Str "kommunizieren!"]
  ,Para [Str "Bild:",Space,Cite [Citation {citationId = "Friedland2012", citationPrefix = [], citationSuffix = [], citationMode = AuthorInText, citationNoteNum = 0, citationHash = 1}] [Str "Friedland",Space,Str "(2012)"],Str ",",Space,Str "S.",Space,Str "263"]]]
,HorizontalRule
,Header 1 ("",[],[("data-background-image","img/contributopia.jpg")]) [Space]
,HorizontalRule
,Header 1 ("",[],[("layout","columns")]) [Str "Commons!"]
,Div ("",["multi-column-row","multi-column-row-3"],[])
 [Div ("",["grow-1","column","column-1"],[])
  [Div ("",["box","left","fragment"],[])
   [Header 2 ("",["left"],[]) [Str "Commons?"]
   ,BulletList
    [[Plain [Str "Ressourcen",Space,Str "/",Space,Str "Produkte,",Space,Str "die",Space,Str "durch",Space,Str "Commoning",Space,Str "erzeugt",Space,Str "werden"]]
    ,[Plain [Str "materiell"]]
    ,[Plain [Str "symbolisch"]]
    ,[Plain [Str "sozial"]]]]
  ,Div ("",["box","notes"],[])
   [Header 2 ("",["notes"],[]) [Str "Beispiele"]
   ,BulletList
    [[Plain [Str "materiell:",Space,Str "Nahrungsmittel,",Space,Str "Land,",Space,Str "Wasser,",Space,Str "\8230"]]
    ,[Plain [Str "symbolisch:",Space,Str "Wissen,",Space,Str "Geschichten,",Space,Str "\8230"]]
    ,[Plain [Str "sozial:",Space,Str "soziale",Space,Str "F\252rsorge",Space,Str "/",Space,Str "Care,",Space,Str "\8230"]]]]]
 ,Div ("",["grow-1","column","column-2"],[])
  [Div ("",["box","center","fragment"],[])
   [Header 2 ("",["center"],[]) [Str "Commoner?"]
   ,BulletList
    [[Plain [Str "Menschen,",Space,Str "die",Space,Str "Commoning",Space,Str "betreiben"]]]]]
 ,Div ("",["grow-1","column","column-3"],[])
  [Div ("",["box","right","fragment"],[])
   [Header 2 ("",["right"],[]) [Str "Commoning?"]
   ,BulletList
    [[Plain [Str "selbstorganisierte",Space,Str "Prozesse",Space,Str "des",Space,Str "gemeinsamen,",Space,Str "bed\252rfnisorientierten",Space,Str "Produzierens,",Space,Str "Verwaltens,",Space,Str "Pflegens",Space,Str "und",Space,Str "Nutzens"]]]]]]
,HorizontalRule
,Header 1 ("",[],[("layout","columns")]) [Str "Prinzipien",Space,Str "des",Space,Str "Commoning"]
,Div ("",["multi-column-row","multi-column-row-3"],[])
 [Div ("",["grow-1","column","column-1"],[])
  [Div ("",["box","left"],[])
   [Header 2 ("",["left"],[]) [Str "Freiwilligkeit"]
   ,BulletList
    [[Plain [Str "wer",Space,Str "kommt,",Space,Str "ist",Space,Str "richtig"]]
    ,[Plain [Str "Bed\252rfnisorientierung"]]]]]
 ,Div ("",["grow-1","column","column-3"],[])
  [Div ("",["box","right"],[])
   [Header 2 ("",["right"],[]) [Str "kollektive",Space,Str "Verf\252gung"]
   ,BulletList
    [[Plain [Str "negativ:",Space,Str "niemand",Space,Str "kann",Space,Str "allgemein",Space,Str "von",Space,Str "der",Space,Str "Verf\252gung",Space,Str "ausgeschlossen",Space,Str "werden"]]]]
  ,Div ("",["box","notes"],[])
   [Header 2 ("",["notes"],[]) [Str "Erl\228uterung"]
   ,BulletList
    [[Plain [Str "Elementarform:",Space,Str "naheliegende",Space,Str "interpersonale",Space,Str "Handlungsweisen,",Space,Str "um",Space,Str "den",Space,Str "eigenen",Space,Str "Lebensunterhalt",Space,Str "zu",Space,Str "sichern."]]
    ,[Plain [Str "Kapitalismus:",Space,Str "Lohnarbeit",Space,Str "oder",Space,Str "Lohnarbeiter",Space,Str "anstellen"]]
    ,[Plain [Str "Systemform:",Space,Str "dominante",Space,Str "transpersonalen",Space,Str "Handlungsstrukturen,",Space,Str "in",Space,Str "und",Space,Str "mit",Space,Str "denen",Space,Str "die",Space,Str "Menschen",Space,Str "ihre",Space,Str "Lebensbedingungen",Space,Str "herstellen"]]
    ,[Plain [Str "Kapitalismus:",Space,Str "Verwertungslogik",Space,Str "und",Space,Str "Staat"]]]]]]
,HorizontalRule
,Header 1 ("",[],[("layout","columns")]) [Cite [Citation {citationId = "Alexander1977", citationPrefix = [], citationSuffix = [], citationMode = AuthorInText, citationNoteNum = 0, citationHash = 2}] [Str "Alexander",Space,Str "(1977)"],Str ":",Space,Str "A",Space,Str "Pattern",Space,Str "Language"]
,Div ("",["multi-column-row","multi-column-row-3"],[])
 [Div ("",["grow-1","column","column-1"],[])
  [Div ("",["box","left","fragment"],[])
   [Header 2 ("",["left"],[]) [Str "Light",Space,Str "on",Space,Str "Two",Space,Str "Sides",Space,Str "of",Space,Str "Every",Space,Str "Room"]
   ,Para [RawInline (Format "html") "<img data-src=\"img/light.jpg\">"]
   ,Para [Str "(S.",Space,Str "746)"]]]
 ,Div ("",["grow-1","column","column-2"],[])
  [Div ("",["box","center","fragment"],[])
   [Header 2 ("",["center"],[]) [Str "Outdoor",Space,Str "Room"]
   ,Para [RawInline (Format "html") "<img data-src=\"img/outdoor-room.jpg\">"]
   ,Para [Str "(S.",Space,Str "764)"]]]
 ,Div ("",["grow-1","column","column-3"],[])
  [Div ("",["box","right","fragment"],[])
   [Header 2 ("",["right"],[]) [Str "Garden",Space,Str "Growing",Space,Str "Wild"]
   ,Para [RawInline (Format "html") "<img data-src=\"img/garden.jpg\">"]
   ,Para [Str "(S.",Space,Str "801)"]]]]
,HorizontalRule
,Header 1 ("",[],[]) [Str "Muster"]
,Div ("",[],[])
 [Plain [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/muster.png\" alt=\"[@Bollier2015]\" title=\"fig:\">",RawInline (Format "html") "<figcaption>",Cite [Citation {citationId = "Bollier2015", citationPrefix = [], citationSuffix = [], citationMode = NormalCitation, citationNoteNum = 0, citationHash = 3}] [Str "(Bollier",Space,Str "&",Space,Str "Helfrich,",Space,Str "2015)"],RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"]]
,HorizontalRule
,Header 1 ("",[],[]) [Str "Triade",Space,Str "des",Space,Str "Commoning"]
,Div ("",[],[])
 [Plain [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/triade.png\" alt=\"[@Helfrich2019]\" title=\"fig:\">",RawInline (Format "html") "<figcaption>",Cite [Citation {citationId = "Helfrich2019", citationPrefix = [], citationSuffix = [], citationMode = NormalCitation, citationNoteNum = 0, citationHash = 4}] [Str "(Helfrich",Space,Str "&",Space,Str "Bollier,",Space,Str "2019)"],RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"]]
,HorizontalRule
,Header 1 ("",[],[("layout","columns")]) [Str "Soziales",Space,Str "Miteianander"]
,Div ("",["multi-column-row","multi-column-row-3"],[])
 [Div ("",["grow-1","column","column-1"],[])
  [Div ("",["box","left","fragment"],[])
   [Header 2 ("",["left"],[]) [Str "Gemeinsame",Space,Str "Absichten",Space,Str "&",Space,Str "Werte",Space,Str "kultivieren"]]]
 ,Div ("",["grow-1","column","column-2"],[])
  [Div ("",["box","center","fragment"],[])
   [Header 2 ("",["center"],[]) [Str "Ohne",Space,Str "Zw\228nge",Space,Str "beitragen"]
   ,BulletList
    [[Plain [Str "Geben",Space,Str "ohne",Space,Str "die",Space,Str "Erwartung,",Space,Str "etwas",Space,Str "zur\252ckzubekommen"]]]]]
 ,Div ("",["grow-1","column","column-3"],[])
  [Div ("",["box","right","fragment"],[])
   [Header 2 ("",["right"],[]) [Str "Eigene",Space,Str "Governance",Space,Str "reflektieren"]
   ,BulletList
    [[Plain [Str "soziale",Space,Str "Dynamiken",Space,Str "reflektieren"]]
    ,[Plain [Quoted DoubleQuote [Str "there",Space,Str "is",Space,Str "no",Space,Str "such",Space,Str "thing",Space,Str "as",Space,Str "a",Space,Str "structureless",Space,Str "group"],Space,Cite [Citation {citationId = "Freeman2013", citationPrefix = [], citationSuffix = [], citationMode = NormalCitation, citationNoteNum = 0, citationHash = 5}] [Str "(Freeman,",Space,Str "2013)"]]]]]]]
,HorizontalRule
,Header 1 ("",[],[("layout","columns")]) [Str "Peer",Space,Str "Governance"]
,Div ("",["multi-column-row","multi-column-row-3"],[])
 [Div ("",["grow-1","column","column-1"],[])
  [Div ("",["box","left","fragment"],[])
   [Header 2 ("",["left"],[]) [Str "Im",Space,Str "Vertrauensraum",Space,Str "transparent",Space,Str "sein"]]]
 ,Div ("",["grow-1","column","column-2"],[])
  [Div ("",["box","center","fragment"],[])
   [Header 2 ("",["center"],[]) [Str "Wissen",Space,Str "gro\223z\252gig",Space,Str "weitergeben"]]]
 ,Div ("",["grow-1","column","column-3"],[])
  [Div ("",["box","right","fragment"],[])
   [Header 2 ("",["right"],[]) [Str "Commons",Space,Str "&",Space,Str "Kommerz",Space,Str "auseinanderhalten"]]]]
,HorizontalRule
,Header 1 ("",[],[("layout","columns")]) [Str "Sorgendes",Space,Str "&",Space,Str "selbstbestimmtes",Space,Str "Wirtschaften"]
,Div ("",["multi-column-row","multi-column-row-3"],[])
 [Div ("",["grow-1","column","column-1"],[])
  [Div ("",["box","left","fragment"],[])
   [Header 2 ("",["left"],[]) [Str "(F\252r-)Sorge",Space,Str "leisten",Space,Str "&",Space,Str "Arbeit",Space,Str "dem",Space,Str "Markt",Space,Str "entziehen"]]]
 ,Div ("",["grow-1","column","column-2"],[])
  [Div ("",["box","center","fragment"],[])
   [Header 2 ("",["center"],[]) [Str "Das",Space,Str "Produktionsrisiko",Space,Str "gemeinsam",Space,Str "tragen"]]]
 ,Div ("",["grow-1","column","column-3"],[])
  [Div ("",["box","right","fragment"],[])
   [Header 2 ("",["right"],[]) [Str "Poolen,",Space,Str "deckeln",Space,Str "&",Space,Str "aufteilen"]]]]
,HorizontalRule
,Header 1 ("",[],[]) [Str "Beispiele"]
,BlockQuote
 [BulletList
  [[Plain [Str "Linux"]]
  ,[Plain [Str "Wikipedia"]]
  ,[Plain [Str "Freir\228ume"]]
  ,[Plain [Str "SoLaWis"]]
  ,[Plain [Str "FabLabs"]]
  ,[Plain [Str "Freifunk"]]
  ,[Plain [Str "Mietsh\228user",Space,Str "Syndikat"]]
  ,[Plain [Str "Artabana"]]]]
,HorizontalRule
,Header 1 ("",[],[("layout","columns")]) [Str "Was",Space,Str "tun?"]
,Div ("",["multi-column-row","multi-column-row-3"],[])
 [Div ("",["grow-1","column","column-1"],[])
  [Div ("",["box","left","fragment"],[])
   [Header 2 ("",["left"],[]) [Str "Wissen",Space,Str "gro\223z\252gig",Space,Str "teilen"]
   ,BulletList
    [[Plain [Str "Freie",Space,Str "Lizenzen",Space,Str "verwenden:",Space,Str "Copyleft",Space,Str "(z.",Space,Str "B.",Space,Str "Creative",Space,Str "Commons)"]]]]]
 ,Div ("",["grow-1","column","column-2"],[])
  [Div ("",["box","center","fragment"],[])
   [Header 2 ("",["center"],[]) [Str "Commons",Space,Str "&",Space,Str "Kommerz",Space,Str "auseinanderhalten"]]]
 ,Div ("",["grow-1","column","column-3"],[])
  [Div ("",["box","right","fragment"],[])
   [Header 2 ("",["right"],[]) [Str "Eigene",Space,Str "Governance",Space,Str "reflektieren"]
   ,BulletList
    [[Plain [Link ("",[],[]) [Str "Progressive",Space,Str "Clock"] ("https://timeoff.intertwinkles.org/","")]]
    ,[Plain [Quoted DoubleQuote [Str "Diskursanalyse"]]]]]]]
,HorizontalRule
,Header 1 ("",[],[("data-background-image","img/commonismus.jpg")]) [Space]
,Para [Link ("",[],[]) [RawInline (Format "html") "<img data-src=\"https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-sa.svg\">"] ("https://creativecommons.org/licenses/by-sa/4.0/","")]
,HorizontalRule
,Header 1 ("",["unnumbered","xx-dense"],[]) [Str "Quellen"]
,Div ("refs",["references"],[])
 [Div ("ref-Alexander1977",[],[])
  [Para [Str "Alexander,",Space,Str "C.",Space,Str "(1977).",Space,Emph [Str "A",Space,Str "Pattern",Space,Str "Language:",Space,Str "Towns,",Space,Str "Buildings,",Space,Str "Construction"],Space,Str "(Center",Space,Str "for",Space,Str "Environmental",Space,Str "Structure",Space,Str "Series).",Space,Str "Oxford",Space,Str "University",Space,Str "Press."]]
 ,Div ("ref-Bollier2015",[],[])
  [Para [Str "Bollier,",Space,Str "D.",Space,Str "&",Space,Str "Helfrich,",Space,Str "S.",Space,Str "(2015).",Space,Emph [Str "Patterns",Space,Str "of",Space,Str "Commoning"],Str ".",Space,Str "Commons",Space,Str "Strategies",Space,Str "Group.",Space,Str "Verf\252gbar",Space,Str "unter:",Space,Link ("",[],[]) [Str "http://patternsofcommoning.org/"] ("http://patternsofcommoning.org/","")]]
 ,Div ("ref-Freeman2013",[],[])
  [Para [Str "Freeman,",Space,Str "J.",Space,Str "(2013).",Space,Str "The",Space,Str "Tyranny",Space,Str "of",Space,Str "Structurelessness.",Space,Emph [Str "WSQ:",Space,Str "Womens",Space,Str "Studies",Space,Str "Quarterly"],Str ",",Space,Emph [Str "41"],Str "(3-4),",Space,Str "231\8211\&246.",Space,Str "Project",Space,Str "Muse.",Space,Str "https://doi.org/",Link ("",[],[]) [Str "10.1353/wsq.2013.0072"] ("https://doi.org/10.1353/wsq.2013.0072","")]]
 ,Div ("ref-Friedland2012",[],[])
  [Para [Str "Friedland,",Space,Str "A.",Space,Str "(2012).",Space,Emph [Str "Environmental",Space,Str "science",Space,Str ":",Space,Str "foundations",Space,Str "and",Space,Str "applications"],Str ".",Space,Str "New",Space,Str "York,",Space,Str "NY",Space,Str "Houndmills,",Space,Str "Basingstoke,",Space,Str "England:",Space,Str "W.H.",Space,Str "Freeman;",Space,Str "Company."]]
 ,Div ("ref-Helfrich2019",[],[])
  [Para [Str "Helfrich,",Space,Str "S.",Space,Str "&",Space,Str "Bollier,",Space,Str "D.",Space,Str "(2019).",Space,Emph [Str "Frei,",Space,Str "fair",Space,Str "und",Space,Str "lebendig",Space,Str "-",Space,Str "Die",Space,Str "Macht",Space,Str "der",Space,Str "Commons"],Str ".",Space,Str "transcript",Space,Str "Verlag.",Space,Str "https://doi.org/",Link ("",[],[]) [Str "10.14361/9783839445303"] ("https://doi.org/10.14361/9783839445303","")]]]]