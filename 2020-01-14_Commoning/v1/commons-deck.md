---
author: Florian Kohrt
date: 9. November 2019
title: Wege zum Commonismus
lang: de
bibliography: commons.bib
csl: deutsche-gesellschaft-fur-psychologie.csl
history: true
slideNumber: true
---

# {layout="columns"}

## {.left}

![Karl Marx (1875)](img/marx.jpg)

## {.right .fragment}

![Wladimir Iljitsch Lenin (1920)](img/lenin.jpg)

# Test

Inhalt

# Tragedy of the Commons {layout="columns"}

## {.left .fragment}

![](img/tragedy1.png)

## {.center .fragment}

![](img/tragedy2.png)

## {.right .fragment}

![](img/tragedy3.png)

## {.bottom .fragment}

Menschen kommunizieren!

Bild: @Friedland2012, S. 263

# ![](img/contributopia.jpg)

# Commons! {layout="columns"}

## Commons? {.left .fragment}

- Ressourcen / Produkte, die durch Commoning erzeugt werden
- materiell
- symbolisch
- sozial

## Beispiele {.notes}
- materiell: Nahrungsmittel, Land, Wasser, ...
- symbolisch: Wissen, Geschichten, ...
- sozial: soziale Fürsorge / Care, ...

## Commoner? {.center .fragment}

- Menschen, die Commoning betreiben

## Commoning? {.right .fragment}

- selbstorganisierte Prozesse des gemeinsamen, bedürfnisorientierten Produzierens, Verwaltens, Pflegens und Nutzens

# Prinzipien des Commoning {layout="columns"}

## Freiwilligkeit {.left}

- wer kommt, ist richtig
- Bedürfnisorientierung

## kollektive Verfügung {.right}

- negativ: niemand kann allgemein von der Verfügung ausgeschlossen werden

## Erläuterung {.notes}

- Elementarform: naheliegende interpersonale Handlungsweisen, um den eigenen Lebensunterhalt zu sichern.
- Kapitalismus: Lohnarbeit oder Lohnarbeiter anstellen
- Systemform: dominante transpersonalen Handlungsstrukturen, in und mit denen die Menschen ihre Lebensbedingungen herstellen
- Kapitalismus: Verwertungslogik und Staat

# @Alexander1977: A Pattern Language {layout="columns"}

## Light on Two Sides of Every Room {.left .fragment}

![](img/light.jpg)

(S. 746)

## Outdoor Room {.center .fragment}

![](img/outdoor-room.jpg)

(S. 764)

## Garden Growing Wild {.right .fragment}

![](img/garden.jpg)

(S. 801)

# Muster

![[@Bollier2015]](img/muster.png)

# Triade des Commoning

![[@Helfrich2019]](img/triade.png)

# Soziales Miteianander {layout="columns"}

## Gemeinsame Absichten & Werte kultivieren {.left .fragment}

## Ohne Zwänge beitragen {.center .fragment}

- Geben ohne die Erwartung, etwas zurückzubekommen

## Eigene Governance reflektieren {.right .fragment}

- soziale Dynamiken reflektieren
- "there is no such thing as a structureless group" [@Freeman2013]

# Peer Governance {layout="columns"}

## Im Vertrauensraum transparent sein {.left .fragment}

## Wissen großzügig weitergeben {.center .fragment}

## Commons & Kommerz auseinanderhalten {.right .fragment}

# Sorgendes & selbstbestimmtes Wirtschaften {layout="columns"}

## (Für-)Sorge leisten & Arbeit dem Markt entziehen {.left .fragment}

## Das Produktionsrisiko gemeinsam tragen {.center .fragment}

## Poolen, deckeln & aufteilen {.right .fragment}

# Beispiele

> - Linux
> - Wikipedia
> - Freiräume
> - SoLaWis
> - FabLabs
> - Freifunk
> - Mietshäuser Syndikat
> - Artabana

# Was tun? {layout="columns"}

## Wissen großzügig teilen {.left .fragment}

- Freie Lizenzen verwenden: Copyleft (z. B. Creative Commons)

## Commons & Kommerz auseinanderhalten {.center .fragment}

## Eigene Governance reflektieren {.right .fragment}

- [Progressive Clock](https://timeoff.intertwinkles.org/)
- "Diskursanalyse"

# ![](img/commonismus.jpg)

[![](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-sa.svg)](https://creativecommons.org/licenses/by-sa/4.0/)

# Quellen {.xx-dense}

