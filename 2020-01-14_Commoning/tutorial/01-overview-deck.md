---
title: Decker Tutorial Overview
history: true
---

# Overview

## [Command line deck](./02-command-line-deck.html)

How to use `decker` on the command line.

## [Header deck](./03-header-deck.html)

An overview of the possible configuration options that can be included in the header of the `*-deck.md` markdown files.

## [Example deck](./04-example-deck.html)

An extensive overview of the possibilities of designing slide decks offered by `decker`.



#

## [Quiz deck](./05-quiz-deck.html)

How to create simple quizzes with different question types for a self-learning scenario.

## [Chart deck](./06-chart-deck.html)

How to include charts using the reveal.js chart plugin.

# General Information

# Folder Structure

- When running, decker creates a `public` folder in the current directory. 
- This folder contains the template and support files needed during presentation time as well as the generated `html` and `pdf` files.


# Technologies

Decker is built using:

- [Haskell](https://www.haskell.org/), [Pandoc](https://pandoc.org/), [Shake](https://shakebuild.com/)
- [reveal.js](https://revealjs.com/#/)
- [LaTeX]()
- [Chrome/Chromium]() (for pdf generation)
