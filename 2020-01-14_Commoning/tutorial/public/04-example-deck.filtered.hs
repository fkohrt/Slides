[HorizontalRule
,Header 1 ("",[],[]) [Str "Navigation"]
,Para [Str "Navigate",Space,Str "this",Space,Str "presentation",Space,Str "with",Space,Str "the",Space,Str "controls",Space,Str "in",Space,Str "the",Space,Str "bottom-right",Space,Str "corner,",Space,Str "your",Space,Str "arrow",Space,Str "keys",Space,Str "or",Space,Str "the",Space,Str "space",Space,Str "bar."]
,Para [Str "Some",Space,Str "explanations",Space,Str "have",Space,Str "examples",Space,Str "on",Space,Str "a",Space,Str "separate",Space,Str "slide.",Space,Str "These",Space,Str "will",Space,Str "be",Space,Str "arranged",Space,Str "below",Space,Str "the",Space,Str "respective",Space,Str "slide",Space,Str "and",Space,Str "will",Space,Str "be",Space,Str "indicated",Space,Str "by",Space,Str "a",Space,Str "down",Space,Str "arrow",Space,Str "in",Space,Str "the",Space,Str "controls.",Space,Str "Use",Space,Str "the",Space,Str "down",Space,Str "arrow",Space,Str "key",Space,Str "to",Space,Str "see",Space,Str "them.",SoftBreak,Str "If",Space,Str "you",Space,Str "use",Space,Str "the",Space,Str "space",Space,Str "bar",Space,Str "to",Space,Str "go",Space,Str "through",Space,Str "the",Space,Str "presentation,",Space,Str "the",Space,Str "examples",Space,Str "will",Space,Str "automatically",Space,Str "follow",Space,Str "their",Space,Str "explanation."]
,Para [Str "The",Space,RawInline (Format "html") "<i class=\"fas fa-bars\">",RawInline (Format "html") "</i>",Space,Str "icon",Space,Str "in",Space,Str "the",Space,Str "bottom-left",Space,Str "corner",Space,Str "opens",Space,Str "a",Space,Str "menu",Space,Str "showing",Space,Str "a",Space,Str "table",Space,Str "of",Space,Str "contents",Space,Str "of",Space,Str "all",Space,Str "slides."]
,HorizontalRule
,Header 1 ("syntax",[],[]) [Str "Markdown",Space,Str "Syntax"]
,Para [Str "The",Space,Str "Decker",Space,Str "Slide",Space,Str "Tool",Space,Str "assists",Space,Str "you",Space,Str "in",Space,Str "creating",Space,Str "media-rich",Space,Str "presentations",Space,Str "with",Space,Str "a",Space,Str "few",Space,Str "easy",Space,Str "to",Space,Str "use",Space,Str "Markdown",Space,Str "commands.",Space,Str "This",Space,Str "user",Space,Str "guide",Space,Str "will",Space,Str "highlight",Space,Str "some",Space,Str "of",Space,Str "the",Space,Str "main",Space,Str "styling",Space,Str "features",Space,Str "of",Space,Str "Decker",Space,Str "and",Space,Str "provide",Space,Str "examples",Space,Str "on",Space,Str "how",Space,Str "to",Space,Str "use",Space,Str "each",Space,Str "feature."]
,Para [Str "Visit",Space,Link ("",[],[]) [Str "http://pandoc.org"] ("http://pandoc.org",""),Space,Str "for",Space,Str "additional",Space,Str "information",Space,Str "on",Space,Str "Pandoc-Markdown",Space,Str "text",Space,Str "formatting."]
,HorizontalRule
,Header 1 ("slides",[],[]) [Str "New",Space,Str "Slides"]
,Para [Str "Heading",Space,Str "1",Space,Str "(h1)",Space,Str "headers",Space,Str "create",Space,Str "new",Space,Str "slides."]
,Div ("",["css-columns"],[])
 [Div ("",["box","split"],[])
  [Header 2 ("",["split"],[]) []
  ,CodeBlock ("",["markdown"],[]) "# Heading 1 (h1) new slide\n## Heading 2 (h2)\n### Heading 3 (h3)\n#### Heading 4 (h4)"]
 ,Div ("",["box"],[])
  [Header 2 ("",[],[]) []]
 ,Div ("",["box"],[])
  [Header 2 ("",[],[]) [Str "Heading",Space,Str "2",Space,Str "(h2)"]
  ,Header 3 ("",[],[]) [Str "Heading",Space,Str "3",Space,Str "(h3)"]
  ,Header 4 ("",[],[]) [Str "Heading",Space,Str "4",Space,Str "(h4)"]]]
,HorizontalRule
,Header 1 ("multicolumn",[],[]) [Str "Multicolumn",Space,Str "Slides"]
,CodeBlock ("",["markdown"],[]) "# W\252rzburg Sehensw\252rdigkeiten {layout=\"columns\"}\n\n## Die Residenz {.left}\nDie W\252rzburger Residenz ist das Hauptwerk des ...\n\n## Alte Mainbr\252cke {.center}\nDiese erste Steinbr\252cke Deutschlands soll bereits um ...\n\n## Dom St. Kilian {.right}\nEin Hauptwerk der deutschen Baukunst zur Zeit der ..."
,HorizontalRule
,Header 1 ("example-multicolumn",["sub"],[("layout","columns")]) [Str "Multicolumn",Space,Str "example"]
,Div ("",["multi-column-row","multi-column-row-3"],[])
 [Div ("",["grow-1","column","column-1"],[])
  [Div ("",["box","left"],[])
   [Header 2 ("",["left"],[]) [Str "Die",Space,Str "Residenz"]
   ,Para [Str "Die",Space,Str "W\252rzburger",Space,Str "Residenz",Space,Str "ist",Space,Str "das",Space,Str "Hauptwerk",Space,Str "des",Space,Str "s\252ddeutschen",Space,Str "Barock."]]]
 ,Div ("",["grow-1","column","column-2"],[])
  [Div ("",["box","center"],[])
   [Header 2 ("",["center"],[]) [Str "Alte",Space,Str "Mainbr\252cke"]
   ,Para [Str "Die",Space,Str "erste",Space,Str "Steinbr\252cke",Space,Str "Deutschlands",Space,Str "soll",Space,Str "bereits",Space,Str "um",Space,Str "1120",Space,Str "errichtet",Space,Str "worden",Space,Str "sein."]]]
 ,Div ("",["grow-1","column","column-3"],[])
  [Div ("",["box","right"],[])
   [Header 2 ("",["right"],[]) [Str "Dom",Space,Str "St.\160Kilian"]
   ,Para [Str "Ein",Space,Str "Hauptwerk",Space,Str "der",Space,Str "deutschen",Space,Str "Baukunst",Space,Str "und",Space,Str "viertgr\246\223te",Space,Str "romanische",Space,Str "Kirche",Space,Str "Deutschlands."]]]]
,HorizontalRule
,Header 1 ("topBottom",[],[]) [Str "Top",Space,Str "and",Space,Str "Bottom"]
,Para [Str "Additionally",Space,Str "use",Space,Str "the",Space,Code ("",[],[]) ".top",Space,Str "and",Space,Code ("",[],[]) ".bottom",Space,Str "tags",Space,Str "can",Space,Str "be",Space,Str "used."]
,CodeBlock ("",["markdown"],[]) "# Top and Bottom Example {layout=\"columns\"}\n\n## Top Colum {.top}\nFirst/top column spans across the following columns.\n\n## Left Column {.left}\n\n## Right Column {.right}\n\n## Third Column {.bottom}\nThird/bottom column spans across the columns above."
,HorizontalRule
,Header 1 ("example-topBottom",["sub"],[("layout","columns")]) [Str "Top",Space,Str "and",Space,Str "Bottom",Space,Str "Example"]
,Div ("",["single-column-row"],[])
 [Div ("",["box","top"],[])
  [Header 2 ("",["top"],[]) [Str "Top",Space,Str "Colum"]
  ,Para [Str "First/top",Space,Str "column",Space,Str "spans",Space,Str "across",Space,Str "the",Space,Str "following",Space,Str "columns."]]]
,Div ("",["multi-column-row","multi-column-row-3"],[])
 [Div ("",["grow-1","column","column-1"],[])
  [Div ("",["box","left"],[])
   [Header 2 ("",["left"],[]) [Str "Left",Space,Str "Column"]]]
 ,Div ("",["grow-1","column","column-3"],[])
  [Div ("",["box","right"],[])
   [Header 2 ("",["right"],[]) [Str "Right",Space,Str "Column"]]]]
,Div ("",["single-column-row"],[])
 [Div ("",["box","bottom"],[])
  [Header 2 ("",["bottom"],[]) [Str "Third",Space,Str "Column"]
  ,Para [Str "Third/bottom",Space,Str "column",Space,Str "spans",Space,Str "across",Space,Str "the",Space,Str "columns",Space,Str "above."]]]
,HorizontalRule
,Header 1 ("verticalSlides",[],[]) [Str "Vertical",Space,Str "Slides"]
,Para [Str "Add",Space,Str "the",Space,Str "{.sub}",Space,Str "tag",Space,Str "to",Space,Str "any",Space,Str "slide",Space,Str "to",Space,Str "place",Space,Str "it",Space,Str "below",Space,Str "the",Space,Str "previous",Space,Str "slide."]
,CodeBlock ("",["txt"],[]) "# Vertical Slide Example {.sub}\n\nThis slide will appear below the previous slide. "
,HorizontalRule
,Header 1 ("",["sub"],[]) [Str "Vertical",Space,Str "Slide",Space,Str "Example"]
,Para [Str "This",Space,Str "slide",Space,Str "will",Space,Str "appear",Space,Str "below",Space,Str "the",Space,Str "previous",Space,Str "slide."]
,HorizontalRule
,Header 1 ("textEmphasis",[],[]) [Str "Text",Space,Str "Emphasis"]
,Para [Str "Format",Space,Str "text",Space,Str "by",Space,Str "surrounding",Space,Str "it",Space,Str "in",Space,Str "appropriate",Space,Str "symbols:"]
,Div ("",["css-columns"],[])
 [Div ("",["box","split"],[])
  [Header 2 ("",["split"],[]) []
  ,CodeBlock ("",["markdown"],[]) "**This is bold text**\n__This is bold text__\n*This is italic text*\n_This is italic text_\n~~Strikethrough~~\n<u>underline</u>\n~subscript~\n^superscript^"]
 ,Div ("",["box"],[])
  [Header 2 ("",[],[]) []
  ,Para [Strong [Str "This",Space,Str "is",Space,Str "bold",Space,Str "text"],LineBreak,Strong [Str "This",Space,Str "is",Space,Str "bold",Space,Str "text"],LineBreak,Emph [Str "This",Space,Str "is",Space,Str "italic",Space,Str "text"],LineBreak,Emph [Str "This",Space,Str "is",Space,Str "italic",Space,Str "text"],LineBreak,Strikeout [Str "Strikethrough"],LineBreak,RawInline (Format "html") "<u>",Str "underline",RawInline (Format "html") "</u>",LineBreak,Str "H",Subscript [Str "2"],Str "O",Space,Str "is",Space,Str "a",Space,Str "liquid.",LineBreak,Str "2",Superscript [Str "3"],Space,Str "equals",Space,Str "8."]]]
,HorizontalRule
,Header 1 ("inverse",["inverse"],[("background-color","black")]) [Str "Inverse",Space,Str "Colors"]
,Div ("",["box"],[])
 [Header 2 ("",[],[]) [Str "Color",Space,Str "Scheme",Space,Str "for",Space,Str "Dark",Space,Str "Images"]
 ,BulletList
  [[Plain [Str "Add",Space,Code ("",[],[]) ".inverse",Space,Str "tag",Space,Str "to",Space,Str "slide",Space,Str "header",Space,Str "(h1)"]]
  ,[Plain [Str "Add",Space,Code ("",[],[]) "background-color=\"black\"",Space,Str "to",Space,Str "slide",Space,Str "header",Space,Str "(h1)"]]]]
,Div ("",["box","fragment","definition"],[])
 [Header 2 ("",["definition"],[]) [Str "Definition",Space,Str "Box"]
 ,Para [Str "Even",Space,Str "colored",Space,Str "boxes",Space,Str "look",Space,Str "ok."]]
,HorizontalRule
,Header 1 ("blocks",[],[]) [Str "Highlight",Space,Str "Blocks"]
,Div ("",["css-columns"],[])
 [Div ("",["box","split"],[("style","font-size:small")])
  [Header 2 ("",["split"],[("style","font-size:small")]) []
  ,CodeBlock ("",["markdown"],[]) "## Alert Block {.alert}\n\n-  Alert Text"]
 ,Div ("",["box"],[("style","font-size:small")])
  [Header 2 ("",[],[("style","font-size:small")]) []
  ,CodeBlock ("",["markdown"],[]) "## Question Block {.question}\n\n-  Question text"]
 ,Div ("",["box"],[("style","font-size:small")])
  [Header 2 ("",[],[("style","font-size:small")]) []
  ,CodeBlock ("",["markdown"],[]) "## Answer Block {.answer}\n\n-  Answer text"]
 ,Div ("",["box"],[("style","font-size:small")])
  [Header 2 ("",[],[("style","font-size:small")]) []
  ,CodeBlock ("",["markdown"],[]) "## Definition Block {.definition}\n\n-  Definition text"]
 ,Div ("",["box"],[("style","font-size:small")])
  [Header 2 ("",[],[("style","font-size:small")]) []
  ,CodeBlock ("",["markdown"],[]) "## Observation Block {.observation}\n\n-  Observation text"]
 ,Div ("",["box"],[("style","font-size:small")])
  [Header 2 ("",[],[("style","font-size:small")]) []
  ,CodeBlock ("",["markdown"],[]) "## Example Block {.example}\n\n-  Example text"]
 ,Div ("",["box"],[("style","font-size:small")])
  [Header 2 ("",[],[("style","font-size:small")]) []
  ,CodeBlock ("",["markdown"],[]) "## Equation Block {.equation}\n\n-  Equation text"]
 ,Div ("",["box"],[("style","font-size:small")])
  [Header 2 ("",[],[("style","font-size:small")]) []
  ,CodeBlock ("",["markdown"],[]) "## Note Block {.note}\n\n-  Note text"]]
,HorizontalRule
,Header 1 ("example-blocks",["sub"],[]) [Str "Highlight",Space,Str "Blocks",Space,Str "example"]
,Div ("",["css-columns"],[])
 [Div ("",["box","alert","split"],[])
  [Header 2 ("",["alert","split"],[]) [Str "Alert",Space,Str "Block"]
  ,BulletList
   [[Plain [Str "Alert",Space,Str "Text"]]]]
 ,Div ("",["box","question"],[])
  [Header 2 ("",["question"],[]) [Str "Question",Space,Str "Block"]
  ,BulletList
   [[Plain [Str "Question",Space,Str "text"]]]]
 ,Div ("",["box","answer"],[])
  [Header 2 ("",["answer"],[]) [Str "Answer",Space,Str "Block"]
  ,BulletList
   [[Plain [Str "Answer",Space,Str "text"]]]]
 ,Div ("",["box","definition"],[])
  [Header 2 ("",["definition"],[]) [Str "Definition",Space,Str "Block"]
  ,BulletList
   [[Plain [Str "Definition",Space,Str "text"]]]]
 ,Div ("",["box","observation"],[])
  [Header 2 ("",["observation"],[]) [Str "Observation",Space,Str "Block"]
  ,BulletList
   [[Plain [Str "Observation",Space,Str "text"]]]]
 ,Div ("",["box","example"],[])
  [Header 2 ("",["example"],[]) [Str "Example",Space,Str "Block"]
  ,BulletList
   [[Plain [Str "Example",Space,Str "text"]]]]
 ,Div ("",["box","equation"],[])
  [Header 2 ("",["equation"],[]) [Str "Equation",Space,Str "Block"]
  ,BulletList
   [[Plain [Str "Equation",Space,Str "text"]]]]
 ,Div ("",["box","note"],[])
  [Header 2 ("",["note"],[]) [Str "Note",Space,Str "Block"]
  ,BulletList
   [[Plain [Str "Note",Space,Str "text"]]]]]
,HorizontalRule
,Header 1 ("lists",[],[]) [Str "Lists"]
,Div ("",["css-columns"],[])
 [Div ("",["box","split"],[])
  [Header 2 ("",["split"],[]) [Str "Ordered",Space,Str "Lists"]
  ,CodeBlock ("",["markdown"],[]) "1.  bread\n2.  milk\n3.  sugar\n4.  flour"]
 ,Div ("",["box","example"],[])
  [Header 2 ("",["example"],[]) []
  ,OrderedList (1,Decimal,Period)
   [[Plain [Str "bread"]]
   ,[Plain [Str "milk"]]
   ,[Plain [Str "sugar"]]
   ,[Plain [Str "flour"]]]]
 ,Div ("",["box"],[])
  [Header 2 ("",[],[]) [Str "Enumerated",Space,Str "Lists"]
  ,CodeBlock ("",["markdown"],[]) "-  Take out trash\n-  Vaccuum\n    - Bedrooms\n-  Wash dishes"]
 ,Div ("",["box","example"],[])
  [Header 2 ("",["example"],[]) []
  ,BulletList
   [[Plain [Str "Take",Space,Str "out",Space,Str "trash"]]
   ,[Plain [Str "Vaccuum"]
    ,BulletList
     [[Plain [Str "Bedrooms"]]]]
   ,[Plain [Str "Wash",Space,Str "dishes"]]]]]
,HorizontalRule
,Header 1 ("",[],[]) [Str "Fragmented",Space,Str "Lists"]
,Para [Str "To",Space,Str "make",Space,Str "list",Space,Str "items",Space,Str "appear",Space,Str "one",Space,Str "after",Space,Str "another,",Space,Str "put",Space,Code ("",[],[]) ">",Space,Str "before",Space,Str "each:"]
,CodeBlock ("",["markdown"],[]) "> - Take out trash\n> - Vacuum\n> - Wash dishes"
,Div ("",["box","example"],[])
 [Header 2 ("",["example"],[]) []
 ,BlockQuote
  [BulletList
   [[Plain [Str "Take",Space,Str "out",Space,Str "trash"]]
   ,[Plain [Str "Vacuum"]]
   ,[Plain [Str "Wash",Space,Str "dishes"]]]]]
,HorizontalRule
,Header 1 ("seqlists",[],[]) [Str "Sequential",Space,Str "Lists"]
,Para [Str "Use",Space,Str "the",Space,Str "(@)",Space,Str "symbol",Space,Str "to",Space,Str "automatically",Space,Str "number",Space,Str "items",Space,Str "in",Space,Str "a",Space,Str "list.",LineBreak,Str "Numbered",Space,Str "examples",Space,Str "do",Space,Str "not",Space,Str "need",Space,Str "to",Space,Str "be",Space,Str "in",Space,Str "a",Space,Str "single",Space,Str "list."]
,Div ("",["box"],[("style","font-size:small;")])
 [Header 2 ("",[],[("style","font-size:small;")]) []
 ,CodeBlock ("",["markdown"],[]) "(@)  Salman Rushdie, *The Ground beneath Her Feet* (New York: Henry Holt, 1999), 25.  \n\n(@)  Bob Stewart, \"Wag of the Tail: Reflecting on Pet Ownership,\" in *Enriching Our\n  Lives with Animals*, ed. John Jaimeson, Tony Bannerman and Selena Wong\n  (Toronto, ON: Petlove Press, 2007),100.  \n\nAdditional sources:  \n\n(@)  Elliot Antokoletz, *Musical Symbolism in the Operas of Debussy and Bartok*\n  (New York: Oxford University Press, 2008),\n  doi:10.1093/acprof:oso/9780195365825.001.0001."]
,HorizontalRule
,Header 1 ("example-seqlists",["sub"],[]) [Str "Sequential",Space,Str "Lists",Space,Str "example"]
,OrderedList (1,Example,TwoParens)
 [[Para [Str "Salman",Space,Str "Rushdie,",Space,Emph [Str "The",Space,Str "Ground",Space,Str "beneath",Space,Str "Her",Space,Str "Feet"],Space,Str "(New",Space,Str "York:",Space,Str "Henry",Space,Str "Holt,",Space,Str "1999),",Space,Str "25."]]
 ,[Para [Str "Bob",Space,Str "Stewart,",Space,Quoted DoubleQuote [Str "Wag",Space,Str "of",Space,Str "the",Space,Str "Tail:",Space,Str "Reflecting",Space,Str "on",Space,Str "Pet",Space,Str "Ownership,"],Space,Str "in",Space,Emph [Str "Enriching",Space,Str "Our",SoftBreak,Str "Lives",Space,Str "with",Space,Str "Animals"],Str ",",Space,Str "ed.",Space,Str "John",Space,Str "Jaimeson,",Space,Str "Tony",Space,Str "Bannerman",Space,Str "and",Space,Str "Selena",Space,Str "Wong",SoftBreak,Str "(Toronto,",Space,Str "ON:",Space,Str "Petlove",Space,Str "Press,",Space,Str "2007),100."]]]
,Para [Str "Additional",Space,Str "sources:"]
,OrderedList (3,Example,TwoParens)
 [[Plain [Str "Elliot",Space,Str "Antokoletz,",Space,Emph [Str "Musical",Space,Str "Symbolism",Space,Str "in",Space,Str "the",Space,Str "Operas",Space,Str "of",Space,Str "Debussy",Space,Str "and",Space,Str "Bartok"],SoftBreak,Str "(New",Space,Str "York:",Space,Str "Oxford",Space,Str "University",Space,Str "Press,",Space,Str "2008),",SoftBreak,Str "doi:10.1093/acprof:oso/9780195365825.001.0001."]]]
,HorizontalRule
,Header 1 ("links",[],[]) [Str "Links"]
,Para [Str "Enter",Space,Str "the",Space,Str "text",Space,Str "to",Space,Str "be",Space,Str "displayed",Space,Str "followed",Space,Str "by",Space,Str "the",Space,Str "URL",Space,Str "or",Space,Str "slide",Space,Str "ID."]
,CodeBlock ("",["markdown"],[]) "[text-to-be-displayed](https://url-of-website)\n[text-to-be-displayed](#slide-id)"
,Para [Emph [Str "Note:"],Space,Str "Slide",Space,Str "IDs",Space,Str "are",Space,Str "entered",Space,Str "on",Space,Str "the",Space,Str "slide",Space,Str "header",Space,Str "(h1)",Space,Str "as",Space,Str "follows:"]
,CodeBlock ("",["markdown"],[]) "# Slide Title {#slide-id}"
,HorizontalRule
,Header 1 ("example-links",["sub"],[]) [Str "Links",Space,Str "example"]
,Div ("",["box"],[("style","font-size:small;")])
 [Header 2 ("",[],[("style","font-size:small;")]) []
 ,CodeBlock ("",["txt"],[]) "Visit [http://pandoc.org](http://pandoc.org) for additional information.\n\nRead more about building [lists](#lists) in Decker."]
,Div ("",["box"],[])
 [Header 2 ("",[],[]) []]
,Div ("",["box","example"],[])
 [Header 2 ("",["example"],[]) []
 ,Para [Str "Visit",Space,Link ("",[],[]) [Str "http://pandoc.org"] ("http://pandoc.org",""),Space,Str "for",Space,Str "additional",Space,Str "information.",LineBreak,Str "Read",Space,Str "more",Space,Str "about",Space,Str "building",Space,Link ("",[],[]) [Str "lists"] ("#lists",""),Space,Str "in",Space,Str "Decker."]]
,HorizontalRule
,Header 1 ("images",[],[]) [Str "Images"]
,Para [Str "Include",Space,Str "images",Space,Str "in",Space,Str "presentations:"]
,CodeBlock ("",["markdown"],[]) "![Image Caption](image-file-location){css-formatting}"
,HorizontalRule
,Header 1 ("example-images",["sub"],[]) [Str "Images",Space,Str "example"]
,Div ("",["box"],[])
 [Header 2 ("",[],[]) []
 ,CodeBlock ("",["markdown"],[]) "![Haskell](img/haskell.png){width=\"30%\"}"]
,Div ("",["box"],[])
 [Header 2 ("",[],[]) []
 ,Div ("",[],[])
  [Plain [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/haskell.png\" style=\"height:auto;width:30%;\" alt=\"Haskell\" title=\"fig:\">",RawInline (Format "html") "<figcaption>",Str "Haskell",RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"]]]
,HorizontalRule
,Header 1 ("video",[],[]) [Str "Videos"]
,Para [Str "Include",Space,Str "videos",Space,Str "in",Space,Str "presentations:"]
,CodeBlock ("",["markdown"],[]) "![title](video-file-location){css-formatting}"
,HorizontalRule
,Header 1 ("example-movies_1",["sub"],[]) [Str "Videos",Space,Str "example"]
,CodeBlock ("",["markdown"],[]) "Video with controls:\n![](movies/jmu-hci-intro.mp4){controls=1}\n\nVideo with autoplay:\n![](movies/jmu-hci-intro.mp4){data-autoplay=true}\n\nStart video at timestamp:\n![](movies/jmu-hci-intro.mp4){data-autoplay=true start=\"10\"}"
,Div ("",["css-columns"],[])
 [Div ("",["box","split"],[])
  [Header 2 ("",["split"],[]) []
  ,Para [Str "Video",Space,Str "with",Space,Str "controls:",SoftBreak,RawInline (Format "html") "<video data-src=\"movies/jmu-hci-intro.mp4\" controls=\"1\">Browser does not support video.</video>"]]
 ,Div ("",["box"],[])
  [Header 2 ("",[],[]) []
  ,Para [Str "Video",Space,Str "with",Space,Str "autoplay:",SoftBreak,RawInline (Format "html") "<video data-src=\"movies/jmu-hci-intro.mp4\" data-autoplay=\"true\">Browser does not support video.</video>"]]]
,HorizontalRule
,Header 1 ("ext-vid",[],[]) [Str "External",Space,Str "Videos"]
,Para [Str "Include",Space,Str "YouTube",Space,Str "and",Space,Str "Vimeo",Space,Str "videos",Space,Str "or",Space,Str "Twitch",Space,Str "channels",Space,Str "in",Space,Str "presentations:"]
,CodeBlock ("",["markdown"],[]) "![](service://video-id){css-formatting}"
,Para [Emph [Str "Note",Space,Str "1:"],Space,Str "Replace",Space,Code ("",[],[]) "service",Space,Str "with",Space,Code ("",[],[]) "youtube",Str ",",Space,Code ("",[],[]) "vimeo",Space,Str "or",Space,Code ("",[],[]) "twitch",Space,Str "and",Space,Str "add",Space,Str "video",Space,Str "id",Space,Str "or",Space,Str "twitch",Space,Str "channel",Space,Str "name",Space,Str "(replaces",Space,Code ("",[],[]) "video-id",Str ")."]
,Para [Emph [Str "Note",Space,Str "2:"],Space,Str "The",Space,Str "video",Space,Str "ID",Space,Str "is",Space,Str "usually",Space,Str "found",Space,Str "in",Space,Str "the",Space,Str "URL."]
,Para [Strong [Str "YouTube",Space,Str "example",Space,Str "URL:"],Space,Str "https://www.youtube.com/watch?v=",RawInline (Format "html") "<u>",Str "qEcmwHRG2Mo",RawInline (Format "html") "</u>",LineBreak,Strong [Str "YouTube",Space,Str "video",Space,Str "ID:"],Space,Str "qEcmwHRG2Mo"]
,HorizontalRule
,Header 1 ("example-movies_2",["sub"],[]) [Str "External",Space,Str "Videos",Space,Str "example"]
,CodeBlock ("",["markdown"],[]) "![](youtube://qEcmwHRG2Mo){width=\"65%\" start=\"10\"}"
,Para [RawInline (Format "html") "<figure class=\"\" style=\"width:65%;start:10;\"><div style=\"position:relative;padding-top:25px;padding-bottom:56.25%;height:0;\"><iframe style=\"position:absolute;top:0;left:0;width:100%;height:100%;\" width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/qEcmwHRG2Mo?iv_load_policy=3&amp;disablekb=1&amp;rel=0&amp;modestbranding=1&amp;autohide=1&amp;start=10\" frameborder=\"0\" allowfullscreen=\"\"><p></p></iframe></div></figure>"]
,HorizontalRule
,Header 1 ("fullscreen",[],[]) [Str "Fullscreen",Space,Str "Videos"]
,Para [Str "Fullscreen",Space,Str "videos",Space,Str "are",Space,Str "identified",Space,Str "in",Space,Str "the",Space,Str "slide",Space,Str "header:"]
,CodeBlock ("",["markdown"],[]) "# ![](movies/jmu-hci-intro.mp4){controls=1}"
,Para [Emph [Str "Note:"],Space,Str "Do",Space,Str "not",Space,Str "include",Space,Str "a",Space,Str "slide",Space,Str "title."]
,HorizontalRule
,Header 1 ("example-movies_3",["sub"],[("data-background-video","movies/jmu-hci-intro.mp4"),("data-menu-title","Fullscreen Videos Example"),("controls","1")]) [Space]
,HorizontalRule
,Header 1 ("audio",[],[]) [Str "Audio"]
,Para [Str "Include",Space,Str "audio",Space,Str "clips",Space,Str "in",Space,Str "presentations:"]
,CodeBlock ("",["markdown"],[]) "![title](audio-file-location){css-formatting}"
,HorizontalRule
,Header 1 ("example-audio",["sub"],[]) [Str "Audio",Space,Str "example"]
,Div ("",["box"],[("style","font-size:small;")])
 [Header 2 ("",[],[("style","font-size:small;")]) []
 ,CodeBlock ("",["markdown"],[]) "Audio with controls:\n![](audio/wildbach.mp3){controls=1}\n\nAudio with controls and autoplay:\n![](audio/wildbach.mp3){controls=1 data-autoplay=true}"]
,Div ("",["box"],[])
 [Header 2 ("",[],[]) []]
,Div ("",["css-columns"],[])
 [Div ("",["box","split","example"],[])
  [Header 2 ("",["split","example"],[]) []
  ,Para [Str "Audio",Space,Str "with",Space,Str "controls:",SoftBreak,Span ("",[],[]) [RawInline (Format "html") "<figure>",RawInline (Format "html") "<audio data-src=\"audio/wildbach.mp3\" alt=\"Wildbach\" controls=\"1\">Browser does not support audio.</audio>",RawInline (Format "html") "<figcaption>",Str "Wildbach",RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"]]]
 ,Div ("",["box","example"],[])
  [Header 2 ("",["example"],[]) []
  ,Para [Str "Audio",Space,Str "with",Space,Str "controls",Space,Str "and",Space,Str "autoplay:",SoftBreak,Span ("",[],[]) [RawInline (Format "html") "<figure>",RawInline (Format "html") "<audio data-src=\"audio/wildbach.mp3\" alt=\"Wildbach\" controls=\"1\" data-autoplay=\"true\">Browser does not support audio.</audio>",RawInline (Format "html") "<figcaption>",Str "Wildbach",RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"]]]]
,HorizontalRule
,Header 1 ("tables",[],[]) [Str "Tables"]
,Para [Str "Tables",Space,Str "are",Space,Str "created",Space,Str "with",Space,Str "pipes",Space,Str "(|)",Space,Str "and",Space,Str "hyphens",Space,Str "(-).",Space,Str "Align",Space,Str "text",Space,Str "with",Space,Str "colons",Space,Str "(:)",Space,Str "on",Space,Str "the",Space,Str "left,",Space,Str "right,",Space,Str "or",Space,Str "on",Space,Str "both",Space,Str "sides",Space,Str "of",Space,Str "the",Space,Str "hyphens",Space,Str "in",Space,Str "the",Space,Str "header",Space,Str "row."]
,CodeBlock ("",["markdown"],[]) "| Right Align | Left Align | Center Align | Default |\n|        ---: | :---       |    :---:     | ------- |\n|        data | data       |     data     | data    |\n|        data | data       |     data     | data    |"
,HorizontalRule
,Header 1 ("example-tables",["sub"],[]) [Str "Tables",Space,Str "example"]
,Div ("",["box"],[("style","font-size:small;")])
 [Header 2 ("",[],[("style","font-size:small;")]) []
 ,CodeBlock ("",["markdown"],[]) "Table: Assignment List\n\n|  Week | Topic | Reading | Book |\n|  ---: | :---  |  :---:  | ---- |\n|   1   | Course Introduction | Chapt. 1 | Physics |\n|   2   | Inertia, Equilibrium, Kinematics | Chapt. 2-3| Physics |\n|   3   | Vectors, Momentum, Energy | Chapt. 4-7 | Physics |"]
,Div ("",["box"],[])
 [Header 2 ("",[],[]) []]
,Div ("",["box","example"],[])
 [Header 2 ("",["example"],[]) []
 ,Table [Str "Assignment",Space,Str "List"] [AlignRight,AlignLeft,AlignCenter,AlignDefault] [0.0,0.0,0.0,0.0]
  [[Plain [Str "Week"]]
  ,[Plain [Str "Topic"]]
  ,[Plain [Str "Reading"]]
  ,[Plain [Str "Book"]]]
  [[[Plain [Str "1"]]
   ,[Plain [Str "Course",Space,Str "Introduction"]]
   ,[Plain [Str "Chapt.",Space,Str "1"]]
   ,[Plain [Str "Physics"]]]
  ,[[Plain [Str "2"]]
   ,[Plain [Str "Inertia,",Space,Str "Equilibrium,",Space,Str "Kinematics"]]
   ,[Plain [Str "Chapt.",Space,Str "2,",Space,Str "3,",Space,Str "4"]]
   ,[Plain [Str "Physics"]]]
  ,[[Plain [Str "3"]]
   ,[Plain [Str "Vectors,",Space,Str "Momentum,",Space,Str "Energy"]]
   ,[Plain [Str "Chapt.",Space,Str "5-8"]]
   ,[Plain [Str "Physics"]]]]]
,HorizontalRule
,Header 1 ("code",[],[]) [Str "Verbatim",Space,Str "Code",Space,Str "Blocks"]
,Para [Str "To",Space,Str "treat",Space,Str "text",Space,Str "as",Space,Str "verbatim,",Space,Str "either:"]
,BulletList
 [[Plain [Str "surround",Space,Str "text",Space,Str "with",Space,Str "three",Space,Str "tildes",Space,Str "(",Space,Str "~",Space,Str ")",Space,Str "or",Space,Str "backticks",Space,Str "(",Space,Str "`",Space,Str ")",LineBreak]]
 ,[Plain [Str "or",Space,Str "indent",Space,Str "each",Space,Str "line",Space,Str "by",Space,Str "four",Space,Str "spaces."]]]
,HorizontalRule
,Header 1 ("example-code",[],[]) [Str "Verbatim",Space,Str "Code",Space,Str "Block",Space,Str "example"]
,Div ("",["box"],[("style","font-size:small;")])
 [Header 2 ("",[],[("style","font-size:small;")]) []
 ,CodeBlock ("",["markdown"],[]) "~~~{.java label=\"Java\"}\nif (a > 3) {\n  moveShip(5 * gravity, DOWN);\n}\n~~~"]
,Div ("",["box"],[])
 [Header 2 ("",[],[]) []
 ,CodeBlock ("",["java"],[("label","Java")]) "if (a > 3) {\n  moveShip(5 * gravity, DOWN);\n}"]
,HorizontalRule
,Header 1 ("blockQuote",[],[]) [Str "Block",Space,Str "Quotes"]
,Para [Str "To",Space,Str "quote",Space,Str "a",Space,Str "block",Space,Str "of",Space,Str "text,",Space,Str "preceed",Space,Str "each",Space,Str "line",Space,Str "with",Space,Str "a",Space,Str "(>)",Space,Str "character:"]
,CodeBlock ("",["markdown"],[]) "> This is a block quote.\n>\n> > A block quote within a block quote."
,BlockQuote
 [Para [Str "This",Space,Str "is",Space,Str "a",Space,Str "block",Space,Str "quote."]
 ,BlockQuote
  [Para [Str "A",Space,Str "block",Space,Str "quote",Space,Str "within",Space,Str "a",Space,Str "block",Space,Str "quote."]]]
,HorizontalRule
,Header 1 ("math",[],[("layout","columns")]) [Str "Mathematics"]
,Div ("",["single-column-row"],[])
 [Div ("",["box","top"],[])
  [Header 2 ("",["top"],[]) []
  ,BulletList
   [[Plain [Str "Single",Space,Str "$",Space,Str "encloses",Space,Str "inline",Space,Str "math"]]
   ,[Plain [Str "Double",Space,Str "$$",Space,Str "encloses",Space,Str "a",Space,Str "display",Space,Str "math",Space,Str "block"]]]]]
,Div ("",["multi-column-row","multi-column-row-3"],[])
 [Div ("",["grow-1","column","column-1"],[])
  [Div ("",["box","left"],[])
   [Header 2 ("",["left"],[]) []]
  ,Div ("",["box"],[("style","font-size:small;")])
   [Header 2 ("",[],[("style","font-size:small;")]) []
   ,CodeBlock ("",["latex"],[]) "To $\\infty$ and beyond!"]
  ,Div ("",["box"],[("style","font-size:small;")])
   [Header 2 ("",[],[("style","font-size:small;")]) []
   ,CodeBlock ("",["latex"],[]) "$$ e = mc ^ 2 $$"]
  ,Div ("",["box"],[("style","font-size:small;")])
   [Header 2 ("",[],[("style","font-size:small;")]) []
   ,CodeBlock ("",["latex"],[]) "\\lim_{x \\to \\infty} \\exp(-x) = 0"]]
 ,Div ("",["grow-1","column","column-3"],[])
  [Div ("",["box","right"],[])
   [Header 2 ("",["right"],[]) []]
  ,Div ("",["box","example"],[])
   [Header 2 ("",["example"],[]) []
   ,Para [Str "To",Space,Math InlineMath "\\infty",Space,Str "and",Space,Str "beyond!"]]
  ,Div ("",["box","example"],[])
   [Header 2 ("",["example"],[]) []
   ,Para [Math DisplayMath " e = mc ^ 2 "]]
  ,Div ("",["box","example"],[])
   [Header 2 ("",["example"],[]) []
   ,Para [Math DisplayMath " \\lim_{x \\to \\infty} \\exp(-x) = 0 "]]]]
,HorizontalRule
,Header 1 ("java",[],[]) [Str "Syntax",Space,Str "Highlighting"]
,Para [Str "Apply",Space,Str "syntax",Space,Str "highlighting",Space,Str "with",Space,Str "the",Space,Code ("",[],[]) ".java",Space,Str "tag.",Space,Str "(",Code ("",[],[]) "java",Space,Str "can",Space,Str "be",Space,Str "replaced",Space,Str "with",Space,Str "any",Space,Str "programming",Space,Str "language)"]
,CodeBlock ("",["markdown"],[]) "~~~{.java label=\"Java\"}\nString s = \"Java highlighting syntax\";\nSystem.out.println (s);\n~~~"
,Div ("",["box"],[])
 [Header 2 ("",[],[]) []
 ,CodeBlock ("",["java"],[("label","Java")]) "String s = \"Java highlighting syntax\";\nSystem.out.println (s);"]
,HorizontalRule
,Header 1 ("javascript",[],[]) [Str "Javascript",Space,Str "Syntax",Space,Str "Highlighting"]
,Para [Str "Apply",Space,Str "Javascript",Space,Str "syntax",Space,Str "highlighting",Space,Str "with",Space,Str "the",Space,Code ("",[],[]) ".Javascript",Space,Str "tag."]
,CodeBlock ("",["markdown"],[]) "~~~javascript\nvar s = \"JavaScript syntax highlighting\";\nalert (s);\n~~~"
,Div ("",["box"],[])
 [Header 2 ("",[],[]) []
 ,CodeBlock ("",["javascript"],[]) "var s = \"JavaScript syntax highlighting\";\nalert (s);"]
,HorizontalRule
,Header 1 ("externalWebite",[],[]) [Str "Embed",Space,Str "External",Space,Str "Websites"]
,BulletList
 [[Plain [Str "Paste",Space,Str "the",Space,Str "following",Space,Str "iframe",Space,Str "on",Space,Str "a",Space,Str "blank",Space,Str "slide",LineBreak]]
 ,[Plain [Str "Change",Space,Quoted DoubleQuote [Str "https://www.uni-wuerzburg.de/"],Space,Str "to",Space,Str "your",Space,Str "website"]]]
,Div ("",["box"],[])
 [Header 2 ("",[],[]) []
 ,CodeBlock ("",["html"],[]) "<iframe class = \"stretch\" src = \"https://www.uni-wuerzburg.de/\"></iframe>"]
,HorizontalRule
,Header 1 ("example-externalWebsite",["sub"],[("data-menu-title","External Website Example")]) []
,RawBlock (Format "html") "<iframe class=\"stretch\" src=\"https://www.uni-wuerzburg.de/\">"
,RawBlock (Format "html") "</iframe>"
,HorizontalRule
,Header 1 ("embedPDF",[],[("style","font-size:small;")]) [Str "Embed",Space,Str "PDF",Space,Str "documents"]
,CodeBlock ("",["markdown"],[]) "![](http://pandoc.org/MANUAL.pdf){width=\"100%\" height=\"500px\"}"
,Para [RawInline (Format "html") "<iframe data-src=\"http://pandoc.org/MANUAL.pdf\" style=\"height:500px;width:100%;\">Browser does not support iframe.</iframe>"]
,RawBlock (Format "html") "<!-- The given path (../../resource/support/...) won't work; therefore I excluded this part for now\n# Embed JavaScript {#embedJavascript}\n\n![](webgl_geometry_minecraft_ao.html){.iframe width=\"100%\" height=\"500px\"}\n\n[](../../resource/support/three.js){.resource}\n\n# ![](webgl_geometry_minecraft_ao.html)\n-->"
,HorizontalRule
,Header 1 ("chalkboard",[],[]) [Str "Chalkboard"]
,Para [Str "Dynamically",Space,Str "make",Space,Str "notes",Space,Str "on",Space,Str "presentations:"]
,BulletList
 [[Plain [Str "Make",Space,Str "notes",Space,Str "on",Space,Str "slides:",Space,Str "click",Space,RawInline (Format "html") "<i class=\"fas fa-pencil-alt\">",RawInline (Format "html") "</i>",Space,Str "or",Space,Str "type",Space,Quoted SingleQuote [Str "c"]]]
 ,[Plain [Str "Draw",Space,Str "on",Space,Str "chalkboard:",Space,Str "click",Space,RawInline (Format "html") "<i class=\"fas fa-edit\">",RawInline (Format "html") "</i>",Space,Str "or",Space,Str "type",Space,Quoted SingleQuote [Str "t"]]]
 ,[Plain [Str "Left",Space,Str "mouse",Space,Str "to",Space,Str "draw,",Space,Str "right",Space,Str "to",Space,Str "erase,",Space,Str "center",Space,Str "for",Space,Str "laser",Space,Str "pointer"]]
 ,[Plain [Str "Click",Space,Str "icon",Space,Str "again",Space,Str "or",Space,Str "type",Space,Quoted SingleQuote [Str "c"],Space,Str "or",Space,Quoted SingleQuote [Str "t"],Space,Str "to",Space,Str "close"]]
 ,[Plain [Quoted SingleQuote [Str "Del"],Space,Str "key",Space,Str "clears",Space,Str "chalkboard"]]
 ,[Plain [Str "Drawings",Space,Str "are",Space,Str "saved",Space,Str "-",Space,Str "type",Space,Quoted SingleQuote [Str "d"],Space,Str "to",Space,Str "download"]]]
,HorizontalRule
,Header 1 ("speakerNotes",[],[]) [Str "Speaker",Space,Str "Notes"]
,Para [Str "Slides",Space,Str "with",Space,Str "headers",Space,Str "with",Space,Str "the",Space,Code ("",[],[]) ".notes",Space,Str "tag",Space,Str "are",Space,Str "not",Space,Str "included",Space,Str "in",Space,Str "a",Space,Str "presentation.",Space,Str "They",Space,Str "only",Space,Str "appear",Space,Str "in",Space,Str "the",Space,Str "handout",Space,Str "and",Space,Str "in",Space,Str "the",Space,Str "speaker",Space,Str "view",Space,Str "(press",Space,Code ("",[],[]) "s",Space,Str "on",Space,Str "this",Space,Str "slide",Space,Str "to",Space,Str "access",Space,Str "the",Space,Str "speaker",Space,Str "view)."]
,Div ("",["box"],[])
 [Header 2 ("",[],[]) []
 ,CodeBlock ("",["markdown"],[]) "# Why Gamify? {.notes}\n\n- Games are among the most powerful motivational tools.\n- Make the non-game experience more rewarding.\n- Motivation has limits. A large leaderboard divide may\n  cause the player to abandon the game."]
,RawBlock (Format "html") "<aside class=\"notes\">"
,Header 2 ("",[],[]) [Str "Why",Space,Str "Gamify?"]
,BulletList
 [[Para [Str "Games",Space,Str "are",Space,Str "among",Space,Str "the",Space,Str "most",Space,Str "powerful",Space,Str "motivational",Space,Str "tools."]]
 ,[Para [Str "Make",Space,Str "the",Space,Str "non-game",Space,Str "experience",Space,Str "more",Space,Str "rewarding"]]
 ,[Para [Str "Motivation",Space,Str "has",Space,Str "limits.",Space,Str "A",Space,Str "large",Space,Str "leaderboard",Space,Str "divide",Space,Str "may",SoftBreak,Str "cause",Space,Str "the",Space,Str "player",Space,Str "to",Space,Str "abandon",Space,Str "the",Space,Str "game."]
  ,BulletList
   [[Plain [Link ("",[],[]) [Str "Blockquotes"] ("#block-quotes","")]]
   ,[Plain [Link ("",[],[]) [Str "Line",Space,Str "Blocks"] ("#line-blocks","")]]
   ,[Plain [Link ("",[],[]) [Str "Tags"] ("#tags","")]]]]]
,RawBlock (Format "html") "</aside>"
,HorizontalRule
,Header 1 ("citations",[],[]) [Str "Citations"]
,Para [Str "Add",Space,Str "citations",Space,Str "to",Space,Str "your",Space,Str "slide",Space,Str "deck.",Space,Str "Be",Space,Str "sure",Space,Str "to",Space,Str "include",Space,Str "a",Space,Code ("",[],[]) "csl",Space,Str "and",Space,Str "a",Space,Code ("",[],[]) "bib",Space,Str "file",SoftBreak,Str "in",Space,Str "your",Space,Link ("",[],[]) [Str "YAML",Space,Str "header"] ("#yaml",""),Str "."]
,Div ("",["box"],[("style","font-size:small;")])
 [Header 2 ("",[],[("style","font-size:small;")]) []
 ,CodeBlock ("",["markdown"],[]) "## Space Tentacles\n\nHave you heard about Space Tentacles [@zimmerer2018space].\nAccording to @zimmerer2018space it is a nice idea."]
,Div ("",["box"],[])
 [Header 2 ("",[],[]) [Str "Space",Space,Str "Tentacles"]
 ,Para [Str "Have",Space,Str "you",Space,Str "heard",Space,Str "about",Space,Str "Space",Space,Str "Tentacles",Space,Cite [Citation {citationId = "zimmerer2018space", citationPrefix = [], citationSuffix = [], citationMode = NormalCitation, citationNoteNum = 0, citationHash = 1}] [Str "(Zimmerer,",Space,Str "Fischbach,",Space,Str "and",Space,Str "Latoschik",Space,Str "2018)"],Str ".",SoftBreak,Str "According",Space,Str "to",Space,Cite [Citation {citationId = "zimmerer2018space", citationPrefix = [], citationSuffix = [], citationMode = AuthorInText, citationNoteNum = 0, citationHash = 2}] [Str "Zimmerer,",Space,Str "Fischbach,",Space,Str "and",Space,Str "Latoschik",Space,Str "(2018)"],Space,Str "it",Space,Str "is",Space,Str "a",Space,Str "nice",Space,Str "idea."]]
,HorizontalRule
,Header 1 ("yaml",[],[]) [Str "Header",Space,Str "Options"]
,Para [Str "Add",Space,Str "optional",Space,Str "settings",Space,Str "in",Space,Str "the",Space,Str "top",Space,Str "of",Space,Str "each",Space,Str "markdown",Space,Str "file."]
,Div ("",["box"],[("style","font-size:small;")])
 [Header 2 ("",[],[("style","font-size:small;")]) []
 ,CodeBlock ("",["yaml"],[]) "center: 0 - Align slide content to the top\ncenter: 1 - Align slide content vertically (default)\n\ncontrols: 0 - Display navigational arrows (default)\ncontrols: 1 - Hide navigational arrows\n\ntransition: fade\nOptions include: none, fade, slide, convex, concave, zoom\n\ncss: example.css - Enter the name of a custom css file\n\nslideNumber: true - Display slide numbers\nslideNumber: false - Hide slide numbers (default)\n\nmenu: true - Display the slide menu icon\nmenu: false - Hide the slide menu icon (default)\n\nhistory: true - Add visited slides to browser history\nhistory: false - Hide visited slides from browser history\n\ncsl: chicago-author-date.csl - citation style\nbibliography: example.bib\nchalkboard: example-deck.json - pre-defined chalkboard"]
,HorizontalRule
,Header 1 ("",["unnumbered"],[]) [Str "References"]
,Div ("refs",["references"],[])
 [Div ("ref-zimmerer2018space",[],[])
  [Para [Str "Zimmerer,",Space,Str "Chris,",Space,Str "Martin",Space,Str "Fischbach,",Space,Str "and",Space,Str "Marc",Space,Str "Erich",Space,Str "Latoschik.",Space,Str "2018.",Space,Str "\8220Space",Space,Str "Tentacles",Space,Str "-",Space,Str "Integrating",Space,Str "Multimodal",Space,Str "Input",Space,Str "into",Space,Str "a",Space,Str "Vr",Space,Str "Adventure",Space,Str "Game.\8221",Space,Str "In",Space,Emph [Str "Proceedings",Space,Str "of",Space,Str "the",Space,Str "25th",Space,Str "Ieee",Space,Str "Virtual",Space,Str "Reality",Space,Str "(Vr)",Space,Str "Conference"],Str ".",Space,Str "IEEE."]]]]