[HorizontalRule
,Header 1 ("",[],[]) [Str "Overview"]
,Div ("",["box"],[])
 [Header 2 ("",[],[]) [Link ("",[],[]) [Str "Command",Space,Str "line",Space,Str "deck"] ("./02-command-line-deck.html","")]
 ,Para [Str "How",Space,Str "to",Space,Str "use",Space,Code ("",[],[]) "decker",Space,Str "on",Space,Str "the",Space,Str "command",Space,Str "line."]]
,Div ("",["box"],[])
 [Header 2 ("",[],[]) [Link ("",[],[]) [Str "Header",Space,Str "deck"] ("./03-header-deck.html","")]
 ,Para [Str "An",Space,Str "overview",Space,Str "of",Space,Str "the",Space,Str "possible",Space,Str "configuration",Space,Str "options",Space,Str "that",Space,Str "can",Space,Str "be",Space,Str "included",Space,Str "in",Space,Str "the",Space,Str "header",Space,Str "of",Space,Str "the",Space,Code ("",[],[]) "*-deck.md",Space,Str "markdown",Space,Str "files."]]
,Div ("",["box"],[])
 [Header 2 ("",[],[]) [Link ("",[],[]) [Str "Example",Space,Str "deck"] ("./04-example-deck.html","")]
 ,Para [Str "An",Space,Str "extensive",Space,Str "overview",Space,Str "of",Space,Str "the",Space,Str "possibilities",Space,Str "of",Space,Str "designing",Space,Str "slide",Space,Str "decks",Space,Str "offered",Space,Str "by",Space,Code ("",[],[]) "decker",Str "."]]
,HorizontalRule
,Header 1 ("",[],[]) []
,Div ("",["box"],[])
 [Header 2 ("",[],[]) [Link ("",[],[]) [Str "Quiz",Space,Str "deck"] ("./05-quiz-deck.html","")]
 ,Para [Str "How",Space,Str "to",Space,Str "create",Space,Str "simple",Space,Str "quizzes",Space,Str "with",Space,Str "different",Space,Str "question",Space,Str "types",Space,Str "for",Space,Str "a",Space,Str "self-learning",Space,Str "scenario."]]
,Div ("",["box"],[])
 [Header 2 ("",[],[]) [Link ("",[],[]) [Str "Chart",Space,Str "deck"] ("./06-chart-deck.html","")]
 ,Para [Str "How",Space,Str "to",Space,Str "include",Space,Str "charts",Space,Str "using",Space,Str "the",Space,Str "reveal.js",Space,Str "chart",Space,Str "plugin."]]
,HorizontalRule
,Header 1 ("",[],[]) [Str "General",Space,Str "Information"]
,HorizontalRule
,Header 1 ("",[],[]) [Str "Folder",Space,Str "Structure"]
,BulletList
 [[Plain [Str "When",Space,Str "running,",Space,Str "decker",Space,Str "creates",Space,Str "a",Space,Code ("",[],[]) "public",Space,Str "folder",Space,Str "in",Space,Str "the",Space,Str "current",Space,Str "directory."]]
 ,[Plain [Str "This",Space,Str "folder",Space,Str "contains",Space,Str "the",Space,Str "template",Space,Str "and",Space,Str "support",Space,Str "files",Space,Str "needed",Space,Str "during",Space,Str "presentation",Space,Str "time",Space,Str "as",Space,Str "well",Space,Str "as",Space,Str "the",Space,Str "generated",Space,Code ("",[],[]) "html",Space,Str "and",Space,Code ("",[],[]) "pdf",Space,Str "files."]]]
,HorizontalRule
,Header 1 ("",[],[]) [Str "Technologies"]
,Para [Str "Decker",Space,Str "is",Space,Str "built",Space,Str "using:"]
,BulletList
 [[Plain [Link ("",[],[]) [Str "Haskell"] ("https://www.haskell.org/",""),Str ",",Space,Link ("",[],[]) [Str "Pandoc"] ("https://pandoc.org/",""),Str ",",Space,Link ("",[],[]) [Str "Shake"] ("https://shakebuild.com/","")]]
 ,[Plain [Link ("",[],[]) [Str "reveal.js"] ("https://revealjs.com/#/","")]]
 ,[Plain [Link ("",[],[]) [Str "LaTeX"] ("","")]]
 ,[Plain [Link ("",[],[]) [Str "Chrome/Chromium"] ("",""),Space,Str "(for",Space,Str "pdf",Space,Str "generation)"]]]]