[HorizontalRule
,Header 1 ("",[],[]) [Str "Introduction"]
,BulletList
 [[Plain [Str "This",Space,Str "slide",Space,Str "deck",Space,Str "shows",Space,Str "how",Space,Str "to",Space,Str "create",Space,Str "simple",Space,Str "quizzes",Space,Str "with",Space,Str "different",Space,Str "question",Space,Str "types",Space,Str "for",Space,Str "a",Space,Str "self-learning",Space,Str "scenario."]]
 ,[Plain [Str "Currently",Space,Str "supported:"]
  ,BulletList
   [[Plain [Str "Matching/pairing",Space,Str "questions"]]
   ,[Plain [Str "Multiple",Space,Str "choice",Space,Str "questions"]]
   ,[Plain [Str "Freetext",Space,Str "questions"]]]]]
,HorizontalRule
,Header 1 ("",[],[]) [Str "Matching",Space,Str "Questions",Space,Str "Syntax"]
,BulletList
 [[Plain [Str "This",Space,Str "type",Space,Str "of",Space,Str "questions",Space,Str "asks",Space,Str "to",Space,Str "create",Space,Str "pairs",Space,Str "by",Space,Str "dragging",Space,Str "each",Space,Str "element",Space,Str "from",Space,Str "a",Space,Str "number",Space,Str "of",Space,Str "elements",Space,Str "to",Space,Str "the",Space,Str "corresponding",Space,Str "area."]]
 ,[Plain [Str "Currently",Space,Str "only",Space,Str "supports",Space,Str "exact",Space,Str "1:1",Space,Str "pairing."]]]
,CodeBlock ("",["markdown"],[]) "{match} A\n: pair with A\n\n{match} Haskell\n: ![](img/haskell.png)\n\n..."
,HorizontalRule
,Header 1 ("",[],[]) [Str "Matching",Space,Str "Questions"]
,Div ("",["matching"],[])
 [Div ("",["dropzones"],[])
  [Div ("",["dropzone"],[])
   [Plain [Str "A"]]
  ,Div ("",["dropzone"],[])
   [Plain [Str "Haskell"]]
  ,Div ("",["dropzone"],[])
   [Plain [Str "B"]]
  ,Div ("",["dropzone"],[])
   [Plain [Str "decker"]]
  ,Div ("",["dropzone"],[])
   [Plain [Str "C"]]]
 ,Div ("",["dragzone"],[])
  [Div ("",["draggable"],[("draggable","true")])
   [Plain [Str "pair",Space,Str "with",Space,Str "A"]]
  ,Div ("",["draggable"],[("draggable","true")])
   [Plain [RawInline (Format "html") "<img data-src=\"img/haskell.png\">"]]
  ,Div ("",["draggable"],[("draggable","true")])
   [Plain [Str "drag",Space,Str "to",Space,Str "B"]]
  ,Div ("",["draggable"],[("draggable","true")])
   [Plain [Link ("",[],[]) [Str "decker"] ("http://go.uniwue.de/decker","")]]
  ,Div ("",["draggable"],[("draggable","true")])
   [Plain [Math InlineMath "\\Leftarrow",Space,Str "C"]]]
 ,Para [LineBreak,RawInline (Format "html") "<button class=\"matchingAnswerButton\" type=\"button\">",Str "Show Solution",RawInline (Format "html") "</button>",RawInline (Format "html") "<button class=\"retryButton\" type=\"button\">",Str "Retry",RawInline (Format "html") "</button>"]]
,HorizontalRule
,Header 1 ("",[],[]) [Str "Freetext",Space,Str "Questions",Space,Str "Syntax"]
,BulletList
 [[Plain [Str "Freetext",Space,Str "questions",Space,Str "consist",Space,Str "of",Space,Str "a",Space,Str "bullet",Space,Str "list",Space,Str "of",Space,Str "two",Space,Str "elements",Space,Str "with",Space,Str "specific",Space,Str "syntax"]]
 ,[Plain [Str "Two",Space,Str "separate",Space,Str "questions",Space,Str "have",Space,Str "to",Space,Str "be",Space,Str "separated",Space,Str "for",Space,Str "example",Space,Str "by",Space,Str "using",Space,Str "a",Space,Str "level",Space,Str "two",Space,Str "header"]]]
,CodeBlock ("",["markdown"],[]) "* {?} Question text\n* {!} Correct solution\n\n## \n\n* {?} Question 2\n* {!} Answer\n"
,HorizontalRule
,Header 1 ("",[],[("layout","columns")]) [Str "Freetext",Space,Str "Questions"]
,Div ("",["multi-column-row","multi-column-row-3"],[])
 [Div ("",["grow-1","column","column-1"],[])
  [Div ("",["box","left"],[])
   [Header 2 ("",["left"],[]) []
   ,Div ("",["freetextQuestion"],[])
    [Para [RawInline (Format "html") "<form onSubmit=\"return false;\">",Space,Math InlineMath "2*2=~?",LineBreak,RawInline (Format "html") "<input type=\"text\" class=\"freetextInput\">",LineBreak,RawInline (Format "html") "<button class=\"freetextAnswerButton\" type=\"button\">",Str "Show Solution",Span ("",["freetextAnswer"],[("style","display:none; font-color:black;"),("type","text")]) [Space,Str "4"],RawInline (Format "html") "</button>",RawInline (Format "html") "</form>"]]]
  ,Div ("",["box"],[])
   [Header 2 ("",[],[]) []
   ,Div ("",["freetextQuestion"],[])
    [Para [RawInline (Format "html") "<form onSubmit=\"return false;\">",Space,Str "The",Space,Str "Answer",Space,Str "to",Space,Str "the",Space,Str "Ultimate",Space,Str "Question",Space,Str "of",Space,Str "Life,",Space,Str "the",Space,Str "Universe,",Space,Str "and",Space,Str "Everything",Space,Str "is",Space,Str "\8230?",LineBreak,RawInline (Format "html") "<input type=\"text\" class=\"freetextInput\">",LineBreak,RawInline (Format "html") "<button class=\"freetextAnswerButton\" type=\"button\">",Str "Show Solution",Span ("",["freetextAnswer"],[("style","display:none; font-color:black;"),("type","text")]) [Space,Str "42"],RawInline (Format "html") "</button>",RawInline (Format "html") "</form>"]]]]
 ,Div ("",["grow-1","column","column-3"],[])
  [Div ("",["box","right"],[])
   [Header 2 ("",["right"],[]) []
   ,Div ("",["freetextQuestion"],[])
    [Para [RawInline (Format "html") "<form onSubmit=\"return false;\">",Space,Str "Is",Space,Str "this",Space,Str "a",Space,Str "question?",LineBreak,RawInline (Format "html") "<input type=\"text\" class=\"freetextInput\">",LineBreak,RawInline (Format "html") "<button class=\"freetextAnswerButton\" type=\"button\">",Str "Show Solution",Span ("",["freetextAnswer"],[("style","display:none; font-color:black;"),("type","text")]) [Space,Str "yes"],RawInline (Format "html") "</button>",RawInline (Format "html") "</form>"]]]
  ,Div ("",["box"],[])
   [Header 2 ("",[],[]) []
   ,Div ("",["freetextQuestion"],[])
    [Para [RawInline (Format "html") "<form onSubmit=\"return false;\">",Space,Str "Name",Space,Str "the",Space,Str "capital",Space,Str "of",Space,Str "Germany",LineBreak,RawInline (Format "html") "<input type=\"text\" class=\"freetextInput\">",LineBreak,RawInline (Format "html") "<button class=\"freetextAnswerButton\" type=\"button\">",Str "Show Solution",Span ("",["freetextAnswer"],[("style","display:none; font-color:black;"),("type","text")]) [Space,Str "Berlin"],RawInline (Format "html") "</button>",RawInline (Format "html") "</form>"]]]]]
,HorizontalRule
,Header 1 ("",[],[]) [Str "Multiple",Space,Str "Choice",Space,Str "Questions",Space,Str "Syntax"]
,CodeBlock ("",["markdown"],[]) "* { } wrong answer\n* { } another wrong answer\n* {X} correct answer\n* { } wrong answer again"
,HorizontalRule
,Header 1 ("",[],[]) [Str "Multiple",Space,Str "Choice",Space,Str "Questions"]
,Div ("",["box","question"],[])
 [Header 2 ("",["question"],[]) [Str "Question:",Space,Str "Which",Space,Str "file",Space,Str "format",Space,Str "does",Space,Str "decker",Space,Str "use?"]
 ,Div ("",["survey"],[])
  [BulletList
   [[Div ("",["answer","wrong"],[])
     [Para [Space,Str ".docx"]]]
   ,[Div ("",["answer","wrong"],[])
     [Para [Space,Str ".csv"]]]
   ,[Div ("",["answer","wrong"],[])
     [Para [Space,Str ".xml"]]]
   ,[Div ("",["answer","right"],[])
     [Para [Space,Str ".md"]]]]
  ,Para [LineBreak,RawInline (Format "html") "<button class=\"mcAnswerButton\" type=\"button\">",Str "Show Solution",RawInline (Format "html") "</button>"]]]]