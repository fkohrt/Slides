[HorizontalRule
,Header 1 ("",[],[]) [Str "Introduction"]
,Para [Str "This",Space,Str "slide",Space,Str "deck",Space,Str "provides",Space,Str "an",Space,Str "overview",Space,Str "over",Space,Str "the",Space,Str "possible",Space,Str "command",Space,Str "line",Space,Str "arguments",Space,Str "supported",Space,Str "by",Space,Str "decker."]
,HorizontalRule
,Header 1 ("",[],[]) [Str "Workflow"]
,Para [Str "The",Space,Str "general",Space,Str "recommended",Space,Str "workflow",Space,Str "of",Space,Str "decker",Space,Str "on",Space,Str "the",Space,Str "command",Space,Str "line",Space,Str "is:"]
,BulletList
 [[Plain [Code ("",[],[]) "decker example",Space,Str "to",Space,Str "create",Space,Str "a",Space,Str "new",Space,Str "project,",Space,Code ("",[],[]) "cd",Space,Str "into",Space,Str "the",Space,Str "generated",Space,Str "project"]]
 ,[Plain [Str "If",Space,Str "you",Space,Str "have",Space,Str "an",Space,Str "existing",Space,Str "project,",Space,Str "navigate",Space,Str "to",Space,Str "that",Space,Str "directory",Space,Str "on",Space,Str "the",Space,Str "command",Space,Str "line"]]
 ,[Plain [Code ("",[],[]) "decker server",Space,Str "to",Space,Str "create",Space,Str "html",Space,Str "versions",Space,Str "and",Space,Str "open",Space,Str "a",Space,Str "local",Space,Str "server"]]
 ,[Plain [Str "Navigate",Space,Str "to",Space,Code ("",[],[]) "localhost:8888",Space,Str "in",Space,Str "a",Space,Str "browser"]]
 ,[Plain [Str "Create",Space,Str "and",Space,Str "edit",Space,Code ("",[],[]) "*-deck.md",Space,Str "files",Space,Str "and",Space,Str "see",Space,Str "changes",Space,Str "in",Space,Str "the",Space,Str "browser",Space,Str "window",Space,Str "on",Space,Str "file",Space,Str "save"]]
 ,[Plain [Str "If",Space,Str "finished,",Space,Str "shut",Space,Str "down",Space,Str "the",Space,Str "server",Space,Str "by",Space,Str "pressing",Space,Code ("",[],[]) "^C/Ctrl C",Space,Str "on",Space,Str "the",Space,Str "command",Space,Str "line"]]]
,HorizontalRule
,Header 1 ("",[],[]) [Code ("",[],[]) "decker help"]
,Para [Str "Prints",Space,Str "a",Space,Str "help",Space,Str "document",Space,Str "to",Space,Str "stdout",Space,Str "in",Space,Str "Markdown",Space,Str "format."]
,HorizontalRule
,Header 1 ("",[],[]) [Code ("",[],[]) "decker info"]
,Para [Str "Prints",Space,Str "information",Space,Str "about",Space,Str "the",Space,Str "current",Space,Str "project\8217s",Space,Str "directories,",Space,Str "the",Space,Str "targets",Space,Str "(files",Space,Str "which",Space,Str "will",Space,Str "be",Space,Str "generated)",Space,Str "and",Space,Str "the",Space,Str "meta",Space,Str "data",Space,Str "options",Space,Str "which",Space,Str "are",Space,Str "found",Space,Str "in",Space,Str "top",Space,Str "level",Space,Code ("",[],[]) "*-meta.yaml",Space,Str "files."]
,HorizontalRule
,Header 1 ("",[],[]) [Code ("",[],[]) "decker example",Space,Str "and",Space,Code ("",[],[]) "decker tutorial"]
,BulletList
 [[Plain [Code ("",[],[]) "decker example",Space,Str "copies",Space,Str "an",Space,Str "example",Space,Str "project",Space,Str "to",Space,Str "the",Space,Str "current",Space,Str "directory"]]
 ,[Plain [Code ("",[],[]) "decker tutorial",Space,Str "copies",Space,Str "extended",Space,Str "examples",Space,Str "and",Space,Str "tutorial",Space,Str "decks",Space,Str "to",Space,Str "the",Space,Str "current",Space,Str "directory"]]]
,HorizontalRule
,Header 1 ("",[],[]) [Code ("",[],[]) "decker watch",Space,Str "and",Space,Code ("",[],[]) "decker server"]
,BulletList
 [[Para [Code ("",[],[]) "decker watch",Space,Str "Builds",Space,Str "HTML",Space,Str "versions",Space,Str "of",Space,Str "all",Space,Str "documents",Space,Str "and",Space,Str "then",Space,Str "watches",Space,Str "for",Space,Str "document",Space,Str "changes.",Space,Str "Each",Space,Str "change",Space,Str "to",Space,Str "a",Space,Str "watched",Space,Str "document",Space,Str "triggers",Space,Str "a",Space,Str "rebuild.",Space,Str "Watching",Space,Str "can",Space,Str "be",Space,Str "terminated",Space,Str "with",Space,Code ("",[],[]) "^C/Ctrl C",Str "."]]
 ,[Para [Code ("",[],[]) "decker server",Str ":",Space,Str "Like",Space,Code ("",[],[]) "decker watch",Str ".",Space,Str "Additionally",Space,Str "a",Space,Str "local",Space,Str "web",Space,Str "server",Space,Str "at",Space,Str "the",Space,Str "address",Space,Code ("",[],[]) "localhost:8888",Space,Str "is",Space,Str "started",Space,Str "that",Space,Str "serves",Space,Str "the",Space,Str "generated",Space,Str "HTML",Space,Str "files.",Space,Str "Changed",Space,Str "files",Space,Str "are",Space,Str "reloaded",Space,Str "in",Space,Str "the",Space,Str "browser."]]]
,HorizontalRule
,Header 1 ("",[],[]) [Code ("",[],[]) "decker html",Space,Str "and",Space,Code ("",[],[]) "decker decks"]
,BulletList
 [[Plain [Code ("",[],[]) "decker html",Space,Str "creates",Space,Str "all",Space,Str "HTML",Space,Str "files",Space,Str "without",Space,Str "opening",Space,Str "a",Space,Str "server"]]
 ,[Plain [Code ("",[],[]) "decker decks",Str "creates",Space,Str "only",Space,Str "HTML",Space,Str "slide",Space,Str "decks"]]]
,HorizontalRule
,Header 1 ("",[],[]) [Code ("",[],[]) "decker clean"]
,BulletList
 [[Plain [Str "Recursively",Space,Str "removes",Space,Str "all",Space,Str "generated",Space,Str "files",Space,Str "from",Space,Str "the",Space,Str "current",Space,Str "directory",Space,Str "i.e.\160only",Space,Str "the",Space,Code ("",[],[]) "public",Space,Str "folder."]]
 ,[Plain [Str "Also",Space,Str "clears",Space,Str "cached",Space,Str "resource",Space,Str "folders",Space,Str "with",Space,Str "version",Space,Str "number",Space,Str "older",Space,Str "than",Space,Str "the",Space,Str "currently",Space,Str "used",Space,Str "decker",Space,Str "version."]]]
,HorizontalRule
,Header 1 ("",[],[]) [Code ("",[],[]) "decker pdf",Space,Str "and",Space,Code ("",[],[]) "decker pdf-decks"]
,BulletList
 [[Plain [Code ("",[],[]) "decker pdf",Space,Str "creates",Space,Str "pdf",Space,Str "versions",Space,Str "of",Space,Str "all",Space,Str "files"]]
 ,[Plain [Code ("",[],[]) "decker pdf-decks",Space,Str "creates",Space,Str "pdf",Space,Str "versions",Space,Str "only",Space,Str "of",Space,Str "the",Space,Str "html",Space,Str "slide",Space,Str "decks"]]]
,HorizontalRule
,Header 1 ("",[],[]) []
,Para [Str "To",Space,Str "use",Space,Code ("",[],[]) "decker pdf",Space,Str "or",Space,Code ("",[],[]) "decker pdf-decks",Str ",",Space,Str "Google",Space,Str "Chrome",Space,Str "has",Space,Str "to",Space,Str "be",Space,Str "installed.",LineBreak,Str "-",Space,Strong [Str "Windows:"],Space,Str "Currently",Space,Code ("",[],[]) "decker pdf",Space,Str "does",Space,Str "not",Space,Str "work",Space,Str "on",Space,Str "Windows.",Space,Str "Please",Space,Str "add",Space,Code ("",[],[]) "print: true",Space,Str "or",Space,Code ("",[],[]) "menu: true",Space,Str "to",Space,Str "your",Space,Str "slide",Space,Str "deck",Space,Str "and",Space,Str "use",Space,Str "the",Space,Str "print",Space,Str "button",Space,Str "on",Space,Str "the",Space,Str "title",Space,Str "slide",Space,Str "or",Space,Str "in",Space,Str "the",Space,Str "menu.",SoftBreak,Str "-",Space,Strong [Str "MacOS:"],Space,Str "Follow",Space,Str "the",Space,Str "Google",Space,Str "Chrome",Space,Str "installer",Space,Str "instructions.",Space,Strong [Str "Google",Space,Str "Chrome.app"],Space,Str "has",Space,Str "to",Space,Str "be",Space,Str "located",Space,Str "in",Space,Str "either",Space,Code ("",[],[]) "/Applications/Google Chrome.app",Space,Str "or",Space,Code ("",[],[]) "/Users/username/Applications/Google Chrome.app",SoftBreak,Str "Alternatively",Space,Str "you",Space,Str "can",Space,Str "add",Space,Code ("",[],[]) "chrome",Space,Str "to",Space,Code ("",[],[]) "$PATH",Str ".",LineBreak,Str "-",Space,Strong [Str "Linux:"],Space,Code ("",[],[]) "chrome",Space,Str "has",Space,Str "to",Space,Str "be",Space,Str "on",Space,Code ("",[],[]) "$PATH",Str "."]
,HorizontalRule
,Header 1 ("",[],[]) [Code ("",[],[]) "decker publish"]
,BulletList
 [[Plain [Str "Publish",Space,Str "the",Space,Str "generated",Space,Str "files",Space,Str "to",Space,Str "a",Space,Str "remote",Space,Str "location",Space,Str "using",Space,Code ("",[],[]) "rsync",Space,Str "if",Space,Str "the",Space,Str "location",Space,Str "is",Space,Str "specified",Space,Str "in",Space,Str "the",Space,Str "meta",Space,Str "data."]]
 ,[Plain [Str "The",Space,Str "keys",Space,Code ("",[],[]) "rsync-destination.host",Space,Str "and",Space,Code ("",[],[]) "rsync-destination.path",Space,Str "specify",Space,Str "the",Space,Str "publishing",Space,Str "destination."]]]]