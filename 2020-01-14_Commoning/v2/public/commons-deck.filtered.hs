[HorizontalRule
,Header 1 ("",[],[("layout","columns")]) [Str "Die",Space,Quoted DoubleQuote [Str "Tragik",Space,Str "der",Space,Str "Allmende"]]
,Div ("",["single-column-row"],[])
 [Div ("",["box","top"],[])
  [Header 2 ("",["top"],[]) []
  ,Para [Str "nach",Space,Cite [Citation {citationId = "Hardin1968", citationPrefix = [], citationSuffix = [], citationMode = AuthorInText, citationNoteNum = 0, citationHash = 1}] [Str "Hardin",Space,Str "(1968)"]]]]
,Div ("",["multi-column-row","multi-column-row-3"],[])
 [Div ("",["grow-1","column","column-1"],[])
  [Div ("",["box","left","fragment"],[])
   [Header 2 ("",["left"],[]) []
   ,Div ("",[],[])
    [Plain [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/tragedy1.png\" alt=\"Ressource: Weidefl\228che\" title=\"fig:\">",RawInline (Format "html") "<figcaption>",Str "Ressource:",Space,Str "Weidefl\228che",RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"]]]]
 ,Div ("",["grow-1","column","column-2"],[])
  [Div ("",["box","center","fragment"],[])
   [Header 2 ("",["center"],[]) []
   ,Div ("",[],[])
    [Plain [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/tragedy2.png\" alt=\"frei verf\252gbar (Nicht-Ausschlie\223barkeit)\" title=\"fig:\">",RawInline (Format "html") "<figcaption>",Str "frei",Space,Str "verf\252gbar",Space,Str "(Nicht-Ausschlie\223barkeit)",RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"]]]]
 ,Div ("",["grow-1","column","column-3"],[])
  [Div ("",["box","right","fragment"],[])
   [Header 2 ("",["right"],[]) []
   ,Div ("",[],[])
    [Plain [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/tragedy3.png\" alt=\"begrenzt (Rivalit\228t)\" title=\"fig:\">",RawInline (Format "html") "<figcaption>",Str "begrenzt",Space,Str "(Rivalit\228t)",RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"]]]]]
,Div ("",["single-column-row"],[])
 [Div ("",["box","bottom","fragment"],[])
  [Header 2 ("",["bottom"],[]) []
  ,Para [Str "Traditionelle",Space,Str "L\246sung"]
  ,BlockQuote
   [BulletList
    [[Plain [Str "Staat",Space,Str "(z.",Space,Str "B.",Space,Str "Grenzwerte,",Space,Str "Naturschutzgebiete)"]]
    ,[Plain [Str "Markt",Space,Str "(z.",Space,Str "B.",Space,Str "Emissionsrechtehandel,",Space,Str "Produktsteuer,",Space,Str "Umweltschutzabgabe,",Space,Str "Privatisierung)"]]]]]]
,RawBlock (Format "html") "<aside class=\"notes\">"
,Header 2 ("",[],[]) [Str "Notizen"]
,BulletList
 [[Plain [Str "neben",Space,Str "\220bernutzung",Space,Str "auch",Space,Str "Verschmutzung"]]
 ,[Plain [Str "Tragik",Space,Str "der",Space,Str "Allmende",Space,Str "=",Space,Str "rationale",Space,Str "Individuen",Space,Str "entscheiden",Space,Str "sich",Space,Str "zwingend",Space,Str "und",Space,Str "zum",Space,Str "Schaden",Space,Str "aller,",Space,Str "aus",Space,Str "einer",Space,Str "Allmende",Space,Str "den",Space,Str "gr\246\223ten",Space,Str "pers\246nlichen",Space,Str "Nutzen",Space,Str "zu",Space,Str "ziehen"]]
 ,[Plain [Str "Frage:",Space,Str "Was",Space,Str "k\246nnten",Space,Str "(traditionelle)",Space,Str "L\246sungen",Space,Str "sein?"]]
 ,[Plain [Str "Frage:",Space,Str "Wo",Space,Str "versagen",Space,Str "L\246sungen",Space,Str "durch",Space,Str "Markt",Space,Str "oder",Space,Str "Staat?"]]]
,RawBlock (Format "html") "</aside>"
,HorizontalRule
,Header 1 ("",["sub"],[("layout","columns")]) [Str "Versagen",Space,Str "von",Space,Str "Markt",Space,Str "und",Space,Str "Staat"]
,Div ("",["multi-column-row","multi-column-row-3"],[])
 [Div ("",["grow-1","column","column-1"],[])
  [Div ("",["box","left","fragment"],[])
   [Header 2 ("",["left"],[]) []
   ,Div ("",[],[])
    [Plain [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/Homeless,_Bremen.jpg\" alt=\"Steigende Mietpreise\" title=\"fig:\">",RawInline (Format "html") "<figcaption>",Str "Steigende",Space,Str "Mietpreise",RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"]]]]
 ,Div ("",["grow-1","column","column-2"],[])
  [Div ("",["box","center","fragment"],[])
   [Header 2 ("",["center"],[]) []
   ,Div ("",[],[])
    [Plain [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/736px-Krankenschwester_doku4.jpg\" alt=\"Personalmangel in der Pflege\" title=\"fig:\">",RawInline (Format "html") "<figcaption>",Str "Personalmangel",Space,Str "in",Space,Str "der",Space,Str "Pflege",RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"]]]]
 ,Div ("",["grow-1","column","column-3"],[])
  [Div ("",["box","right","fragment"],[])
   [Header 2 ("",["right"],[]) []
   ,Div ("",[],[])
    [Plain [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/640px-Electronic_waste_at_Agbogbloshie,_Ghana.jpg\" alt=\"Kurze Produktnutzungszyklen\" title=\"fig:\">",RawInline (Format "html") "<figcaption>",Str "Kurze",Space,Str "Produktnutzungszyklen",RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"]]]]]
,Div ("",["single-column-row"],[])
 [Div ("",["box","bottom","fragment","definition"],[])
  [Header 2 ("",["bottom","definition"],[]) []
  ,Para [Str "Alternative:",Space,Strong [Str "Commoning"]]
  ,Para [Emph [Str "selbstorganisierte",Space,Str "Prozesse",Space,Str "des",Space,Str "gemeinsamen,",Space,Str "bed\252rfnisorientierten",Space,Str "Produzierens,",Space,Str "Verwaltens,",Space,Str "Pflegens",Space,Str "und",Space,Str "Nutzens"],Space,Cite [Citation {citationId = "Meretz2018", citationPrefix = [], citationSuffix = [Str ",",Space,Str "S.",Space,Str "156"], citationMode = NormalCitation, citationNoteNum = 0, citationHash = 2}] [Str "(Sutterl\252tti",Space,Str "&",Space,Str "Meretz,",Space,Str "2018,",Space,Str "S.",Space,Str "156)"]]]]
,RawBlock (Format "html") "<aside class=\"notes\">"
,Header 2 ("",[],[]) [Str "Notizen"]
,BulletList
 [[Plain [Str "Commoning",Space,Str "=",Space,Str "Selbstorganisation,",Space,Str "um",Space,Str "die",Space,Str "es",Space,Str "nun",Space,Str "gehen",Space,Str "wird"]]
 ,[Plain [Quoted DoubleQuote [Str "Wann",Space,Str "gelingt",Space,Str "es?"],Space,Str "auch",Space,Str "psych.",Space,Str "Fragestellung"]]]
,RawBlock (Format "html") "</aside>"
,HorizontalRule
,Header 1 ("",["mid"],[("layout","columns")]) [Str "Commons",Space,Str "am",Space,Str "Beispiel:",Space,Emph [Str "Wikipedia"]]
,Div ("",["multi-column-row","multi-column-row-3"],[])
 [Div ("",["grow-1","column","column-1"],[])
  [Div ("",["box","left","fragment"],[])
   [Header 2 ("",["left"],[]) []
   ,Div ("",[],[])
    [Plain [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/Anarchismus_Wikipedia.png\" alt=\"Ressource: Artikel\" title=\"fig:\">",RawInline (Format "html") "<figcaption>",Str "Ressource:",Space,Str "Artikel",RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"]]]]
 ,Div ("",["grow-1","column","column-2"],[])
  [Div ("",["box","center","fragment"],[])
   [Header 2 ("",["center"],[]) []
   ,Div ("",[],[])
    [Plain [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/WikiMUC_in_Aktion.jpg\" alt=\"Community: Wikipedianer\" title=\"fig:\">",RawInline (Format "html") "<figcaption>",Str "Community:",Space,Str "Wikipedianer",RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"]]]]
 ,Div ("",["grow-1","column","column-3"],[])
  [Div ("",["box","right","fragment"],[])
   [Header 2 ("",["right"],[]) []
   ,Div ("",[],[])
    [Plain [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/672px-Rechtssystem_der_deutschsprachigen_Wikipedia.svg.png\" alt=\"Regeln: Reihenfolge der Konfliktbearbeitung\" title=\"fig:\">",RawInline (Format "html") "<figcaption>",Str "Regeln:",Space,Str "Reihenfolge",Space,Str "der",Space,Str "Konfliktbearbeitung",RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"]]]]]
,Div ("",["single-column-row"],[])
 [Div ("",["box","bottom","fragment"],[])
  [Header 2 ("",["bottom"],[]) []
  ,BlockQuote
   [Para [Str "\8222There",Space,Str "is",Space,Str "no",Space,Str "commons",Space,Str "without",Space,Str "commoning.\8220"]
   ,Para [Str "(Peter",Space,Str "Linebaugh)"]]]]
,RawBlock (Format "html") "<aside class=\"notes\">"
,Header 2 ("",[],[]) [Str "Notizen"]
,BulletList
 [[Plain [Quoted DoubleQuote [Str "Commons"],Space,Str "an",Space,Str "einem",Space,Str "bekannten",Space,Str "Beispiel:",Space,Str "Wikipedia"]]
 ,[Plain [Str "zu",Space,Str "Wikipedia",Space,Str "geh\246rt",Space,Str "auch:",Space,Str "regelm.",Space,Str "Treffen,",Space,Str "Umfragen,",Space,Str "Wettbewerbe,",Space,Str "internes",Space,Str "Nachrichtenblatt,",Space,Str "Mentorenprogramm,",Space,Str "Literaturstipendium"]]
 ,[Plain [Str "Commons",Space,Str "=",Space,Str "Ressourcen",Space,Str "+",Space,Str "Community",Space,Str "+",Space,Str "Regeln"]]]
,RawBlock (Format "html") "</aside>"
,HorizontalRule
,Header 1 ("",["sub"],[("layout","columns")]) [Str "Weitere",Space,Str "Beispiele"]
,Div ("",["multi-column-row","multi-column-row-3"],[])
 [Div ("",["grow-1","column","column-1"],[])
  [Div ("",["box","left"],[])
   [Header 2 ("",["left"],[]) []
   ,Para [Link ("",["fragment"],[]) [RawInline (Format "html") "<img data-src=\"img/Mietshaeusersyndikat-Logo.jpg\">"] ("https://www.syndikat.org/",""),Str "\160"]
   ,Para [Link ("",["fragment"],[]) [RawInline (Format "html") "<img data-src=\"img/Buurtzorg-Logo.png\">"] ("https://www.buurtzorg-deutschland.de/",""),Str "\160"]
   ,Para [Link ("",["fragment"],[]) [RawInline (Format "html") "<img data-src=\"img/FabLab-Logo.png\">"] ("https://www.fablab-wuerzburg.de/","")]
   ,Para [Link ("",["fragment"],[]) [RawInline (Format "html") "<img data-src=\"img/Logo-Lastenrad-ohne-256.png\">"] ("https://lastenrad-wuerzburg.de/","")]]]
 ,Div ("",["grow-1","column","column-2"],[])
  [Div ("",["box","center"],[])
   [Header 2 ("",["center"],[]) []
   ,Para [Link ("",["fragment"],[]) [RawInline (Format "html") "<img data-src=\"img/509px-Tux.svg.png\">"] ("https://de.wikipedia.org/wiki/Linux","")]
   ,Para [Link ("",["fragment"],[]) [RawInline (Format "html") "<img data-src=\"img/Solawue.png\">"] ("https://www.solawue.de/","")]]]
 ,Div ("",["grow-1","column","column-3"],[])
  [Div ("",["box","right"],[])
   [Header 2 ("",["right"],[]) []
   ,Para [Link ("",["fragment"],[]) [RawInline (Format "html") "<img data-src=\"img/luftschloss.png\">"] ("https://umsonstladen4wuerzburg.wordpress.com/",""),Str "\160"]
   ,Para [Link ("",["fragment"],[]) [RawInline (Format "html") "<img data-src=\"img/Freifunk.net.svg.png\">"] ("https://freifunk.net/","")]]]]
,HorizontalRule
,Header 1 ("",["xx-dense"],[("layout","columns")]) [Str "Alfred-Nobel-Ged\228chtnispreis",Space,Str "f\252r",Space,Str "Wirtschaftswissenschaften"]
,Div ("",["multi-column-row","multi-column-row-3"],[])
 [Div ("",["grow-1","column","column-1"],[])
  [Div ("",["box","left"],[])
   [Header 2 ("",["left"],[]) []
   ,Div ("",[],[])
    [Plain [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/Nobel_Prize_2009-Press_Conference_KVA-30.jpg\" style=\"height:auto;width:70%;\" alt=\"Elinor Ostrom\" title=\"fig:\">",RawInline (Format "html") "<figcaption>",Str "Elinor",Space,Str "Ostrom",RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"]]]]
 ,Div ("",["grow-1","column","column-3"],[])
  [Div ("",["box","right"],[])
   [Header 2 ("",["right"],[]) []
   ,BlockQuote
    [Para [Str "\8222It",Space,Str "was",Space,Str "long",Space,Str "unanimously",Space,Str "held",Space,Str "among",Space,Str "economists",Space,Str "that",Space,Str "natural",Space,Str "resources",Space,Str "that",Space,Str "were",Space,Str "collectively",Space,Str "used",Space,Str "by",Space,Str "their",Space,Str "users",Space,Str "would",Space,Str "be",Space,Str "over-exploited",Space,Str "and",Space,Str "destroyed",Space,Str "in",Space,Str "the",Space,Str "long-term.",Space,Str "Elinor",Space,Str "Ostrom",Space,Str "disproved",Space,Str "this",Space,Str "idea",Space,Str "by",Space,Str "conducting",Space,Str "field",Space,Str "studies",Space,Str "on",Space,Str "how",Space,Str "people",Space,Str "in",Space,Str "small,",Space,Str "local",Space,Str "communities",Space,Str "manage",Space,Str "shared",Space,Str "natural",Space,Str "resources,",Space,Str "such",Space,Str "as",Space,Str "pastures,",Space,Str "fishing",Space,Str "waters,",Space,Str "and",Space,Str "forests.\8220"]
    ,Para [Cite [Citation {citationId = "Nobelpreis2009", citationPrefix = [], citationSuffix = [], citationMode = NormalCitation, citationNoteNum = 0, citationHash = 3}] [Str "(Nobel",Space,Str "Media",Space,Str "AB,",Space,Str "2020)"]]]]]]
,HorizontalRule
,Header 1 ("",["sub","list-style-type-none"],[("layout","columns")]) [Str "Design-Prinzipien",Space,Str "nach",Space,Cite [Citation {citationId = "Ostrom2010", citationPrefix = [], citationSuffix = [], citationMode = AuthorInText, citationNoteNum = 0, citationHash = 4}] [Str "Ostrom",Space,Str "(2010)"]]
,Div ("",["multi-column-row","multi-column-row-3"],[])
 [Div ("",["grow-1","column","column-1"],[])
  [Div ("",["box","left"],[])
   [Header 2 ("",["left"],[]) []
   ,BlockQuote
    [BulletList
     [[Plain [Span ("",[],[]) [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/noun_boundaries_82980.svg\" alt=\"(1) Nutzer und Ressourcen abgrenzbar\">",RawInline (Format "html") "<figcaption>",Str "(1)",Space,Str "Nutzer",Space,Str "und",Space,Str "Ressourcen",Space,Str "abgrenzbar",RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"],Str "\160"]]
     ,[Plain [Span ("",[],[]) [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/noun_Local_SEO_2077484.svg\" alt=\"(2a) Regeln zu \246rtl. Bedingungen passend\">",RawInline (Format "html") "<figcaption>",Str "(2a)",Space,Str "Regeln",Space,Str "zu",Space,Str "\246rtl.",Space,Str "Bedingungen",Space,Str "passend",RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"],Str "\160"]]
     ,[Plain [Span ("",[],[]) [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/noun_give_and_take_363072.svg\" alt=\"(2b) Verteilung von Kosten und Nutzen proportional\" title=\"fig:\">",RawInline (Format "html") "<figcaption>",Str "(2b)",Space,Str "Verteilung",Space,Str "von",Space,Str "Kosten",Space,Str "und",Space,Str "Nutzen",Space,Str "proportional",RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"]]]]]]]
 ,Div ("",["grow-1","column","column-2"],[])
  [Div ("",["box","center"],[])
   [Header 2 ("",["center"],[]) []
   ,BlockQuote
    [BulletList
     [[Plain [Span ("",[],[]) [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/noun_decision_making_1248549.svg\" alt=\"(3) gemeinschaftliche Entscheidungen\">",RawInline (Format "html") "<figcaption>",Str "(3)",Space,Str "gemeinschaftliche",Space,Str "Entscheidungen",RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"],Str "\160"]]
     ,[Plain [Span ("",[],[]) [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/noun_spying_eye_2198901.svg\" alt=\"(4) Monitoring der Nutzer und der Ressourcen\">",RawInline (Format "html") "<figcaption>",Str "(4)",Space,Str "Monitoring",Space,Str "der",Space,Str "Nutzer",Space,Str "und",Space,Str "der",Space,Str "Ressourcen",RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"],Str "\160"]]
     ,[Plain [Span ("",[],[]) [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/noun_sanction_3031023.svg\" alt=\"(5) abgestufte Sanktionen\" title=\"fig:\">",RawInline (Format "html") "<figcaption>",Str "(5)",Space,Str "abgestufte",Space,Str "Sanktionen",RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"]]]]]]]
 ,Div ("",["grow-1","column","column-3"],[])
  [Div ("",["box","right"],[])
   [Header 2 ("",["right"],[]) []
   ,BlockQuote
    [BulletList
     [[Plain [Span ("",[],[]) [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/noun_mediation_2301878.svg\" alt=\"(6) Mechanismen zur Konfliktl\246sung\">",RawInline (Format "html") "<figcaption>",Str "(6)",Space,Str "Mechanismen",Space,Str "zur",Space,Str "Konfliktl\246sung",RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"],Str "\160"]]
     ,[Plain [Span ("",[],[]) [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/noun_independence_427600.svg\" alt=\"(7) staatliche Anerkennung\">",RawInline (Format "html") "<figcaption>",Str "(7)",Space,Str "staatliche",Space,Str "Anerkennung",RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"],Str "\160"]]
     ,[Plain [Span ("",[],[]) [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/noun_layer_1630360.svg\" alt=\"(8) eingebettete Institutionen (f\252r gro\223e Systeme)\" title=\"fig:\">",RawInline (Format "html") "<figcaption>",Str "(8)",Space,Str "eingebettete",Space,Str "Institutionen",Space,Str "(f\252r",Space,Str "gro\223e",Space,Str "Systeme)",RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"]]]]]]]]
,RawBlock (Format "html") "<aside class=\"notes\">"
,Header 2 ("",[],[]) [Str "Notizen"]
,BulletList
 [[Plain [Str "(1):",Space,Str "vermeidet",Space,Str "Trittbrettfahrer,",Space,Str "Achtung",Space,Str "bei",Space,Str "Gruppenzugeh\246rigkeit,",Space,Quoted DoubleQuote [Str "Staatsb\252rgerschaft"]]]
 ,[Plain [Str "(2a):",Space,Quoted DoubleQuote [Str "Gesetze"]]]
 ,[Plain [Str "(2b):",Space,Str "Fairness-Empfinden"]]
 ,[Plain [Str "(3):",Space,Str "h\246here",Space,Str "Akzeptanz,",Space,Quoted DoubleQuote [Str "Demokratie"]]]
 ,[Plain [Str "(4),",Space,Str "(5),",Space,Str "(6):",Space,Str "Feedback-Kontrolle",Space,Str "f\252r",Space,Str "Ressourcennutzung,",Space,Str "erh\246hen",Space,Str "Robustheit",Space,Str "des",Space,Str "Systems,",Space,Quoted DoubleQuote [Str "Exekutive"],Space,Str "und",Space,Quoted DoubleQuote [Str "Judikative"]]]
 ,[Plain [Str "(7):",Space,Quoted DoubleQuote [Str "Souver\228nit\228t"]]]
 ,[Plain [Str "(8):",Space,Quoted DoubleQuote [Str "F\246deralismus"]]]
 ,[Plain [Str "Quellen:",Space,Cite [Citation {citationId = "Ostrom2004", citationPrefix = [], citationSuffix = [], citationMode = AuthorInText, citationNoteNum = 0, citationHash = 5}] [Str "Anderies,",Space,Str "Janssen",Space,Str "&",Space,Str "Ostrom",Space,Str "(2004)"],Space,Str "sowie",Space,Cite [Citation {citationId = "Ostrom2005", citationPrefix = [], citationSuffix = [], citationMode = AuthorInText, citationNoteNum = 0, citationHash = 6}] [Str "Ostrom",Space,Str "(2005)"]]]]
,RawBlock (Format "html") "</aside>"
,HorizontalRule
,Header 1 ("",[],[("layout","columns")]) [Str "Normative",Space,Str "Grundlagen"]
,Div ("",["multi-column-row","multi-column-row-3"],[])
 [Div ("",["grow-1","column","column-1"],[])
  [Div ("",["box","left"],[])
   [Header 2 ("",["left"],[]) [Str "Freiwilligkeit"]
   ,BulletList
    [[Plain [Str "wer",Space,Str "kommt,",Space,Str "ist",Space,Str "richtig"]]
    ,[Plain [Str "Bed\252rfnisorientierung"]]]]]
 ,Div ("",["grow-1","column","column-3"],[])
  [Div ("",["box","right"],[])
   [Header 2 ("",["right"],[]) [Str "kollektive",Space,Str "Verf\252gung"]
   ,BulletList
    [[Plain [Str "negativ:",Space,Str "niemand",Space,Str "kann",Space,Str "allgemein",Space,Str "von",Space,Str "der",Space,Str "Verf\252gung",Space,Str "ausgeschlossen",Space,Str "werden"]]]]]]
,HorizontalRule
,Header 1 ("",[],[]) [Str "Muster"]
,Div ("",[],[])
 [Plain [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/muster.png\" alt=\"[@Bollier2015]\" title=\"fig:\">",RawInline (Format "html") "<figcaption>",Cite [Citation {citationId = "Bollier2015", citationPrefix = [], citationSuffix = [], citationMode = NormalCitation, citationNoteNum = 0, citationHash = 7}] [Str "(Bollier",Space,Str "&",Space,Str "Helfrich,",Space,Str "2015)"],RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"]]
,HorizontalRule
,Header 1 ("",["sub"],[("layout","columns")]) [Cite [Citation {citationId = "Alexander1977", citationPrefix = [], citationSuffix = [], citationMode = AuthorInText, citationNoteNum = 0, citationHash = 8}] [Str "Alexander",Space,Str "(1977)"],Str ":",Space,Str "A",Space,Str "Pattern",Space,Str "Language"]
,Div ("",["multi-column-row","multi-column-row-3"],[])
 [Div ("",["grow-1","column","column-1"],[])
  [Div ("",["box","left","fragment"],[])
   [Header 2 ("",["left"],[]) [Str "Light",Space,Str "on",Space,Str "Two",Space,Str "Sides",Space,Str "of",Space,Str "Every",Space,Str "Room"]
   ,Para [RawInline (Format "html") "<img data-src=\"img/light.jpg\">"]
   ,Para [Str "(S.",Space,Str "746)"]]]
 ,Div ("",["grow-1","column","column-2"],[])
  [Div ("",["box","center","fragment"],[])
   [Header 2 ("",["center"],[]) [Str "Outdoor",Space,Str "Room"]
   ,Para [RawInline (Format "html") "<img data-src=\"img/outdoor-room.jpg\">"]
   ,Para [Str "(S.",Space,Str "764)"]]]
 ,Div ("",["grow-1","column","column-3"],[])
  [Div ("",["box","right","fragment"],[])
   [Header 2 ("",["right"],[]) [Str "Garden",Space,Str "Growing",Space,Str "Wild"]
   ,Para [RawInline (Format "html") "<img data-src=\"img/garden.jpg\">"]
   ,Para [Str "(S.",Space,Str "801)"]]]]
,HorizontalRule
,Header 1 ("",[],[]) [Str "Triade",Space,Str "des",Space,Str "Commoning"]
,Div ("",[],[])
 [Plain [RawInline (Format "html") "<figure>",RawInline (Format "html") "<img data-src=\"img/triade.png\" alt=\"[@Helfrich2019]\" title=\"fig:\">",RawInline (Format "html") "<figcaption>",Cite [Citation {citationId = "Helfrich2019", citationPrefix = [], citationSuffix = [], citationMode = NormalCitation, citationNoteNum = 0, citationHash = 9}] [Str "(Helfrich",Space,Str "&",Space,Str "Bollier,",Space,Str "2019)"],RawInline (Format "html") "</figcaption>",RawInline (Format "html") "</figure>"]]
,HorizontalRule
,Header 1 ("",["blur-bg"],[("data-background-image","img/PI_4.8_Reflect_C.jpg"),("layout","columns")]) [Str "Soziales",Space]
,Div ("",["multi-column-row","multi-column-row-3"],[])
 [Div ("",["grow-1","column","column-1"],[])
  [Div ("",["box","left","fragment"],[])
   [Header 2 ("",["left"],[]) [Str "Gegenseitigkeit",Space,Str "behutsam",Space,Str "aus\252ben"]]]
 ,Div ("",["grow-1","column","column-2"],[])
  [Div ("",["box","center","fragment"],[])
   [Header 2 ("",["center"],[]) [Str "Konflikte",Space,Str "beziehungswahrend",Space,Str "bearbeiten"]]]
 ,Div ("",["grow-1","column","column-3"],[])
  [Div ("",["box","right","fragment"],[])
   [Header 2 ("",["right"],[]) [Str "Eigene",Space,Str "Governance",Space,Str "reflektieren"]
   ,BulletList
    [[Plain [Str "soziale",Space,Str "Dynamiken",Space,Str "reflektieren"]]
    ,[Plain [Quoted DoubleQuote [Str "there",Space,Str "is",Space,Str "no",Space,Str "such",Space,Str "thing",Space,Str "as",Space,Str "a",Space,Str "structureless",Space,Str "group"],Space,Cite [Citation {citationId = "Freeman2013", citationPrefix = [], citationSuffix = [], citationMode = NormalCitation, citationNoteNum = 0, citationHash = 10}] [Str "(Freeman,",Space,Str "2013)"]]]]]]]
,HorizontalRule
,Header 1 ("",["blur-bg"],[("data-background-image","img/PI_5.2_Membranes_C.jpg"),("layout","columns")]) [Str "Selbstorganisation",Space]
,Div ("",["multi-column-row","multi-column-row-3"],[])
 [Div ("",["grow-1","column","column-1"],[])
  [Div ("",["box","left","fragment"],[])
   [Header 2 ("",["left"],[]) [Str "Commons",Space,Str "mit",Space,Str "halbdurchl\228ssigen",Space,Str "Membranen",Space,Str "umgeben"]]]
 ,Div ("",["grow-1","column","column-2"],[])
  [Div ("",["box","center","fragment"],[])
   [Header 2 ("",["center"],[]) [Str "Wissen",Space,Str "gro\223z\252gig",Space,Str "weitergeben"]]]
 ,Div ("",["grow-1","column","column-3"],[])
  [Div ("",["box","right","fragment"],[])
   [Header 2 ("",["right"],[]) [Str "Gemeinstimmig",Space,Str "entscheiden"]]]]
,HorizontalRule
,Header 1 ("",["blur-bg"],[("data-background-image","img/PI_6.6_Divide_Up_C.jpg"),("layout","columns")]) [Str "Wirtschaft",Space]
,Div ("",["multi-column-row","multi-column-row-3"],[])
 [Div ("",["grow-1","column","column-1"],[])
  [Div ("",["box","left","fragment"],[])
   [Header 2 ("",["left"],[]) [Str "Kreativ",Space,Str "anpassen",Space,Str "&",Space,Str "erneuern"]]]
 ,Div ("",["grow-1","column","column-2"],[])
  [Div ("",["box","center","fragment"],[])
   [Header 2 ("",["center"],[]) [Str "Das",Space,Str "Produktionsrisiko",Space,Str "gemeinsam",Space,Str "tragen"]]]
 ,Div ("",["grow-1","column","column-3"],[])
  [Div ("",["box","right","fragment"],[])
   [Header 2 ("",["right"],[]) [Str "Poolen,",Space,Str "deckeln",Space,Str "&",Space,Str "aufteilen"]]]]
,HorizontalRule
,Header 1 ("",["blur-bg"],[("data-background-image","img/PI_5.4_Share_Knowledge_C.jpg"),("layout","columns")]) [Str "Was",Space,Str "tun?",Space]
,Div ("",["multi-column-row","multi-column-row-3"],[])
 [Div ("",["grow-1","column","column-1"],[])
  [Div ("",["box","left","fragment","list-style-type-none"],[])
   [Header 2 ("",["left","list-style-type-none"],[]) [Str "Wissen",Space,Str "gro\223z\252gig",Space,Str "teilen"]
   ,BulletList
    [[Plain [Str "Freie",Space,Str "Lizenzen",Space,Str "verwenden:",Space,Str "Copyleft",Space,Str "(z.",Space,Str "B.",Space,Str "Creative",Space,Str "Commons)\160"]]
    ,[Plain [RawInline (Format "html") "<img data-src=\"https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-sa.svg\">"]]]]]
 ,Div ("",["grow-1","column","column-2"],[])
  [Div ("",["box","center","fragment"],[])
   [Header 2 ("",["center"],[]) [Str "Kreativ",Space,Str "anpassen",Space,Str "&",Space,Str "erneuern"]
   ,Para [Str "Reparieren",Space,Str "statt",Space,Str "wegwerfen!"]]]
 ,Div ("",["grow-1","column","column-3"],[])
  [Div ("",["box","right","fragment"],[])
   [Header 2 ("",["right"],[]) [Str "Eigene",Space,Str "Governance",Space,Str "reflektieren"]
   ,BulletList
    [[Plain [Link ("",[],[]) [Str "Progressive",Space,Str "Clock"] ("https://timeoff.intertwinkles.org/","")]]
    ,[Plain [Quoted DoubleQuote [Str "Machtanalyse"]]]]]]]
,HorizontalRule
,Header 1 ("",["sub"],[]) [Str "Progressive",Space,Str "Clock"]
,RawBlock (Format "html") "<iframe class=\"stretch\" src=\"https://timeoff.intertwinkles.org/\">"
,RawBlock (Format "html") "</iframe>"
,HorizontalRule
,Header 1 ("",["xx-dense"],[]) [Str "Abbildungsverzeichnis"]
,BulletList
 [[Plain [Str "Tragik",Space,Str "der",Space,Str "Allmende:",Space,Cite [Citation {citationId = "Friedland2012", citationPrefix = [], citationSuffix = [], citationMode = AuthorInText, citationNoteNum = 0, citationHash = 11}] [Str "Friedland",Space,Str "(2012)"],Str ",",Space,Str "S.",Space,Str "263"]]
 ,[Plain [Link ("",[],[]) [Str "Muntaka",Space,Str "Chasant"] ("https://commons.wikimedia.org/wiki/User:Synth85",""),Str ",",Space,Link ("",[],[]) [Str "Electronic",Space,Str "waste",Space,Str "at",Space,Str "Agbogbloshie,",Space,Str "Ghana"] ("https://commons.wikimedia.org/wiki/File:Electronic_waste_at_Agbogbloshie,_Ghana.jpg",""),Str ",",Space,Link ("",[],[]) [Str "CC",Space,Str "BY-SA",Space,Str "4.0"] ("https://creativecommons.org/licenses/by-sa/4.0/","")]]
 ,[Plain [Link ("",[],[]) [Str "Peronimo"] ("https://www.flickr.com/photos/peronimo/with/13900277195/",""),Str ",",Space,Link ("",[],[]) [Str "Homeless,",Space,Str "Bremen",Space,Str "(2014)"] ("https://commons.wikimedia.org/wiki/File:Homeless,_Bremen_%282014%29.jpg",""),Str ",",Space,Link ("",[],[]) [Str "CC",Space,Str "BY",Space,Str "2.0"] ("https://creativecommons.org/licenses/by/2.0/","")]]
 ,[Plain [Link ("",[],[]) [Str "Produnis"] ("http://www.pflegewiki.de/wiki/Benutzer:Produnis",""),Str ",",Space,Link ("",[],[]) [Str "Krankenschwester",Space,Str "doku4"] ("https://commons.wikimedia.org/wiki/File:Krankenschwester_doku4.jpg",""),Str ",",Space,Link ("",[],[]) [Str "CC",Space,Str "BY-SA",Space,Str "3.0"] ("https://creativecommons.org/licenses/by-sa/3.0/","")]]
 ,[Plain [Link ("",[],[]) [Str "Burkhard",Space,Str "M\252cke"] ("https://commons.wikimedia.org/wiki/User:Pimpinellus",""),Str ",",Space,Link ("",[],[]) [Str "WikiMUC",Space,Str "in",Space,Str "Aktion"] ("https://commons.wikimedia.org/wiki/File:WikiMUC_in_Aktion.jpg",""),Str ",",Space,Link ("",[],[]) [Str "CC",Space,Str "BY-SA",Space,Str "4.0"] ("https://creativecommons.org/licenses/by-sa/4.0/","")]]
 ,[Plain [Link ("",[],[]) [Str "Madden"] ("https://commons.wikimedia.org/wiki/User:Madden",""),Space,Str "&",Space,Str "Indoor-Fanatiker",Space,Str "&",Space,Link ("",[],[]) [Str "Ianusius"] ("https://commons.wikimedia.org/wiki/User:Ianusius",""),Str ",",Space,Link ("",[],[]) [Str "Rechtssystem",Space,Str "der",Space,Str "deutschsprachigen",Space,Str "Wikipedia"] ("https://commons.wikimedia.org/wiki/File:Rechtssystem_der_deutschsprachigen_Wikipedia.svg",""),Str ",",Space,Str "marked",Space,Str "as",Space,Str "public",Space,Str "domain,",Space,Str "more",Space,Str "details",Space,Str "on",Space,Link ("",[],[]) [Str "Wikimedia",Space,Str "Commons"] ("https://commons.wikimedia.org/wiki/Template:PD-self","")]]
 ,[Plain [Link ("",[],[]) [Str "Holger",Space,Str "Motzkau"] ("https://commons.wikimedia.org/wiki/User:Prolineserver",""),Space,Str "2010,",Space,Link ("",[],[]) [Str "Nobel",Space,Str "Prize",Space,Str "2009-Press",Space,Str "Conference",Space,Str "KVA-30"] ("https://commons.wikimedia.org/wiki/File:Nobel_Prize_2009-Press_Conference_KVA-30.jpg",""),Str ",",Space,Link ("",[],[]) [Str "CC",Space,Str "BY-SA",Space,Str "3.0"] ("https://creativecommons.org/licenses/by-sa/3.0/","")]]
 ,[Plain [Link ("",[],[]) [Str "Roleplay"] ("https://thenounproject.com/roleplay",""),Space,Str "from",Space,Str "the",Space,Str "Noun",Space,Str "Project,",Space,Link ("",[],[]) [Str "boundaries"] ("https://thenounproject.com/term/boundaries/82980",""),Str ",",Space,Link ("",[],[]) [Str "CC",Space,Str "BY",Space,Str "3.0",Space,Str "US"] ("https://creativecommons.org/licenses/by/3.0/us/","")]]
 ,[Plain [Link ("",[],[]) [Str "supalerk",Space,Str "laipawat"] ("https://thenounproject.com/photo3idea",""),Space,Str "from",Space,Str "the",Space,Str "Noun",Space,Str "Project,",Space,Link ("",[],[]) [Str "Local",Space,Str "SEO"] ("https://thenounproject.com/term/local-seo/2077484",""),Str ",",Space,Link ("",[],[]) [Str "CC",Space,Str "BY",Space,Str "3.0",Space,Str "US"] ("https://creativecommons.org/licenses/by/3.0/us/","")]]
 ,[Plain [Link ("",[],[]) [Str "Alfredo",Space,Str "@",Space,Str "IconsAlfredo.com"] ("https://thenounproject.com/AlfredoCreates",""),Space,Str "from",Space,Str "the",Space,Str "Noun",Space,Str "Project,",Space,Link ("",[],[]) [Str "give",Space,Str "and",Space,Str "take"] ("https://thenounproject.com/term/give-and-take/363072",""),Str ",",Space,Link ("",[],[]) [Str "CC",Space,Str "BY",Space,Str "3.0",Space,Str "US"] ("https://creativecommons.org/licenses/by/3.0/us/","")]]
 ,[Plain [Link ("",[],[]) [Str "Delwar",Space,Str "Hossain"] ("https://thenounproject.com/delwar",""),Space,Str "from",Space,Str "the",Space,Str "Noun",Space,Str "Project,",Space,Link ("",[],[]) [Str "decision",Space,Str "making"] ("https://thenounproject.com/term/decision-making/1248549",""),Str ",",Space,Link ("",[],[]) [Str "CC",Space,Str "BY",Space,Str "3.0",Space,Str "US"] ("https://creativecommons.org/licenses/by/3.0/us/","")]]
 ,[Plain [Link ("",[],[]) [Str "ProSymbols"] ("https://thenounproject.com/prosymbols",""),Space,Str "from",Space,Str "the",Space,Str "Noun",Space,Str "Project,",Space,Link ("",[],[]) [Str "spying",Space,Str "eye"] ("https://thenounproject.com/term/spying-eye/2198901/",""),Str ",",Space,Link ("",[],[]) [Str "CC",Space,Str "BY",Space,Str "3.0",Space,Str "US"] ("https://creativecommons.org/licenses/by/3.0/us/","")]]
 ,[Plain [Link ("",[],[]) [Str "priyanka"] ("https://thenounproject.com/creativepriyanka",""),Space,Str "from",Space,Str "the",Space,Str "Noun",Space,Str "Project,",Space,Link ("",[],[]) [Str "sanction"] ("https://thenounproject.com/term/sanction/3031023",""),Str ",",Space,Link ("",[],[]) [Str "CC",Space,Str "BY",Space,Str "3.0",Space,Str "US"] ("https://creativecommons.org/licenses/by/3.0/us/","")]]
 ,[Plain [Link ("",[],[]) [Str "Nithinan",Space,Str "Tatah"] ("https://thenounproject.com/noomtah",""),Space,Str "from",Space,Str "the",Space,Str "Noun",Space,Str "Project,",Space,Link ("",[],[]) [Str "mediation"] ("https://thenounproject.com/term/mediation/2301878",""),Str ",",Space,Link ("",[],[]) [Str "CC",Space,Str "BY",Space,Str "3.0",Space,Str "US"] ("https://creativecommons.org/licenses/by/3.0/us/","")]]
 ,[Plain [Link ("",[],[]) [Str "Yu",Space,Str "luck"] ("https://thenounproject.com/yuluck",""),Space,Str "from",Space,Str "the",Space,Str "Noun",Space,Str "Project,",Space,Link ("",[],[]) [Str "independence"] ("https://thenounproject.com/term/independence/427600",""),Str ",",Space,Link ("",[],[]) [Str "CC",Space,Str "BY",Space,Str "3.0",Space,Str "US"] ("https://creativecommons.org/licenses/by/3.0/us/","")]]
 ,[Plain [Link ("",[],[]) [Str "Stepan",Space,Str "Voevodin"] ("https://thenounproject.com/voevodin.stepan",""),Space,Str "from",Space,Str "the",Space,Str "Noun",Space,Str "Project,",Space,Link ("",[],[]) [Str "layer"] ("https://thenounproject.com/term/layer/1630360",""),Str ",",Space,Link ("",[],[]) [Str "CC",Space,Str "BY",Space,Str "3.0",Space,Str "US"] ("https://creativecommons.org/licenses/by/3.0/us/","")]]
 ,[Plain [Link ("",[],[]) [Str "Pattern",Space,Str "Illustrations"] ("https://www.freefairandalive.org/patterns-illustrations/",""),Str ",",Space,Str "Merc\232",Space,Str "Moreno",Space,Str "Tarr\233s,",Space,Link ("",[],[]) [Str "Peer",Space,Str "Production",Space,Str "License"] ("https://wiki.p2pfoundation.net/Peer_Production_%20License","")]]]
,HorizontalRule
,Header 1 ("",["unnumbered","xx-dense"],[]) [Str "Quellen"]
,Div ("refs",["references"],[])
 [Div ("ref-Alexander1977",[],[])
  [Para [Str "Alexander,",Space,Str "C.",Space,Str "(1977).",Space,Emph [Str "A",Space,Str "Pattern",Space,Str "Language:",Space,Str "Towns,",Space,Str "Buildings,",Space,Str "Construction"],Space,Str "(Center",Space,Str "for",Space,Str "Environmental",Space,Str "Structure",Space,Str "Series).",Space,Str "Oxford",Space,Str "University",Space,Str "Press."]]
 ,Div ("ref-Ostrom2004",[],[])
  [Para [Str "Anderies,",Space,Str "J.",Space,Str "M.,",Space,Str "Janssen,",Space,Str "M.",Space,Str "A.",Space,Str "&",Space,Str "Ostrom,",Space,Str "E.",Space,Str "(2004).",Space,Str "A",Space,Str "Framework",Space,Str "to",Space,Str "Analyze",Space,Str "the",Space,Str "Robustness",Space,Str "of",Space,Str "Social-ecological",Space,Str "Systems",Space,Str "from",Space,Str "an",Space,Str "Institutional",Space,Str "Perspective.",Space,Emph [Str "Ecology",Space,Str "and",Space,Str "Society"],Str ",",Space,Emph [Str "9"],Str "(1),",Space,Str "art18.",Space,Str "https://doi.org/",Link ("",[],[]) [Str "10.5751/ES-00610-090118"] ("https://doi.org/10.5751/ES-00610-090118","")]]
 ,Div ("ref-Bollier2015",[],[])
  [Para [Str "Bollier,",Space,Str "D.",Space,Str "&",Space,Str "Helfrich,",Space,Str "S.",Space,Str "(2015).",Space,Emph [Str "Patterns",Space,Str "of",Space,Str "Commoning"],Str ".",Space,Str "Commons",Space,Str "Strategies",Space,Str "Group.",Space,Str "Verf\252gbar",Space,Str "unter:",Space,Link ("",[],[]) [Str "http://patternsofcommoning.org/"] ("http://patternsofcommoning.org/","")]]
 ,Div ("ref-Freeman2013",[],[])
  [Para [Str "Freeman,",Space,Str "J.",Space,Str "(2013).",Space,Str "The",Space,Str "Tyranny",Space,Str "of",Space,Str "Structurelessness.",Space,Emph [Str "WSQ:",Space,Str "Womens",Space,Str "Studies",Space,Str "Quarterly"],Str ",",Space,Emph [Str "41"],Str "(3-4),",Space,Str "231\8211\&246.",Space,Str "Project",Space,Str "Muse.",Space,Str "https://doi.org/",Link ("",[],[]) [Str "10.1353/wsq.2013.0072"] ("https://doi.org/10.1353/wsq.2013.0072","")]]
 ,Div ("ref-Friedland2012",[],[])
  [Para [Str "Friedland,",Space,Str "A.",Space,Str "(2012).",Space,Emph [Str "Environmental",Space,Str "science",Space,Str ":",Space,Str "foundations",Space,Str "and",Space,Str "applications"],Str ".",Space,Str "New",Space,Str "York,",Space,Str "NY",Space,Str "Houndmills,",Space,Str "Basingstoke,",Space,Str "England:",Space,Str "W.H.",Space,Str "Freeman;",Space,Str "Company."]]
 ,Div ("ref-Hardin1968",[],[])
  [Para [Str "Hardin,",Space,Str "G.",Space,Str "(1968).",Space,Str "The",Space,Str "Tragedy",Space,Str "of",Space,Str "the",Space,Str "Commons.",Space,Emph [Str "Science"],Str ",",Space,Emph [Str "162"],Str "(3859),",Space,Str "1243\8211\&1248.",Space,Str "American",Space,Str "Association",Space,Str "for",Space,Str "the",Space,Str "Advancement",Space,Str "of",Space,Str "Science.",Space,Str "https://doi.org/",Link ("",[],[]) [Str "10.1126/science.162.3859.1243"] ("https://doi.org/10.1126/science.162.3859.1243","")]]
 ,Div ("ref-Helfrich2019",[],[])
  [Para [Str "Helfrich,",Space,Str "S.",Space,Str "&",Space,Str "Bollier,",Space,Str "D.",Space,Str "(2019).",Space,Emph [Str "Frei,",Space,Str "fair",Space,Str "und",Space,Str "lebendig",Space,Str "-",Space,Str "Die",Space,Str "Macht",Space,Str "der",Space,Str "Commons"],Str ".",Space,Str "transcript",Space,Str "Verlag.",Space,Str "https://doi.org/",Link ("",[],[]) [Str "10.14361/9783839445303"] ("https://doi.org/10.14361/9783839445303","")]]
 ,Div ("ref-Nobelpreis2009",[],[])
  [Para [Str "Nobel",Space,Str "Media",Space,Str "AB.",Space,Str "(2020).",Space,Str "Elinor",Space,Str "Ostrom",Space,Str "\8211",Space,Str "Facts.",Space,Emph [Str "NobelPrize.org"],Str ".",Space,Str "Zugriff",Space,Str "am",Space,Str "14.1.2020.",Space,Str "Verf\252gbar",Space,Str "unter:",Space,Link ("",[],[]) [Str "https://www.nobelprize.org/prizes/economic-sciences/2009/ostrom/facts/"] ("https://www.nobelprize.org/prizes/economic-sciences/2009/ostrom/facts/","")]]
 ,Div ("ref-Ostrom2005",[],[])
  [Para [Str "Ostrom,",Space,Str "E.",Space,Str "(2005).",Space,Emph [Str "Understanding",Space,Str "institutional",Space,Str "diversity"],Space,Str "(Princeton",Space,Str "paperbacks).",Space,Str "Princeton:",Space,Str "Princeton",Space,Str "University",Space,Str "Press."]]
 ,Div ("ref-Ostrom2010",[],[])
  [Para [Str "Ostrom,",Space,Str "E.",Space,Str "(2010).",Space,Str "Beyond",Space,Str "Markets",Space,Str "and",Space,Str "States:",Space,Str "Polycentric",Space,Str "Governance",Space,Str "of",Space,Str "Complex",Space,Str "Economic",Space,Str "Systems.",Space,Emph [Str "American",Space,Str "Economic",Space,Str "Review"],Str ",",Space,Emph [Str "100"],Str "(3),",Space,Str "641\8211\&72.",Space,Str "https://doi.org/",Link ("",[],[]) [Str "10.1257/aer.100.3.641"] ("https://doi.org/10.1257/aer.100.3.641","")]]
 ,Div ("ref-Meretz2018",[],[])
  [Para [Str "Sutterl\252tti,",Space,Str "S.",Space,Str "&",Space,Str "Meretz,",Space,Str "S.",Space,Str "(2018).",Space,Emph [Str "Kapitalismus",Space,Str "aufheben:",Space,Str "eine",Space,Str "Einladung,",Space,Str "\252ber",Space,Str "Utopie",Space,Str "und",Space,Str "Transformation",Space,Str "neu",Space,Str "nachzudenken"],Space,Str "(Beitr\228ge",Space,Str "zur",Space,Str "kritischen",Space,Str "Transformationsforschung).",Space,Str "Hamburg:",Space,Str "VSA:",Space,Str "Verlag."]]]]