---
author: Florian Kohrt
date: 14. Januar 2020 [![](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-sa.svg)](https://creativecommons.org/licenses/by-sa/4.0/)
title: Commoning
subtitle: Seminar "Intervention und Organisation"
lang: de
bibliography: commons.bib
csl: deutsche-gesellschaft-fur-psychologie.csl
history: true
slideNumber: true
css: styles.css
---

# Die "Tragik der Allmende" {layout="columns"}

## {.top}

nach @Hardin1968

## {.left .fragment}

![Ressource: Weidefläche](img/tragedy1.png)

## {.center .fragment}

![frei verfügbar (Nicht-Ausschließbarkeit)](img/tragedy2.png)

## {.right .fragment}

![begrenzt (Rivalität)](img/tragedy3.png)

## {.bottom .fragment}

Traditionelle Lösung

> - Staat (z. B. Grenzwerte, Naturschutzgebiete)
> - Markt (z. B. Emissionsrechtehandel, Produktsteuer, Umweltschutzabgabe, Privatisierung)

# Notizen {.notes}

- neben Übernutzung auch Verschmutzung
- Tragik der Allmende = rationale Individuen entscheiden sich zwingend und zum Schaden aller, aus einer Allmende den größten persönlichen Nutzen zu ziehen
- Frage: Was könnten (traditionelle) Lösungen sein?
- Frage: Wo versagen Lösungen durch Markt oder Staat?

# Versagen von Markt und Staat {.sub layout="columns"}

## {.left .fragment}

![Steigende Mietpreise](img/Homeless,_Bremen.jpg)

## {.center .fragment}

![Personalmangel in der Pflege](img/736px-Krankenschwester_doku4.jpg)

## {.right .fragment}

![Kurze Produktnutzungszyklen](img/640px-Electronic_waste_at_Agbogbloshie,_Ghana.jpg)

## {.bottom .fragment .definition}

Alternative: **Commoning**

_selbstorganisierte Prozesse des gemeinsamen, bedürfnisorientierten Produzierens, Verwaltens, Pflegens und Nutzens_ [@Meretz2018, S. 156]

# Notizen {.notes}

- Commoning = Selbstorganisation, um die es nun gehen wird
- "Wann gelingt es?" auch psych. Fragestellung

# Commons am Beispiel: _Wikipedia_ {layout="columns" .mid}

## {.left .fragment}

![Ressource: Artikel](img/Anarchismus_Wikipedia.png)

## {.center .fragment}

![Community: Wikipedianer](img/WikiMUC_in_Aktion.jpg)

## {.right .fragment}

![Regeln: Reihenfolge der Konfliktbearbeitung](img/672px-Rechtssystem_der_deutschsprachigen_Wikipedia.svg.png)

## {.bottom .fragment}

> „There is no commons without commoning.“
> 
> (Peter Linebaugh)

# Notizen {.notes}

- "Commons" an einem bekannten Beispiel: Wikipedia
- zu Wikipedia gehört auch: regelm. Treffen, Umfragen, Wettbewerbe, internes Nachrichtenblatt, Mentorenprogramm, Literaturstipendium
- Commons = Ressourcen + Community + Regeln

# Weitere Beispiele {.sub layout="columns"}

## {.left}

[![](img/Mietshaeusersyndikat-Logo.jpg)](https://www.syndikat.org/){.fragment}\ 

[![](img/Buurtzorg-Logo.png)](https://www.buurtzorg-deutschland.de/){.fragment}\ 

[![](img/FabLab-Logo.png)](https://www.fablab-wuerzburg.de/){.fragment}

[![](img/Logo-Lastenrad-ohne-256.png)](https://lastenrad-wuerzburg.de/){.fragment}

## {.center}

[![](img/509px-Tux.svg.png)](https://de.wikipedia.org/wiki/Linux){.fragment}

[![](img/Solawue.png)](https://www.solawue.de/){.fragment}

## {.right}

[![](img/luftschloss.png)](https://umsonstladen4wuerzburg.wordpress.com/){.fragment}\ 

[![](img/Freifunk.net.svg.png)](https://freifunk.net/){.fragment}

# Alfred-Nobel-Gedächtnispreis für Wirtschaftswissenschaften {layout="columns" .xx-dense}

## {.left}

![Elinor Ostrom](img/Nobel_Prize_2009-Press_Conference_KVA-30.jpg){width="70%"}

## {.right}

> „It was long unanimously held among economists that natural resources that were collectively used by their users would be over-exploited and destroyed in the long-term. Elinor Ostrom disproved this idea by conducting field studies on how people in small, local communities manage shared natural resources, such as pastures, fishing waters, and forests.“
> 
> [@Nobelpreis2009]

# Design-Prinzipien nach @Ostrom2010 {.sub layout="columns" .list-style-type-none}

## {.left}

> - ![(1) Nutzer und Ressourcen abgrenzbar](img/noun_boundaries_82980.svg)\ 
> - ![(2a) Regeln zu örtl. Bedingungen passend](img/noun_Local_SEO_2077484.svg)\ 
> - ![(2b) Verteilung von Kosten und Nutzen proportional](img/noun_give_and_take_363072.svg)

## {.center}

> - ![(3) gemeinschaftliche Entscheidungen](img/noun_decision_making_1248549.svg)\ 
> - ![(4) Monitoring der Nutzer und der Ressourcen](img/noun_spying_eye_2198901.svg)\ 
> - ![(5) abgestufte Sanktionen](img/noun_sanction_3031023.svg)

## {.right}

> - ![(6) Mechanismen zur Konfliktlösung](img/noun_mediation_2301878.svg)\ 
> - ![(7) staatliche Anerkennung](img/noun_independence_427600.svg)\ 
> - ![(8) eingebettete Institutionen (für große Systeme)](img/noun_layer_1630360.svg)

# Notizen {.notes}

- (1): vermeidet Trittbrettfahrer, Achtung bei Gruppenzugehörigkeit, "Staatsbürgerschaft"
- (2a): "Gesetze"
- (2b): Fairness-Empfinden
- (3): höhere Akzeptanz, "Demokratie"
- (4), (5), (6): Feedback-Kontrolle für Ressourcennutzung, erhöhen Robustheit des Systems, "Exekutive" und "Judikative"
- (7): "Souveränität"
- (8): "Föderalismus"
- Quellen: @Ostrom2004 sowie @Ostrom2005

# Normative Grundlagen {layout="columns"}

## Freiwilligkeit {.left}

- wer kommt, ist richtig
- Bedürfnisorientierung

## kollektive Verfügung {.right}

- negativ: niemand kann allgemein von der Verfügung ausgeschlossen werden

# Muster

![[@Bollier2015]](img/muster.png)

# @Alexander1977: A Pattern Language {.sub layout="columns"}

## Light on Two Sides of Every Room {.left .fragment}

![](img/light.jpg)

(S. 746)

## Outdoor Room {.center .fragment}

![](img/outdoor-room.jpg)

(S. 764)

## Garden Growing Wild {.right .fragment}

![](img/garden.jpg)

(S. 801)

# Triade des Commoning

![[@Helfrich2019]](img/triade.png)

# Soziales ![](img/PI_4.8_Reflect_C.jpg) {layout="columns" .blur-bg}

## Gegenseitigkeit behutsam ausüben {.left .fragment}

## Konflikte beziehungswahrend bearbeiten {.center .fragment}

## Eigene Governance reflektieren {.right .fragment}

- soziale Dynamiken reflektieren
- "there is no such thing as a structureless group" [@Freeman2013]

# Selbstorganisation ![](img/PI_5.2_Membranes_C.jpg) {layout="columns" .blur-bg}

## Commons mit halbdurchlässigen Membranen umgeben {.left .fragment}

## Wissen großzügig weitergeben {.center .fragment}

## Gemeinstimmig entscheiden {.right .fragment}

# Wirtschaft ![](img/PI_6.6_Divide_Up_C.jpg) {layout="columns" .blur-bg}

## Kreativ anpassen & erneuern {.left .fragment}

## Das Produktionsrisiko gemeinsam tragen {.center .fragment}

## Poolen, deckeln & aufteilen {.right .fragment}

# Was tun? ![](img/PI_5.4_Share_Knowledge_C.jpg) {layout="columns" .blur-bg}

## Wissen großzügig teilen {.left .fragment .list-style-type-none}

- Freie Lizenzen verwenden: Copyleft (z. B. Creative Commons)\ 
- ![](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-sa.svg)

## Kreativ anpassen & erneuern {.center .fragment}

Reparieren statt wegwerfen!

## Eigene Governance reflektieren {.right .fragment}

- [Progressive Clock](https://timeoff.intertwinkles.org/)
- "Machtanalyse"

# Progressive Clock {.sub}

<iframe class="stretch" src = "https://timeoff.intertwinkles.org/"></iframe>

# Abbildungsverzeichnis {.xx-dense}

- Tragik der Allmende: @Friedland2012, S. 263
- [Muntaka Chasant](https://commons.wikimedia.org/wiki/User:Synth85), [Electronic waste at Agbogbloshie, Ghana](https://commons.wikimedia.org/wiki/File:Electronic_waste_at_Agbogbloshie,_Ghana.jpg), [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
- [Peronimo](https://www.flickr.com/photos/peronimo/with/13900277195/), [Homeless, Bremen (2014)](https://commons.wikimedia.org/wiki/File:Homeless,_Bremen_%282014%29.jpg), [CC BY 2.0](https://creativecommons.org/licenses/by/2.0/)
-  [Produnis](http://www.pflegewiki.de/wiki/Benutzer:Produnis), [Krankenschwester doku4](https://commons.wikimedia.org/wiki/File:Krankenschwester_doku4.jpg), [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)
- [Burkhard Mücke](https://commons.wikimedia.org/wiki/User:Pimpinellus), [WikiMUC in Aktion](https://commons.wikimedia.org/wiki/File:WikiMUC_in_Aktion.jpg), [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
- [Madden](https://commons.wikimedia.org/wiki/User:Madden) & Indoor-Fanatiker & [Ianusius](https://commons.wikimedia.org/wiki/User:Ianusius), [Rechtssystem der deutschsprachigen Wikipedia](https://commons.wikimedia.org/wiki/File:Rechtssystem_der_deutschsprachigen_Wikipedia.svg), marked as public domain, more details on [Wikimedia Commons](https://commons.wikimedia.org/wiki/Template:PD-self)
- [Holger Motzkau](https://commons.wikimedia.org/wiki/User:Prolineserver) 2010, [Nobel Prize 2009-Press Conference KVA-30](https://commons.wikimedia.org/wiki/File:Nobel_Prize_2009-Press_Conference_KVA-30.jpg), [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)
- [Roleplay](https://thenounproject.com/roleplay) from the Noun Project, [boundaries](https://thenounproject.com/term/boundaries/82980),  [CC BY 3.0 US](https://creativecommons.org/licenses/by/3.0/us/)
- [supalerk laipawat](https://thenounproject.com/photo3idea) from the Noun Project, [Local SEO](https://thenounproject.com/term/local-seo/2077484), [CC BY 3.0 US](https://creativecommons.org/licenses/by/3.0/us/)
- [Alfredo @ IconsAlfredo.com](https://thenounproject.com/AlfredoCreates) from the Noun Project, [give and take](https://thenounproject.com/term/give-and-take/363072), [CC BY 3.0 US](https://creativecommons.org/licenses/by/3.0/us/)
- [Delwar Hossain](https://thenounproject.com/delwar) from the Noun Project, [decision making](https://thenounproject.com/term/decision-making/1248549), [CC BY 3.0 US](https://creativecommons.org/licenses/by/3.0/us/)
- [ProSymbols](https://thenounproject.com/prosymbols) from the Noun Project, [spying eye](https://thenounproject.com/term/spying-eye/2198901/), [CC BY 3.0 US](https://creativecommons.org/licenses/by/3.0/us/)
- [priyanka](https://thenounproject.com/creativepriyanka) from the Noun Project, [sanction](https://thenounproject.com/term/sanction/3031023), [CC BY 3.0 US](https://creativecommons.org/licenses/by/3.0/us/)
- [Nithinan Tatah](https://thenounproject.com/noomtah) from the Noun Project, [mediation](https://thenounproject.com/term/mediation/2301878), [CC BY 3.0 US](https://creativecommons.org/licenses/by/3.0/us/)
- [Yu luck](https://thenounproject.com/yuluck) from the Noun Project, [independence](https://thenounproject.com/term/independence/427600), [CC BY 3.0 US](https://creativecommons.org/licenses/by/3.0/us/)
- [Stepan Voevodin](https://thenounproject.com/voevodin.stepan) from the Noun Project, [layer](https://thenounproject.com/term/layer/1630360), [CC BY 3.0 US](https://creativecommons.org/licenses/by/3.0/us/)
- [Pattern Illustrations](https://www.freefairandalive.org/patterns-illustrations/), Mercè Moreno Tarrés, [Peer Production License](https://wiki.p2pfoundation.net/Peer_Production_%20License)

# Quellen {.xx-dense}

