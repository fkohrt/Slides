const { Marp } = require('@marp-team/marp-core')
const container = require('markdown-it-container')

module.exports = {
  // Customize engine on configuration file directly
  engine: opts => new Marp(opts).use(container, 'columns').use(container, 'column').use(container, 'center').use(container, 'right').use(container, 'big').use(container, 'small'),
}

