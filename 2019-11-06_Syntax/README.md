# README

- install NodeJS: `./install_npm.sh`
- add to path
  - `VERSION=v13.0.1`
  - `DISTRO=linux-x64`
  - `export PATH=/usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin:$PATH`
  - `. ~/.profile`
- `npm install @marp-team/marp-cli`
- `npm install markdown-it-container`
- Visualisierung Oberflächen- vs. Tiefenstruktur: https://visl.sdu.dk/visl/de/parsing/automatic/trees.php