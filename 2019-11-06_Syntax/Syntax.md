---
title: Syntax
paginate: true
---

<!-- _paginate: false -->

# Syntax

Florian Kohrt, 5. November 2019

---

## Begriffsklärungen

- Syntax: befasst sich mit Struktur / Ordnung von Sätzen
- Grammatik: Sprachtheorie (Beschreibung der Struktur einer Sprache)
- Satz
  * Intonationseinheit
  * Sinneinheit
  * Formeinheit

---
<style>.columns { column-count: 2; } .column { break-before: column; break-after: column;  } @-moz-document url-prefix() {.column {display: inline-block;} }</style>

### Der Satz als Intonationseinheit

:::: columns
::: column
- Melodie (u. a. Tonhöhe)
- Atemdruck
:::

::: column
* „Du kommst auch mit?“
* „Du kommst auch mit!“
:::
::::

---
<style>.columns { column-count: 2; } .column { break-before: column; break-after: column;  } @-moz-document url-prefix() {.column {display: inline-block;} }</style>

### Der Satz als Sinneinheit

:::: columns
::: column
- Ausdruck einer abgeschlossenen Aussage
:::

::: column
* „Hilfe!“, „Feuer!“
:::
::::

---

### Der Satz als Formeinheit

- Anordnung der Wörter zu Wortgruppen

---

<style>.right { text-align: right; } .big { font-size: 200%; }</style>

#### Verbzweitsatz

::: big
> „Das Loch _ist_ der Grundpfeiler dieser Gesellschaftsordnung“
:::

::: right
Kurt Tucholsky: Zur soziologischen Psychologie der Löcher.
:::

---

#### Verberstsatz

::: big
 > _Entschuldigen_ Sie bitte!
:::

---

#### Verbendsatz

::: big
 > Dass Sie sich das _trauen_!
:::

---
![bg right](Jedermann1.jpg)

## Traditionelle Grammatik

- Was geschieht?
  * Prädikat
- Wer tut oder erleidet etwas?
  * Subjekt
- Wer oder was ist daran beteiligt?
  * Objekt
- Unter welchen Umständen geschieht es?
  * Adverbiale

---
![bg left](Noam_Chomsky_(1977).jpg)

## Generative Grammatik

- Wie bildet ein Sprecher Unerhörtes?
- Universalgrammatik: Prinzipien und Parameter

---
<style>.columns { column-count: 2; } .column { break-before: column; break-after: column;  } @-moz-document url-prefix() {.column {display: inline-block;} }</style>

:::: columns
::: column
### Wortarten
- Nomen
- Adjektiv
- Präposition
- Verb
- Adverb
- Konjunktion
- Artikel
- Partikel
- Pronomen
:::

::: column
### Phrasenarten
- Nominalphrase: das kleine _Kind_
- Verbalphrase: nach Hause _fahren_
- Präpositionalphrase: _mit_ dem Kind
- Adjektivphrase: sehr _klein_
- Adverbphrase: sehr _oft_
:::
::::

---
![bg contain](gesamt.png)

<!--
* S → NP VP

* NP → PRON
* VP → V NP
-->

---

### Phrasenstrukturregeln

- S → NP VP
- NP → PRON
- VP → V NP
- NP → NP KONJ NP
- NP → ART ADJ NOM
- NP → ART ADJ NOM

---

### Oberflächen- und Tiefenstruktur

- Oberflächenstruktur: konkrete Wortfolge
- Tiefenstruktur: abstrakte Ebene, die Oberflächenstruktur zugrunde liegt
- Transformation überführt Tiefenstruktur in Oberflächenstruktur

---
![bg right](void.jpg)

# Eine TS, viele OS

1. Yves ist heute gesprungen.
2. Ist Yves heute gesprungen?
3. Heute ist Yves gesprungen.
4. (dass) Yves heute gesprungen ist.

---
<style>.center { text-align: center; }</style>

# Viele TS, eine OS

::: center
![](Stilbluete_Mehrdeutigkeit.jpg)
:::

---
<style>.columns { column-count: 2; } .column { break-before: column; break-after: column;  } @-moz-document url-prefix() {.column {display: inline-block;} } .small { font-size: 66%; }</style>

## Vielen Dank für eure Aufmerksamkeit!

::::: columns
:::: column
### Literaturquellen
- Ernst, P. (2004). Germanistische Sprachwissenschaft. Wien: WUV. ISBN: 978-3-8252-2541-4
- Dürscheid, C. (2010). Syntax : Grundlagen und Theorien. Göttingen: Vandenhoeck & Ruprecht. ISBN: 978-3-8385-3319-3
::::

:::: column
### Bildquellen
::: small
- Inszenierung Jedermann: [commons.wikimedia.org/wiki/File:Jedermann1.jpg](https://commons.wikimedia.org/wiki/File:Jedermann1.jpg)
- Noam Chomsky: [commons.wikimedia.org/wiki/File:Noam_Chomsky_(1977).jpg](https://commons.wikimedia.org/wiki/File:Noam_Chomsky_(1977).jpg)
- Syntaxbaum: [Eigene Darstellung](https://mermaidjs.github.io/mermaid-live-editor/#/view/eyJjb2RlIjoic3RhdGVEaWFncmFtXG4gICAgUyA6IGVyIHRyw6RndCBlaW4gd2Vpw59lcyBoZW1kIHVuZCBlaW5lIGdvbGRlbmUga3Jhd2F0dGVcbiAgICBOUF8xIDogZXJcbiAgICBWUCA6IHRyw6RndCBlaW4gd2Vpw59lcyBoZW1kIHVuZCBlaW5lIGdvbGRlbmUga3Jhd2F0dGVcbiAgICBQUk9OIDogZXJcbiAgICBWIDogdHLDpGd0XG4gICAgTlBfMiA6IGVpbiB3ZWnDn2VzIGhlbWQgdW5kIGVpbmUgZ29sZGVuZSBrcmF3YXR0ZVxuICAgIE5QXzMgOiBlaW4gd2Vpw59lcyBoZW1kXG4gICAgS09OSiA6IHVuZFxuICAgIE5QXzQgOiBlaW5lIGdvbGRlbmUga3Jhd2F0dGVcbiAgICBBUlRfMSA6IGVpblxuICAgIEFESl8xOiB3ZWnDn2VzXG4gICAgTk9NXzEgOiBoZW1kXG4gICAgQVJUXzIgOiBlaW5lXG4gICAgQURKXzI6IGdvbGRlbmVcbiAgICBOT01fMiA6IGtyYXdhdHRlXG4gICAgXG4gICAgUyAtLT4gTlBfMVxuICAgIFMgLS0-IFZQXG4gICAgXG4gICAgTlBfMSAtLT4gUFJPTlxuICAgIFZQIC0tPiBWXG4gICAgVlAgLS0-IE5QXzJcbiAgICBcbiAgICBOUF8yIC0tPiBOUF8zXG4gICAgTlBfMiAtLT4gS09OSlxuICAgIE5QXzIgLS0-IE5QXzRcbiAgICBcbiAgICBOUF8zIC0tPiBBUlRfMVxuICAgIE5QXzMgLS0-IEFESl8xXG4gICAgTlBfMyAtLT4gTk9NXzFcbiAgICBOUF80IC0tPiBBUlRfMlxuICAgIE5QXzQgLS0-IEFESl8yXG4gICAgTlBfNCAtLT4gTk9NXzIiLCJtZXJtYWlkIjp7InRoZW1lIjoiZGVmYXVsdCJ9fQ)
- Leap into the Void: [www.metmuseum.org/art/collection/search/266750](https://www.metmuseum.org/art/collection/search/266750)
- Stilblüte Mehrdeutigkeit: [de.wikipedia.org/wiki/Datei:Stilbluete_Mehrdeutigkeit.jpg](https://de.wikipedia.org/wiki/Datei:Stilbluete_Mehrdeutigkeit.jpg)
:::
::::
:::::

