---
# use recent beamer theme: https://github.com/matze/mtheme
# change blockquote design: https://gitlab.com/fkohrt/bachelorarbeit-code/-/blob/dccbbb05/analysis/assets/manual_assets/blockquote.tex
# add `numbers=none` at `\lstset` inside `assets/metadata/beamer.yml`
# compile with make CSL=apa.csl INCREMENTAL=false beamer
# if revealjs were used, compile with make CSL=apa.csl MULTIPLEX_SOCKET_ID=e3ba682aff0ab360 MULTIPLEX_SOCKET_SECRET=16375159753204345041
title: Reproducible Data Analysis
author: Florian Kohrt
date: November 23, 2021
lang: en
themeoptions:
  - progressbar=foot
  - sectionpage=none
---

# Introduction

## Welcome

::: {.columns align=center}
:::: column
![@excerpt:Scriberia2021](bannerwelcome.pdf)
::::

:::: column
### About me

- Florian Kohrt
- background in Psychology
- starting PhD as part of DFG Priority Programme META-REP
- "A Meta-scientific Program to Analyse and Optimise Replicability in the Behavioral, Social, and Cognitive Sciences" [![](meta-rep.png)](https://www.psy.lmu.de/soz/meta-rep/index.html)
::::
:::

## What is Reproducibility?

::: columns
:::: column
### Definition
> _Reproducibility_ is obtaining consistent results using the same input data; computational steps, methods, and code; and conditions of analysis. This definition is synonymous with "computational reproducibility" [...].
> 
> --- @quotation:NAP2019
::::

. . .

:::: column
### Reproducibility Principles

::::: incremental
> 1. Provide transparency regarding how computational results are produced.
> 2. When writing and releasing research code, aim for ease of (re-)executability.
> 3. Make any code upon which the results rely as deterministic as possible.
> 
> --- @quotation:Krafczyk2021
:::::
::::
:::

::: notes
- independent of user, computer/location and time
- same research question, data, model
:::

## And what is Replicability?

![@excerpt:Scriberia2021](reproducibledefinitiongrid.pdf)

::: notes
__Reproducibility__

- plain text: take as much as original assets as possible and do exactly the same
- reproducibility is about computational correctness, not about scientific correctness

__Replicability__

> _Replicability_ is obtaining consistent results across studies aimed at answering the same scientific question, each of which has obtained its own data.
> 
> --- @quotation:NAP2019

- replicability also involves data collection
- and hardware (reagents, organisms, computer cluster, sensors)

__Comparison__

| Analysis \\ Data | same | different |
|--|--|--|
| same | reproducible | replicable |
| different | robust | generalisable |
:::

## Why?

::: columns
:::: column
> We found that we were able to obtain artifacts from 44% of our sample and were able to __reproduce the findings for 26%__. [...] The results from our survey show [...] much room for improvement for software citation standards, suggesting especially the __need for improved community standards around the use and reuse of software__.
> 
> --- @quotation:Stodden2018
::::

:::: column
> For __13 articles [37%]__ at least one value __could not be reproduced__ despite author assistance. [...] __[S]uboptimal data curation, unclear analysis specification and reporting errors__ can impede analytic reproducibility, undermining the utility of data sharing and the credibility of scientific findings.
> 
> --- @quotation:Hardwicke2018
::::
:::

::: notes
- @documents:Stodden2018 looks into a random sample of 204 articles published in _Science_, which has a policy that requires authors make digital artifacts available upon request
- @documents:Hardwicke2018 looks at articles published in _Cognition_ which has a mandatory open data policy. They only selected articles they determined to have reusable data.

The situation might be better for RCTs [@evidence:Naudet2018], registered reports [@evidence:Obels2020] and articles with open data badges [@evidence:Hardwicke2021].
:::

## Reproducibility Crisis? [@excerpt:Baker2016]

::: columns
:::: column
![](Baker2016-1.pdf)
::::

:::: column
![](Baker2016-2.pdf)
::::
:::

## Other Advantages

![](reasons-reproducibility.png)

# Reproducible Research Tools

## Criteria for Longevity [@documents:Akhlaghi2021]

::: columns
:::: column
1. Completeness
2. Modularity
3. Minimal complexity
4. Scalability
::::

:::: column
5. Verifiable inputs and outputs
6. Recorded history
7. Including narrative that is linked to analysis
8. Free and open-source software (FOSS)
::::
:::

## Overview over Reproducible Research Tools

`\begin{center}`{=latex}![](tutorials_overview.png)`\end{center}`{=latex}

## GUI vs. CLI applications

||GUI|CLI|
|--|--|--|
|Current state|Visual metaphor|Textual description|
|Possible actions|Visible icons and menus|Commands|
|Interaction with|Mouse and keyboard|Always keyboard|
|Example interaction|![Graphical User Interface](Drag-and-drop.png){ width=150px }|![Command Line Interface](cli.png){ width=150px }|
|Description|"Move the files on the desktop to the thumb drive"|`mv -t /mnt/data/Bilder foo bar.txt green.png blubb.wav`|

::: notes
- descriptions of CLI interactions are easier to reproduce
- commands are already a description
- therefore, most reproducible research tools are
:::

## Version Control with git

::: columns
:::: column
![@excerpt:Scriberia2021](versioncontrol.pdf)
::::

:::: column
![](Git-Logo-2Color.eps){ width=50% }

- `git` stores previous versions of files
- Website: [`git-scm.com`](https://git-scm.com/)
- Guide: [Version Control with Git](https://swcarpentry.github.io/git-novice/index.html)
- Interactive tutorial: [LearnGitBranching](https://learngitbranching.js.org/)
- Graphical interface for Windows and macOS: [GitHub Desktop](https://desktop.github.com/)
::::
:::

::: notes
- plain text formats
- small changes
:::

## Environment management with Conda

::: {.columns align=center}
:::: column
![](logo_conda_RGB_inkscape.pdf)

- keeps track of required packages and their versions
- Website: [`conda.io`](https://conda.io/)
::::

:::: column
![`environment.yml` file](environment-yml.png){ width=90% }
::::
:::

::: notes
__Advantages__

- machine-readable documentation of software requirements
- run different versions of the same package
:::

## Matplotlib's dependencies

![@excerpt:Alliez2020](matplotlib-dependencies.pdf)

## Workflow management with Snakemake

::: {.columns align=center}
:::: column
![](snakemake.png){ width=15% }

- performs and monitors a defined sequence of computational tasks
- Website: [`snakemake.github.io`](https://snakemake.github.io/)
::::

:::: column
![`Snakefile`](Snakefile.png)

Execute with

```bash
snakemake -r -p a.upper.txt
```
::::
:::

## Notebooks with Jupyter

::: {.columns align=center}
:::: column
![](Jupyter.pdf){ width=50% }

- conduct exploratory analyses with literal programming
- Website: [`jupyter.org`](https://jupyter.org/)
::::

:::: column
![](jupyterpreview.png)
::::
:::

::: notes
"playing" with data
:::

## Reports with R Markdown

::: columns
:::: column
![](rmarkdown_wizards.png)

- write research articles with literal programming
- Website: [`rmarkdown.rstudio.com`](https://rmarkdown.rstudio.com/)
- can also be used with other programming languages than R (e.g. Python, Julia)
::::

:::: column
![](rmd-echo2.png)

![](rmd-inline.png)
::::
:::

## Containerization with Docker or Singularity

::: {.columns align=center}
:::: column
![](horizontal-logo-monochromatic-white.png)

- Website: [`www.docker.com`](https://www.docker.com/)
::::

:::: column
![](singularity.png)

- Website: [`sylabs.io/singularity/`](https://sylabs.io/singularity/)
::::
:::

## Packaging into one Research Compendium [@documents:Gentleman2007]

![@excerpt:Scriberia2021](researchcompendium.pdf){ height=80% }

## Outlook: Facets of Reproducibility

. . .

::: {.columns .onlytextwidth}
:::: {.column width="25%"}
### ![](folder.pdf){ width=10% } Organization

- version control system
- research data manager
::::

. . .

:::: {.column width="25%"}
### ![](file-text.pdf){ width=10% } Documentation

- literate programming
- electronic lab notebook
- protocols
- data dictionary
::::

. . .

:::: {.column width="25%"}
### ![](play.pdf){ width=10% } Automation

- workflow manager
- dependency management
- testing
- continuous integration
- analysis platforms
::::

. . .

:::: {.column width="25%"}
### ![](share.pdf){ width=10% } Dissemination

- licensing
- citability
- archival
::::
:::

::: notes
- organization: reusable
- automation: repeatable
- documentation: understandable
- dissemination: accessible
:::

# Conclusion

> `{\Huge{}`{=latex}Publish your computer code: it is good enough.`}`{=latex}
> 
> --- @quotation:Barnes2010

::: notes
do not let the perfect be the enemy of the good
:::

## Further Resources

- The Turing Way: [Guide for Reproducible Research](https://the-turing-way.netlify.app/reproducible-research/reproducible-research.html)
- [Tools for reproducible research](https://nbis-reproducible-research.readthedocs.io/)
- [Ten simple rules for making research software more robust](https://doi.org/10.1371/journal.pcbi.1005412)
- [Good enough practices in scientific computing](https://doi.org/10.1371/journal.pcbi.1005510)

## Thanks

![@excerpt:Scriberia2021](bannerthanks.pdf){ height=80% }

## References {.allowframebreaks}

::: {#refs}
:::

## Image Credit

- Illustrations from the Turing Way book dashes created by [Scriberia](http://www.scriberia.co.uk/) for The Turing Way community licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
- R & stats illustrations by \@allison_horst licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
- Icons from the [Feather](https://feathericons.com/) collection licensed under MIT/Expat
- [Drag-and-drop.png](https://commons.wikimedia.org/wiki/File:Drag-and-drop.png) by [Sven](https://commons.wikimedia.org/wiki/User:Sven) licensed under [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)

## About this Presentation

--------- ------------------------------------------------------------------------------------------------
Title     Reproducible Data Analysis
Author    Florian Kohrt
License   [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
Updated   2021-11-23
--------- ------------------------------------------------------------------------------------------------

# Additional topics

## Dealing with Sensitive Data

`\begin{center}`{=latex}![@excerpt:Scriberia2021](sensitive-data.jpg){ height=90% }`\end{center}`{=latex}

## Why share work?

`\begin{center}`{=latex}![@excerpt:Scriberia2021](share-work.jpg){ height=90% }`\end{center}`{=latex}

## Reproducibility Guidelines [@excerpt:Krafczyk2021] {.allowframebreaks}

### 1. Make all artifacts that support published results available, up to legal and ethical barriers.

- use open source license and upload on widely-used platform (e.g. GitLab)
- use long-term storage repository that issues DOIs to share datasets (e.g. Zenodo)
- appropriate licenses for code, data, and text / media
- synthesized version of assets that cannot be fully shared

### 2. Connect published scientific claims to the underlying computational steps and data.

- explain parameters for experiments
- don't require interactivity by the user
- use a scripting language
- provide script that use the software used for analysis

### 3. Specify versions and unique persistent identifiers for all artifacts.

- use version control or explicitly save different versions of code marked with version number
- state the exact unique identifier (e.g. commit hash) of the code that has been used for a particular result

### 4. Declare software dependencies and their versions.
- package the code into Docker or Singularity environment; prepare with Dockerfile
- use a package management system (e.g. Conda)
- use a build system

### 5. Refrain from using hard coded parameters in code.

- parameters and arguments should be accepted from command line or a configuration file
- if parameters are hard-coded, (re)compilation of the code must be part of the master script in case parameters are changed

### 6. Avoid using absolute or hard-coded filepaths in code.

- Start file paths with . or ..
- look for own username of external hard drive references and eliminate them

### 7. Provide clear mechanisms to set and report random seed values.

### 8. Report expected errors and tolerances with any published result that include any uncertainty from software or computational environments.

- assess how much results change e.g. due to parallelisation
- provide a script that verifies the result

### 9. Give implementations for any competing approaches or methods relied upon in the article.

### 10. Use build systems for complex software.

- build in containerized environment

### 11. Provide scripts to reproduce visualizations of results.

- provide scripts that recreate figures and tables

### 12. Disclose resource requirements for computational experiments.

- report the hardware
- report the runtime
