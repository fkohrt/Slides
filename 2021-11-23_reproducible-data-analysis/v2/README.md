# Pandoc + make boilerplate

Write informal, lightweight markup and easily convert to multiple formats.

The [original Markdown specification](https://daringfireball.net/projects/markdown/) by John Gruber says:

> The overriding design goal for Markdown’s formatting syntax is
> to make it as readable as possible.
> The idea is that a Markdown-formatted document should be publishable as-is,
> as plain text, without looking like it’s been marked up
> with tags or formatting instructions.
> 
> [...]
> 
> HTML is a *publishing* format; Markdown is a *writing* format.
> Thus, Markdown's formatting syntax only addresses issues
> that can be conveyed in plain text.

The aim of this project is to ease the conversion to publishing formats.
These publishing formats naturally have more features than plain Markdown.
Pandoc's Markdown is an extension to the original specification.
If your document, however, increasingly looks like code, reconsider your tools.

As text is the primary type of content it's reasonable to expect
that it looks good in the various publication formats.
To ease the process of writing, certain [typewriter habits](https://practicaltypography.com/typewriter-habits.html) are fine,
as they will be fixed in the final formats:

- using straight quotes (`"Zitat"`) instead of curly ones (`„Zitat“`)
- multiple hyphens (`--`) instead of dashes (`–`)
- three periods (`...`) instead of ellipses (`…`)
- non-curly apostrophes (`'`) instead of typographic ones (`’`)

Because single newlines are treated as spaces and
multiple spaces are trimmed to one,
the following useful conventions are fine as well:

- every sentence and clause on its own line ([Semantic Linefeeds](https://rhodesmill.org/brandon/2012/one-sentence-per-line/))
- two spaces between sentences on the same line ([Emacs manual](https://www.gnu.org/software/emacs/manual/html_node/emacs/Sentences.html))

But there are limits to what the typographic enhancements
of markdown processors can achieve. 
Therefore it is adviced to manually make the following substitutions:

- `→` instead of `->`

In case you really care about typography,
you may also insert (narrow) no-break spaces where appropriate:

- no-break spaces where appropriate:
  `Fall 1` (character U+00A0) or `Fall\ 1` (backslash-escaped space)
- narrow no-break spaces between abbreviations (German only):
  `z. B.` (character U+202F) or `z.&#x202f;B.` (HTML entity)

When using the X Window System, the <kbd>Compose</kbd>+<kbd>Space</kbd>+<kbd>Space</kbd> combination
may be used to insert a no-break space.

## Usage

### Prerequisites

1. Run the [R Markdown sandbox](https://gitlab.com/fkohrt/RMarkdown-sandbox) in Binder.
2. Clone this repository: `git clone https://gitlab.com/fkohrt/make-pandoc.git`
3. Install [panflute](https://github.com/sergiocorreia/panflute): `pip install panflute`
   (required by [typography.py](https://github.com/NMarkgraf/typography.py))

### Creating documents

Put all your markdown files and images into `content/`.
In the same directory where the `Makefile` is located,
run `make <target>` with `<target>` being one of the following:

- `all`
- `html`
- `pdf`
- `docx`
- `beamer`
- `revealjs`

Clean the generated files with `make clean`.

### Presentations

Both HTML and PDF presentions can be created.
Per default, level-1 and level-2 headings create new slides,
but [horizontal rules](https://pandoc.org/MANUAL.html#horizontal-rules) work as well.
Speaker notes can be added using Pandoc's fenced divs and the class name
[`notes`](https://pandoc.org/MANUAL.html#speaker-notes):

```md
::: notes
:::
```

For the slide that will show the cited references, it is recommended to add the
class `allowframebreaks` to the heading, so multiple slides are created instead of one overfilled one:

```
# References {.allowframebreaks}

::: {#refs}
:::
```

### Multiplexing presentations

`revealjs` supports real-time updates of "client" presentations by one "master" presentation (called multiplex). Therefore, three `revealjs` files are created:

- default: `*.revealjs.html`
- client: `*.revealjs.client.html`
- master: `*.revealjs.master.html`

Share the "client" presentation during the event (probably with `?controls=false` appended) and the "default" presentation after the event (probably with `?showNotes=true` appended).

For this to work, generate a token at [`reveal-multiplex.glitch.me`](https://reveal-multiplex.glitch.me/) and provide it to the `make` command as follows.

#### Without notes

If you don't have any notes inside your presentation (or don't need them), you can set `HAS_NOTES=false`:

```sh
make MULTIPLEX_SOCKET_ID=<id> MULTIPLEX_SOCKET_SECRET=<secret> HAS_NOTES=false revealjs
```

Your "client" presentation needs to be publicly accessible. You can use [`hostyoself.com`](https://hostyoself.com/) to create a shareable link from inside your browser -- or any other static file server.

Your "master" presentation should only be accessible to you. Because of browser's restrictions when using the `file://` pseudo-protocol, you can spin up a static file server from within your presentation's directory with `python` to open it on your device:

```sh
python3 -m http.server 1947
```

But any other static file server is fine as well, e.g.

```sh
npm install node-static
npx static -p 1947
```

Now navigate to [`localhost:1947`](http://localhost:1947/) in your browser.

#### With notes

Create the presentation files:

```sh
make MULTIPLEX_SOCKET_ID=<id> MULTIPLEX_SOCKET_SECRET=<secret> revealjs
```

As without notes, your "client" presentation can reside on any publicly accessible static file server.

Save your "master" presentation as `index.html` to a separate folder and inside there, install the Node.js package `reveal-notes-server` and run it:

```bash
npm install reveal-notes-server
node node_modules/reveal-notes-server
```

Navigate to [`localhost:1947`](http://localhost:1947/) in your browser.

#### Different socket server

There's a public socket server preconfigured. If you want to use your own, you will need the Node.js package `reveal-multiplex` and run the following from a publicly accessible server:

```sh
npm install reveal-multiplex
node node_modules/reveal-multiplex
```

Generate your ID and secret from your own socket server. Then create your presentation with

```sh
make MULTIPLEX_SOCKET_ID=<id> MULTIPLEX_SOCKET_SECRET=<secret> MULTIPLEX_SOCKET_URL=https://example.org revealjs
```

If you want, upload your "client" presentation to your socket server. Proceed with the "master" presentation as above.

## Customization

Some other settings can be easily changed by calling `make` with arguments. Have a look at the `Makefile` to discover them!
