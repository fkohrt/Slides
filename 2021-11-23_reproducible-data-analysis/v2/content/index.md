---
# use recent beamer theme: https://github.com/matze/mtheme
# change blockquote design: https://gitlab.com/fkohrt/bachelorarbeit-code/-/blob/dccbbb05/analysis/assets/manual_assets/blockquote.tex
# add `numbers=none` at `\lstset` inside `assets/metadata/beamer.yml`
# compile with make CSL=apa.csl INCREMENTAL=false beamer
# if revealjs were used, compile with make CSL=apa.csl MULTIPLEX_SOCKET_ID=e3ba682aff0ab360 MULTIPLEX_SOCKET_SECRET=16375159753204345041
title: Reproducible Data Analysis
author: Florian Kohrt
date: January 27, 2022
lang: en
colorlinks: true
themeoptions:
  - progressbar=foot
  - block=fill
---

# Introduction

## What is Reproducibility?

::: columns
:::: column
### Definition

"_Reproducibility_ is obtaining consistent results using the same input data; computational steps, methods, and code; and conditions of analysis. This definition is synonymous with 'computational reproducibility' [...]." [@quotation:NAP2019]
::::

. . .

:::: column
### Reproducibility Principles [@quotation:Krafczyk2021]

::::: incremental
1. Provide transparency regarding how computational results are produced.
2. When writing and releasing research code, aim for ease of (re-)executability.
3. Make any code upon which the results rely as deterministic as possible.
:::::
::::
:::

::: notes
__Definition__

- plain text: take as much as original assets as possible and do exactly the same
- reproducibility is about computational correctness, not about scientific correctness
- independent of user, computer/location and time
:::

## Why?

::: columns
:::: column
> We found that we were able to obtain artifacts from 44% of our sample and were able to __reproduce the findings for 26%__. [...] The results from our survey show [...] much room for improvement for software citation standards, suggesting especially the __need for improved community standards around the use and reuse of software__.
> 
> --- @quotation:Stodden2018
::::

:::: column
> For __13 articles [37%]__ at least one value __could not be reproduced__ despite author assistance. [...] __[S]uboptimal data curation, unclear analysis specification and reporting errors__ can impede analytic reproducibility, undermining the utility of data sharing and the credibility of scientific findings.
> 
> --- @quotation:Hardwicke2018
::::
:::

::: notes
- @documents:Stodden2018 looks into a random sample of 204 articles published in _Science_, which has a policy that requires authors make digital artifacts available upon request
- @documents:Hardwicke2018 looks at articles published in _Cognition_ which has a mandatory open data policy. They only selected articles they determined to have reusable data.
- other examples are @documents:Eubank2016 and @documents:Ioannidis2009

The situation might be better for RCTs [@evidence:Naudet2018], registered reports [@evidence:Obels2020] and articles with open data badges [@evidence:Hardwicke2021].
:::

## Overview: Facets of Reproducibility

. . .

::: {.columns .onlytextwidth}
:::: {.column width="25%"}
### ![](folder.pdf){ width=10% } Organization

- version control system
- research data manager
::::

. . .

:::: {.column width="25%"}
### ![](file-text.pdf){ width=10% } Documentation

- literate programming
- electronic lab notebook
- protocols
- data dictionary
::::

. . .

:::: {.column width="25%"}
### ![](play.pdf){ width=10% } Automation

- workflow manager
- dependency management
- testing
- continuous integration
- analysis platforms
::::

. . .

:::: {.column width="25%"}
### ![](share.pdf){ width=10% } Dissemination

- licensing
- citability
- archival
::::
:::

::: notes
- organization: reusable
- automation: repeatable
- documentation: understandable
- dissemination: accessible
:::

## GUI vs. CLI applications

||GUI|CLI|
|--|--|--|
|Current state|Visual metaphor|Textual description|
|Possible actions|Visible icons and menus|Commands|
|Interaction with|Mouse and keyboard|Always keyboard|
|Example interaction|![Graphical User Interface](Drag-and-drop.png){ width=150px }|![Command Line Interface](cli.png){ width=150px }|
|Description|"Move the files on the desktop to the thumb drive"|`mv -t /mnt/data/Bilder foo bar.txt green.png blubb.wav`|

::: notes
- descriptions of CLI interactions are easier to reproduce
- commands are already a description
- therefore, most reproducible research tools are CLI applications
:::

# Reproducibility Guidelines [@excerpt:Krafczyk2021] {.allowframebreaks}

## 1. Make all artifacts that support published results available, up to legal and ethical barriers.

::: columns

:::: {.column width="33%"}
### Repository

`\begin{center}`{=latex}

[![Figshare](Figshare_logo.pdf){ width=50% }](https://figshare.com/)

[![Zenodo](zenodo-gradient-round.pdf){ width=50% }](https://zenodo.org/)

[![Software Heritage](software-heritage-logo-title.pdf){ width=50% }](https://www.softwareheritage.org/)

[![Open Science Framework](osf_black.png){ width=50% }](https://osf.io/)

`\end{center}`{=latex}
::::

. . .

:::: {.column width="33%"}
### License

- [CC0 1.0](https://choosealicense.com/licenses/cc0-1.0/)  
  (public domain)
- [Apache License 2.0](https://choosealicense.com/licenses/apache-2.0/)  
  (permissive)
- [AGPL 3.0](https://choosealicense.com/licenses/agpl-3.0/)  
  (copyleft)
::::

. . .

:::: {.column width="33%"}
### Anonymize / Synthesize

`\begin{center}`{=latex}

[![ARX](arx-logo.pdf){ width=25% }](https://arx.deidentifier.org/)

[![Amnesia](amnLogo.pdf){ width=25% }](https://amnesia.openaire.eu/)

[![](synthpop-logo.pdf){ width=25% }](https://www.synthpop.org.uk/)  
Synthpop

`\end{center}`{=latex}
::::

:::

::: notes
- use open source license and upload on widely-used platform (e.g. GitLab)
- use long-term storage repository that issues DOIs to share datasets (e.g. Zenodo)
- appropriate licenses for code, data, and text / media
- synthesized version of assets that cannot be fully shared
:::

## 2. Connect published scientific claims to the underlying computational steps and data.

::: columns

:::: {.column width="33%"}
### Jupyter Notebook

`\begin{center}`{=latex}[![](Jupyter.pdf){ width=25% }](https://jupyter.org/)`\end{center}`{=latex}

- conduct exploratory analyses with literal programming
- supports Python, R, Julia, Scala and other more
::::

. . .

:::: {.column width="33%"}
### R Markdown Report

`\begin{center}`{=latex}[![](rmarkdown.pdf){ width=25% }](https://rmarkdown.rstudio.com/)`\end{center}`{=latex}

- write research articles with literal programming
- can also be used with other programming languages than R (e.g. Python, Julia)
::::

. . .

:::: {.column width="33%"}
### Microsoft Word + StatTag

`\begin{center}`{=latex}[![](stattag-logo.png){ width=33% }](https://sites.northwestern.edu/stattag/)`\end{center}`{=latex}

- embed statistical output (estimates, tables, and figures) within Word
- use R, R Markdown, SAS, Stata or Python
::::
:::

::: notes
- motivation: How has this figure been created? With which data set?
- Jupyter: allows "playing" with data
- explain parameters for experiments
- don't require interactivity by the user
- use a scripting language
- provide script that use the software used for analysis
:::

## Reports with R Markdown

::: {.columns align=center}
:::: column
![](rmarkdown_wizards.png)
::::

:::: column
![](rmd-echo2.png)

![](rmd-inline.png)
::::
:::

## 3. Specify versions and unique persistent identifiers for all artifacts.

`\begin{center}`{=latex}[![](Git-Logo-2Color.eps){ width=25% }](https://git-scm.com/)`\end{center}`{=latex}

::: columns
:::: column
- `git` stores previous versions of files
- Graphical interface for Windows and macOS: [GitHub Desktop](https://desktop.github.com/)
- Follow a tutorial
  - classic: [Version Control with Git](https://swcarpentry.github.io/git-novice/)
  - gamified: [Git In The Trenches](https://cbx33.github.io/gitt/)
  - interactive: [Learn Git Branching](https://learngitbranching.js.org/) and  
    [Oh My Git!](https://ohmygit.org/)
::::

:::: column
- Read about theory
  - [Think Like (a) Git](http://think-like-a-git.net/)
  - [Git from the inside out](https://maryrosecook.com/blog/post/git-from-the-inside-out)
- Find quick fixes
  - [Git Command Explorer](https://gitexplorer.com/)
  - [Oh Shit, Git!?!](https://ohshitgit.com/)
  - [Git Fix Um](https://sukima.github.io/GitFixUm/)
  - [Git Cheatsheet](https://ndpsoftware.com/git-cheatsheet.html)
::::
:::

::: notes
- motivation: analysis doesn't work anymore, restore working state; what has changed since last modification by colleague?
- use version control or explicitly save different versions of code marked with version number
- state the exact unique identifier (e.g. commit hash) of the code that has been used for a particular result
- plain text formats
- small changes
- unique identifiers issued by repository (e.g. DOI, SWHID)
:::

## 4. Declare software dependencies and their versions.

::: columns
:::: column
![](102-implementation.png)
::::

. . .

:::: column
### Environment management with [Conda](https://conda.io/)

- keep track of required packages and their versions
- create environment locally with [repo2docker](https://github.com/jupyterhub/repo2docker) or with a single click online via [`mybinder.org`](https://mybinder.org/)
- all by defining a single `environment.yml` file

### Sample `environment.yml` {.example}

`\begin{center}`{=latex}![](environment-yml.png){ width=33% }`\end{center}`{=latex}
::::
:::

::: notes
__Implementation comic__

- motivation: software changes, gets deprecated and becomes unavailable
- dependencies have dependencies (other R packages, R version, system packages, operating system)
- every dependency is a liability, only add when necessary
- ideally document dependencies machine-readable

__Conda__

- allows declaring system dependencies, Python and R packages
- but not all [R packages available on conda-forge](https://conda-forge.org/feedstock-outputs/)
- run different versions of the same package
- package the code into Docker or Singularity environment; prepare with Dockerfile
- use a build system
:::

## Matplotlib's dependencies

![@excerpt:Alliez2020](matplotlib-dependencies.pdf)

## Obtain R packages from time stamped repository

Use [`checkpoint`](https://cran.r-project.org/package=checkpoint) or [`renv`](https://cran.r-project.org/package=renv) to keep track of dependencies.

```r
checkpoint::checkpoint("2021-07-23")
```

. . .

Install packages from Microsoft R Application Network:

```r
options(repos = c(CRAN = "https://mran.revolutionanalytics.com/snapshot/2021-07-23"))
install.packages(c("tinytex", "rmarkdown"))
```

. . .

Or use the [RStudio Public Package Manager](https://packagemanager.rstudio.com/client/#/repos/1/overview) as source.

## Obtain R packages from own repository

1. Store packages in local repository with [`miniCRAN`](https://cran.r-project.org/package=miniCRAN):

```bash
git clone https://github.com/coolbutuseless/miranda.git
git -C miranda/ checkout 310ee71
Rscript -e 'miniCRAN::addLocalPackage(pkgs = c("miranda"), pkgPath = getwd(), path = file.path("my-r-project", "miniCRAN"), build = TRUE)'
```

. . .

2. Install packages stored in local repository:

```r
install.packages(pkgs = c("miranda"),
                 repos = paste0("file:///", "miniCRAN"))
```

## 5. & 6. No hard-coding.

::: {.columns align=center}
:::: column
::::: incremental
5. Refrain from using hard coded parameters in code.
6. Avoid using absolute or hard-coded filepaths in code.
:::::
::::

:::: column
![](here.png)
::::
:::

::: notes
__Ad 5.__

- parameters and arguments should be accepted from command line or a configuration file
- if parameters are hard-coded, (re)compilation of the code must be part of the master script in case parameters are changed

__Ad 6.__

- Start file paths with . or ..
- look for own username of external hard drive references and eliminate them
:::

## Additional guidelines (1)

::: columns
:::: {.column width="33%"}
7. Provide clear mechanisms to set and report random seed values.
::::

. . .

:::: {.column width="33%"}
8. Report expected errors and tolerances with any published result that include any uncertainty from software or computational environments.
::::

. . .

:::: {.column width="33%"}
9. Give implementations for any competing approaches or methods relied upon in the article.
::::
:::

::: notes
__Ad 8.__

- assess how much results change e.g. due to parallelisation
- provide a script that verifies the result
:::

## Additional guidelines (2)

::: columns
:::: {.column width="33%"}
10. Use build systems for complex software.
    - e.g. [`targets`](https://cran.r-project.org/package=targets)
::::

. . .

:::: {.column width="33%"}
11. Provide scripts to reproduce visualizations of results.
    - e.g. output by [jamovi](https://www.jamovi.org/) or [`esquisse`](https://cran.r-project.org/package=esquisse)
::::

. . .

:::: {.column width="33%"}
12. Disclose resource requirements for computational experiments.
::::
:::

::: notes
__Ad 10.__

- build in containerized environment

__Ad 11.__

- provide scripts that recreate figures and tables

__Add 12.__

- report the hardware
- report the runtime
:::

# Conclusion

> `{\Huge{}`{=latex}Publish your computer code: it is good enough.`}`{=latex}
> 
> --- @quotation:Barnes2010

::: notes
do not let the perfect be the enemy of the good
:::

## Further Resources

- The Turing Way: [Guide for Reproducible Research](https://the-turing-way.netlify.app/reproducible-research/reproducible-research.html)
- [Tools for reproducible research](https://nbis-reproducible-research.readthedocs.io/)
- [Ten simple rules for making research software more robust](https://doi.org/10.1371/journal.pcbi.1005412)
- [Good enough practices in scientific computing](https://doi.org/10.1371/journal.pcbi.1005510)

## Thanks

![](bannerthanks.pdf)

## References {.allowframebreaks}

::: {#refs}
:::

## Image Credit

- Illustrations from the Turing Way book dashes created by [Scriberia](http://www.scriberia.co.uk/) for The Turing Way community licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
- R & stats illustrations by \@allison_horst licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
- Icons from the [Feather](https://feathericons.com/) collection licensed under MIT/Expat
- [Drag-and-drop.png](https://commons.wikimedia.org/wiki/File:Drag-and-drop.png) by [Sven](https://commons.wikimedia.org/wiki/User:Sven) licensed under [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)
- "[Implementation](https://www.monkeyuser.com/2018/implementation/)" by MonkeyUser.com

## About this Presentation

--------- ------------------------------------------------------------------------------------------------
Title     Reproducible Data Analysis
Author    Florian Kohrt
License   [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
Updated   2022-01-27
--------- ------------------------------------------------------------------------------------------------

# Additional topics

## Dealing with Sensitive Data

`\begin{center}`{=latex}![@excerpt:Scriberia2021](sensitive-data.jpg){ height=90% }`\end{center}`{=latex}

## Why share work?

`\begin{center}`{=latex}![@excerpt:Scriberia2021](share-work.jpg){ height=90% }`\end{center}`{=latex}

## Criteria for Longevity

::: columns
:::: column
1. Completeness
2. Modularity
3. Minimal complexity
4. Scalability
::::

:::: column
5. Verifiable inputs and outputs
6. Recorded history
7. Including narrative that is linked to analysis
8. Free and open-source software (FOSS)
::::
:::

Source: @documents:Akhlaghi2021
