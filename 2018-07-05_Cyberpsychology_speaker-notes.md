# Speaker notes

- Slide 1:
```
Imagine: You want to examine someone in a limited amount of time with just a few questions. What would you ask?
```
- Slide 2:
```
We asked ourselves just that. Introduction: Lisa + Florian In the following: a classical approach (= short scales) a new idea (Kosinski’s studies) or: What were we (= Lisa + Florian) interested in (= use the data many people provide to identify the best questions we would ask unknown people) a big data approach (= decision trees)
```
- Slide 3:
```
personality theories based on traits trait = relatively stable pattern of thoughts, feelings and behaviours on which people differ Goldberg: conducted factor analyses on sets of trait adjectives; found support for the “Big Five” factors of personality McCrae & Costa: repeatedly found five dimensions across instruments and observer
```
- Slide 4:
```
1 = disagree strongly 5 = agree strongly ...is reserved. ...is generally trusting. ...tends to be lazy. ...is relaxed, handles stress well. ...has few artistic interests. ...is outgoing, sociable. ...tends to find fault with others. ...does a thorough job. ...gets nervous easily. ...has an active imagination. relevance: validation when assessing personality online limit: describes, but doesn't explain; big five not the only model
```
- Side 14:
```
social network activities substance use network size field of study
values, sensational interests, physical health, depression, self monitoring, impulsivity, life satisfaction, political attitude
```
- Slide 17:
```
 - learn simple decision rules inferred from the data features
 - questions are chosen to maximize information gain at each step
```
- Slide 18:
```
EAR corpus: 96 participants for a total of 97,468 words and 15,269 utterances
```
- Slide 19 (originally showed Shannon's 1948 paper "A Mathematical Theory of Communication"):
```
page 10, chapter 6
entropy = a measure of the unpredictability of a state's average information content
```