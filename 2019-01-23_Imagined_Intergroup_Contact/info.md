# Info

## Bildquellen
- https://de.wikipedia.org/wiki/Datei:Btw13_npd_zweit_endgueltig.svg
- https://de.wikipedia.org/wiki/Datei:Migrationshintergrund_Kreise_Zensus_2011.png
- https://thenounproject.com/term/audience/2096107
- https://thenounproject.com/term/think/159182
- https://thenounproject.com/term/friends/1201432
- https://thenounproject.com/term/neighbor-dispute/1930950
- https://xkcd.com/1478/
- https://commons.wikimedia.org/wiki/File:Nicosia_3_April_2008_14.jpg

### Diagramm
- [mermaid](https://mermaidjs.github.io/): [live editor](https://mermaidjs.github.io/mermaid-live-editor/)
- in SVG-Datei des Sequenzdiagramms
  - `max-width` anpassen, damit Diagramm vollständig angezeigt wird
  - untere Aktoren entfernen

```
sequenceDiagram
    participant c as Kontrolle: 5,77
    participant h as homogen: 6,75
    participant d as divers: 7,05
    c->d: ANOVA: p = 0,05
    Note over h: • „significant“<br />• exakter: p = 0,051
    c-->d: t-Test: p = 0,032
    Note over h: • t-Test<br />• ungerichtet<br />• keine α-Adj.
    h-->c: t-Test: p = 0,09
    Note right of c: • „marginally<br />significantly higher“<br />• gerichtet<br />wäre signifikant
    h-->d: t-Test: p = 0,519
    Note right of h: • erst: „equally high“<br />• später: divers<br />„more powerful“
```

```
graph LR
    VP((VP)) --> C(Spaziergang im Freien)
    C --> T((Test))
    VP --> B(1. Begegnung)
    B --> H(2. Begegnung, Ort und Zeit gleich)
    H --> T
    B --> D(2. Begegnung, Ort und Zeit verändert)
    D --> T
```

## Prez-Veränderungen

### Plugins

- [anything](https://github.com/rajgoel/reveal.js-plugins/tree/master/anything)
- [mapbox-gl](https://github.com/lipov3cz3k/reveal.js-mapbox-gl-plugin): [docs](https://docs.mapbox.com/mapbox-gl-js/style-spec/#paint-fill-fill-color), [Cyprus No Mans Area](https://github.com/arm0th/CountryGeoJSONCollection/blob/master/geojson/CNM.geojson) von [Natural Earth](https://www.naturalearthdata.com/)

### `/usr/lib/node_modules/prez/data/init/js/custom.js`

- the default contains the line `Reveal.configure({"center": false});`, which disables vertical centering of slides; hence empty it

```
// Custom JS code can go here

```

### `/usr/lib/node_modules/prez/node_modules/reveal.js/plugin/mapbox-gl/mapbox-gl.js`
```
        function addTrack(mapbox, trekUrl){
                trekId = trekUrl
                if(mapbox.getLayer(trekId))
                        return;
                try{
                        mapbox.addLayer({
                                "id": trekId,
                                "type": "fill",
                                "source": {
                                        "type": "geojson",
                                        "data": trekUrl
                                },
                                "paint": {
                                        "fill-color": "#33cc33",
                                        "fill-opacity": 0.5,
                                        "fill-outline-color": "#003300"
                                }
                        });
                }catch(err){
                        console.log(err);
                }
        }
```

### `/usr/lib/node_modules/prez/data/index.html`

- verändern der `index.html` [hier](https://github.com/byteclubfr/prez/issues/35) beschrieben

```
    <!-- INCLUDE CSS HERE -->
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.46.0/mapbox-gl.css' rel='stylesheet' />
```

```
    <!-- INCLUDE JS HERE -->
    <script src="https://cdn.jsdelivr.net/npm/mermaid@8.0.0/dist/mermaid.min.js"></script>
```

```
    <script>
    // Full list of configuration options available at:
    // https://github.com/hakimel/reveal.js#configuration
    Reveal.initialize({
      controls: true,
      progress: true,
      history: true,
      center: true,

      transition: Reveal.getQueryHash().transition || 'slide', // none/fade/slide/convex/concave/zoom

      mapbox: {accessToken: "pk.eyJ1IjoibGlwb3YzY3ozayIsImEiOiJjamo3YXlwNTMwdHV1M3FvM3E2dmcxMWZsIn0.9Ax3qI890KLFhFTS6inMEA"},
      anything: [
        {className: "animate",  initialize: (function(container, options){
          Reveal.addEventListener( 'fragmentshown', function( event ) {
            if (typeof event.fragment.beginElement === "function" ) {
              event.fragment.beginElement();
            }
          });
          Reveal.addEventListener( 'fragmenthidden', function( event ) {
            if (event.fragment.hasAttribute('data-reverse') ) {
              var reverse = event.fragment.parentElement.querySelector('[id=\"' + event.fragment.getAttribute('data-reverse') + '\"]');
              if ( reverse && typeof reverse.beginElement === "function" ) {
                reverse.beginElement();
              }
            }
          });
          if ( container.getAttribute("data-svg-src") ) {
            var xhr = new XMLHttpRequest();
            xhr.onload = function() {
              if (xhr.readyState === 4) {
                var svg = container.querySelector('svg');
                  container.removeChild( svg );
                container.innerHTML = xhr.responseText + container.innerHTML;
                if ( svg ) {

var animate = document.createElement("animate");
animate.setAttribute("class", "fragment");
animate.setAttribute("attributeName", "opacity");
animate.setAttribute("repeatCount", "1");
animate.setAttribute("begin", "indefinite");
animate.setAttribute("dur", "0.1s");
animate.setAttribute("from", "0");
animate.setAttribute("to", "1");
animate.setAttribute("fill", "freeze");

var animateBack = document.createElement("animate");
animateBack.setAttribute("attributeName", "opacity");
animateBack.setAttribute("repeatCount", "1");
animateBack.setAttribute("begin", "indefinite");
animateBack.setAttribute("dur", "0.1s");
animateBack.setAttribute("from", "1");
animateBack.setAttribute("to", "0");
animateBack.setAttribute("fill", "freeze");

var classesSD = ["actor-line", "messageLine0", "messageLine1", "note"];
var elementCounter = 0;
for (var j=0; j<classesSD.length; j++) {
  var elements = container.querySelector('svg').getElementsByClassName(classesSD[j]);
  for (var i=0; i<elements.length; i++) {
    var id = '_' + Math.random().toString(36).substr(2, 9);
    var anClone = animate.cloneNode(true);
    anClone.setAttribute("data-reverse", id);
    anClone.setAttribute("data-fragment-index", elementCounter);
    var anBackClone = animateBack.cloneNode(true);
    anBackClone.setAttribute("id", id);
    elements[i].parentNode.setAttribute("opacity", "0");
    elements[i].parentNode.appendChild(anClone);
    elements[i].parentNode.appendChild(anBackClone);
    elementCounter++;
  }
}
var classesFC = ["node", "edgePath"];
var classesCounter = 0;
for (var j=0; j<classesFC.length; j++) {
  elementCounter = classesCounter;
  var elements = container.querySelector('svg').getElementsByClassName(classesFC[j]);
  for (var i=0; i<elements.length; i++) {
    var id = '_' + Math.random().toString(36).substr(2, 9);
    var anClone = animate.cloneNode(true);
    anClone.setAttribute("data-reverse", id);
    anClone.setAttribute("data-fragment-index", elementCounter);
    var anBackClone = animateBack.cloneNode(true);
    anBackClone.setAttribute("id", id);
    elements[i].setAttribute("style", "");
    elements[i].setAttribute("opacity", "0");
    elements[i].appendChild(anClone);
    elements[i].appendChild(anBackClone);
    elementCounter += classesFC.length;
  }
  classesCounter++;
}

                  container.querySelector('svg').innerHTML = container.querySelector('svg').innerHTML + svg.innerHTML;
                }
              }
              else {
                console.warn( "Failed to get file. ReadyState: " + xhr.readyState + ", Status: " + xhr.status);
              }
            };
            xhr.open( 'GET', container.getAttribute("data-svg-src"), true );
            xhr.send();
          }
        }) }
      ],

      // Optional reveal.js plugins
      dependencies: [
        //{ src: 'plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
        //{ src: 'plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
        //{ src: 'plugin/highlight/highlight.js', async: true, condition: function() { return !!document.querySelector( 'pre code' ); }, callback: function() { hljs.initHighlightingOnLoad(); } },
        { src: 'plugin/zoom-js/zoom.js', async: true },
        { src: 'plugin/notes/notes.js', async: true },
        { src: 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.46.0/mapbox-gl.js' },
        { src: 'plugin/mapbox-gl/mapbox-gl.js'},
        { src: 'plugin/anything/anything.js'}
      ]
    });
    </script>
```

## PDF-Export
- für Decktape unmöglichen Code identifizieren: Plugin mapbox-gl (merkt man, wenn man decktape laufen lässt)
- mit prez builden (`prez --title="<title>" --author="<author>" --theme=serif --build`)
- auf Server kopieren (`sudo rm -r -f /var/www/html/<server_path> && sudo mkdir /var/www/html/<server_path> && sudo cp -r build/* /var/www/html/<server_path> && sudo chown -R www-data:www-data /var/www/html/<server_path>`)
- mit Chrome als PDF drucken
- Seitenverhältnis bestimmen (264mm x 193mm)
- in einem anderen Ordner: mit Decktape Bilder exportieren
  - Seitenverhältnis bestimmen: 1920x1080 geschnitten auf 264x193: 1477x1080
  - Bilder drucken und mit Strg+C bei erster Wiederholung abbrechen (`sudo docker run astefanutti/decktape --screenshots --screenshots-directory . --screenshots-format jpg --size 1477x1080 generic --key=" " "http://<local_IP>/<server_path>/?controls=false&controlsTutorial=false&progress=false&slideNumber=true&fragments=true&fragmentInURL=true" slides.pdf`)
  - Bilder exportieren (`sudo docker ps -lq`, `sudo docker cp <letzte Ausgabe>:slides .`)
  - Besitzreche am Ordner holen (`sudo chown <user>:<user> slides`)
  - führende Nullen in Dateinamen ergänzen ([Bash-Skript](https://stackoverflow.com/a/3700146))
  - Bilder ohne führende Nullen löschen
- Bilder in PDF zusammenfügen (`convert slides/*.jpg "<title>.pdf"`)
- Problemstellen im Druck-PDF durch entsprechende Seiten im Bilder-PDF ersetzen (z. B. mit PDF-Shuffler)