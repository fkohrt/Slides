<section data-mapbox='{"duration":4000, "zoom":4, "bearing":0, "center":[10.455278, 51.165]}' data-mapbox-trek="https://cdn.jsdelivr.net/gh/arm0th/CountryGeoJSONCollection/geojson/CNM.geojson">Zypern
	<span class="fragment current-visible" data-mapbox-transform-to='{"duration":8000, "zoom":5, "bearing":0, "center":[33.5, 35.1686323]}'></span>
	<span class="fragment current-visible" data-mapbox-transform-to='{"duration":1000, "zoom":8, "bearing":0, "center":[33.5, 35.1686323]}'></span>
Note:
- 1570: Zypern durch Türken erobert
- 1878: Großbritannien übernimmt Zypern von Türkei
- 1925: Zypern wird britische Kronkolonie
- 1931: Aufstand gegen Briten
- 1955: Bombenanschläge gegen Briten, später Bürgerkrieg
- 1959: Türkei und Griechenland einigen sich auf unabhängige Republik
- 1960: Proklamation der Unabhängigkeit
- Unzufriedenheit mit ethnischer Ausrichtung der Verfassung
- 1963: „blutige Weihnachten“ (Massaker durch Polizei) und interkommunale Kämpfe
- 1964: „Green Line“ (neutrale Zone)
- 1974
  - versuchter Putsch der Nationalgarde (durch Griechenland unterstützt)
  - türkische Truppen besetzen den Norden
  - Krieg zwischen den Ethnien
  - Flüchtlinge auf beiden Seiten
- 1975: Bildung eines zyperntürkischen Teilstaats (Bundesstaat)
- 1983: Proklamation Türkische Republik Nordzypern
- 2003: Öffnung der Grenzen
- 2004: EU-Beitritt
</section>