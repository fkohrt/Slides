### Bedingungen für optimalen Kontakt

<ul>
<li class="fragment">gleicher Status</li>
<li class="fragment">gemeinsame Ziele</li>
<li class="fragment">Kooperation zw. Gruppen</li>
<li class="fragment">unterstützende gesellschaftliche Regeln</li>
<li class="fragment">natürliche Kontaktsituation</li>
<li class="fragment">hohe Gruppensalienz</li>
</ul>

Note:
- die ersten vier Moderatoren kommen von Allport
- die andere beiden wurden später hinzugefügt
- keiner davon ist (zwingend) notwendig (Pettigrew et al., 2011)