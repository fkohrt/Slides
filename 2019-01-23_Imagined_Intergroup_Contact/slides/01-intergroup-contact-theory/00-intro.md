### Kontakt-Hypothese <span style="font-size:80%;">(Allport, 1954)</span>

<div class="fragment">häufiger Kontakt zu Mitgliedern einer Outgroup reduziert Vorurteile gegenüber diesen</div>

<div class="row">
<div class="half-half-left fragment">
<img src="images/Migrationshintergrund_Kreise_Zensus_2011.png" style="height:9em;" />
    <div class="footnote">Anteil der Bevölkerung mit Migrationshintergrund nach Ergebnissen des Zensus 2011 auf Kreisebene (Michael Sander, <a class="rawURL" href="https://de.wikipedia.org/wiki/Datei:Migrationshintergrund_Kreise_Zensus_2011.png">de.wikipedia.org</a>, <a class="noBlue" href="https://creativecommons.org/licenses/by-sa/3.0/">CC BY-SA 3.0</a>)</div>
</div>
<div class="half-half-right fragment">
<img src="images/Btw13_npd_zweit_endgueltig.svg" style="height:9em;" />
    <div class="footnote">Zweitstimmenanteil NPD bei der Bundestagswahl 2013 in Deutschland (own work (wahlatlas.net), <a class="rawURL" href="https://de.wikipedia.org/wiki/Datei:Btw13_npd_zweit_endgueltig.svg">de.wikipedia.org</a>, <a class="noBlue" href="https://creativecommons.org/licenses/by-sa/3.0/">CC BY-SA 3.0</a>)</div>
</div>
</div>

Note:
- mögliches Beispiel für Kontakthypothese: Deutschlandwahl 2013