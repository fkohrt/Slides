### Mediatoren

<div class="row">
<div class="column-3 fragment">
<img src="images/noun_anxious_216014_000000.svg" style="border:none;box-shadow:none;height:6em;" />
    <div>Unbehagen reduzieren</div>
    <div class="footnote">(Daryl Vandemont,<br /><a class="rawURL" href="https://thenounproject.com/term/anxious/216014">thenounproject.com</a>, <a class="noBlue" href="https://creativecommons.org/licenses/by/3.0/us/">CC BY 3.0 US</a>)</div>
</div>
<div class="column-3 fragment">
<img src="images/noun_friends_1201432_000000.svg" style="border:none;box-shadow:none;height:6em;" />
    <div>Empathie aufbauen</div>
    <div class="footnote">(Adrien Coquet,<br /><a class="rawURL" href="https://thenounproject.com/term/friends/1201432/">thenounproject.com</a>, <a class="noBlue" href="https://creativecommons.org/licenses/by/3.0/us/">CC BY 3.0 US</a>)</div>
</div>
<div class="column-3 fragment fade-in-then-semi-out">
<img src="images/noun_think_159182_000000.svg" style="border:none;box-shadow:none;height:6em;" />
    <div>Wissen entwickeln</div>
    <div class="footnote">(Guilherme Furtado,<br /><a class="rawURL" href="https://thenounproject.com/term/think/159182">thenounproject.com</a>, <a class="noBlue" href="https://creativecommons.org/licenses/by/3.0/us/">CC BY 3.0 US</a>)</div>
</div>
</div>

<span class="fragment"><!-- fragment after fade-in-then-semi-out --></span>

Note:
- Mediator Wissen wurde von Allport vermutet
- tatsächlich sind Reduktion von Unbehagen und Aufbau von Empathie bessere Moderatoren