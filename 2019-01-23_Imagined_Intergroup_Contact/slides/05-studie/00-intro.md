### Studie
„Imagined intergroup contact: A new technique for encouraging greater inter-ethnic contact in Cyprus.“ (2010)

<ul>
<li class="fragment">Anwendung des Konzepts auf Situation in Zypern, um Lage zu verbessern</li>
<li class="fragment">es gab vorherige Interventionen</li>
<li class="fragment">aber nur eingeschränkt erfolgreich</li>
<li class="fragment">Ziel: Effektivität von vorgestelltem Kontakt auf Absicht, real Kontakt aufzunehmen</li>
</ul>