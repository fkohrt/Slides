### Design

<ul>
<li class="fragment">UV: 3 (vorgestellte Situation:
<ul><li class="fragment">ein Spaziergang im Freien vs.</li>
<li class="fragment">zwei Begegnungen in gleicher Umgebung vs.</li>
<li class="fragment">zwei Begegnungen in unterschiedlicher Umgebung)</li></ul></li>
<li class="fragment">AV: Absicht, zukünftig bei Gelegenheit Kontakt mit einer griechisch-zyprischen Person aufzunehmen
    <ul><li class="fragment">Wahrscheinlichkeit, ein Gespräch zu beginnen</li>
    <li class="fragment">Interesse, ein Gespräch zu beginnen</li>
    <li class="fragment">Selbsteinschätzung auf Skala von 1 (gar nicht) bis 9 (sehr)</li></ul></li>
</ul>
