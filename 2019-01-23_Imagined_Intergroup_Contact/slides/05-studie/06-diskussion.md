### Diskussion der Hypothesen

<ol style="list-style-type:lower-alpha;">
<li class="fragment fade-in">„Vorstellung von Kontakt zwischen türkischen und griechischen Zyprern stärkt die Absicht, tatsächlich Kontakt aufzunehmen“<ul style="list-style-type:square;">
<li class="fragment">wurde nicht unter Berücksichtigung aller Daten geprüft (MW aus beiden Bedingungen)</li>
<li class="fragment">kann also nicht gesagt werden</li>
<li class="fragment">Autoren sehen das anders</li></ul></li>
<li class="fragment">keine verlässliche Verbesserung bei Variation der Situation</li>
</ul>