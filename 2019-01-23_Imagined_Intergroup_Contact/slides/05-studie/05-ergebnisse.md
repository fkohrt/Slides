### Ergebnisse

<div class="row">
<div class="half-half-left">
<div class="animate" data-svg-src="images/ergebnisse.svg" style="border:4px solid #000;box-shadow:0 0 10px rgba(0, 0, 0, 0.15);font-size:16px;">
    <svg></svg>
</div>
</div>
<div class="half-half-right">
<img src="images/p_values.png" />
<div class="footnote">(Randall Munroe,<br /><a class="rawURL" href="https://xkcd.com/1478/">xkcd.com/1478/</a>, <a class="noBlue" href="https://creativecommons.org/licenses/by-nc/2.5/deed.de">CC BY-NC 2.5</a>)</div>
</div>
</div>