$class:no-center$
### Durchführung

<div class="animate" data-svg-src="images/durchfuehrung.svg" style="border:4px solid #000;box-shadow:0 0 10px rgba(0, 0, 0, 0.15);font-size:14px;">
    <svg></svg>
</div>

Note:
- Kontrolle:
  - Nimm Dir eine Minute
  - Stell Dir vor, Du gehst im Freien spazieren
  - Denke an einzelne Aspekte dieser Szene (Strand?, Wald?, Bäume?, Himmel?)
- homogen + divers:
  - Stell Dir vor, Du triffst einen gr. Zyprer das erste Mal
  - Überlege Dir, wann (nächsten Donnerstag?) und wo Du mit ihm sprichst
  - Während der Konversation erfährst Du etwas Interessantes und Unerwartetes über den Zyprer
- homogen:
  - weitere Begegnung mit einem anderen gr. Zyprer vor
  - zur selben Zeit am selben Ort
  - interessant + unerwartet
- divers:
  - weitere Begegnung mit einem anderen gr. Zyprer
  - zu einer anderen Zeit an einem anderen Ort
  - interessant + unerwartet
- Fragebogen:
  - Wenn Du das nächste mal Gelegenheit hast, mit einem gr. Zyprer zu sprechen...
    - wie wahrscheinlich
    - wie interessant
  - ...wäre ein Gesprächsbeginn durch Dich?