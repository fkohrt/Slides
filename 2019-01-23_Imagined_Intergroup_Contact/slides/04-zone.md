### UN-Pufferzone

<img src="images/Nicosia_3_April_2008_14.jpg" style="height:12em;" />
<div class="footnote">(Steffen Löwe,<br /><a class="rawURL" href="https://commons.wikimedia.org/wiki/File:Nicosia_3_April_2008_14.jpg">commons.wikimedia.org</a>, <a class="noBlue" href="https://creativecommons.org/licenses/by-sa/3.0/deed.de">CC BY-SA 3.0</a>)</div>

Note:
- im Bild: Grenzöffnung in der Hauptstadt Nikosia an der Ledrastraße im April 2008
- noch immer psychische Barriere für Zyprer, obwohl Grenzen seit 2003 stellenweise offen
- daher wenig Kontakt zwischen Norden und Süden
- geringes Wissen übereinander
- ~40% gr. u. ~30% türk. Zyprer haben die Grenze seit ihrer Öffnung 2003 nicht überquert (UN, 2007)
- mehr als 50% d. gr. Zyprer empfinden Grenzübertritt als unangemessen (Webster et al., 2006)