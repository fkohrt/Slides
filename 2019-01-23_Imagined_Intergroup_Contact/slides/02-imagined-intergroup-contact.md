### Imagined Intergroup Contact

<span class="fragment">mentale Simulation einer sozialen Interaktion mit Mitgliedern einer Outgroup</span>

<div class="fragment">
<img src="images/noun_audience_2096107_000000.svg" style="border:none;box-shadow:none;height:6em;" />
<div class="footnote">(priyanka,<br /><a class="rawURL" href="https://thenounproject.com/term/audience/2096107/">thenounproject.com</a>, <a class="noBlue" href="https://creativecommons.org/licenses/by/3.0/us/">CC BY 3.0 US</a>)</div>
</div>

Note:
- Definition: mentale Simulation einer sozialen Interaktion mit Mitgliedern einer Outgroup
- Beispiel: Bystander-Effekt bei vorgestellter Menschenmenge (Garcia, 2002)
- bloßes Denken an Outgroup-Mitglieder reicht _nicht_
- auch Vorstellen einer Begegnung notwendig
- beim vorgestellten Kontakt
  - ähnliche emotionale und motivationale Reaktionen
  - ähnliche neuronale Mechanismen