### Was denkt ihr:

- Was sind die Einschränkungen der Studie?
- Wo lässt sich die Methode einsetzen?
- Sind Täuschungen bzw. Manipulationen vorstellbar?

Note:
**Einschränkungen**
- Absicht ≠ Handlung
- Herbeiführen _und_ Vermeiden abfragen, da versch. psych. Prozesse
- studentische Stichprobe; ältere Zyprer haben Krieg erlebt

**Einsatz**
- politische Konflikte: Nordirland (katholisch vs. protestantisch)
- Therapie (erste Stufe einer Annäherung)
- Schule (neben passivem Wissen auch aktive Vorstellung eines Kontakts)