### Quellen

#### Grundlage der Präsentation
<ul style="font-size:18px;">
<li>Husnu, S., & Crisp, R. J. (2010). Imagined intergroup contact: A new technique for encouraging greater inter-ethnic contact in Cyprus. Peace and Conflict: Journal of Peace Psychology, 16(1), 97–108. DOI: <a class="rawURL" href="https://dx.doi.org/10.1080/10781910903484776">10.1080/10781910903484776</a></li>
<li>PALUCK, E. L., GREEN, S. A., & GREEN, D. P. (2018). The contact hypothesis re-evaluated. Behavioural Public Policy, 1–30. DOI: <a class="rawURL" href="https://dx.doi.org/10.1017/bpp.2018.25">10.1017/bpp.2018.25</a></li>
<li>Pettigrew, T. F., Tropp, L. R., Wagner, U., & Christ, O. (2011). Recent advances in intergroup contact theory. International Journal of Intercultural Relations, 35(3), 271–280. DOI: <a class="rawURL" href="https://dx.doi.org/10.1016/j.ijintrel.2011.03.001">10.1016/j.ijintrel.2011.03.001</a></li>
</ul>

#### Weitere Literatur

<ul style="font-size:18px;">
<li>Allport, G. W. (1954). The nature of prejudice. Cambridge, Mass: Boston, Mass. OCLC: <a class="rawURL" href="https://www.worldcat.org/oclc/797356583">797356583</a></li>
    <li>Garcia, S. M., Weaver, K., Moskowitz, G. B., & Darley, J. M. (2002). Crowded minds: The implicit bystander effect. Journal of Personality and Social Psychology, 83(4), 843–853. DOI: <a class="rawURL" href="https://dx.doi.org/10.1037/0022-3514.83.4.843">10.1037/0022-3514.83.4.843</a></li>
<li>United Nations. (2007). UN in Cyprus. UNFICYP Information Technology Unit. Zitiert nach: Husnu et al., 2010.</li>
<li>Webster, C., & Timothy, D. J. (2006). Travelling to the ‘Other Side’: the Occupied Zone and Greek Cypriot Views of Crossing the Green Line. Tourism Geographies, 8(2), 162–181. Zitiert nach: Husnu et al., 2010. DOI: <a class="rawURL" href="https://dx.doi.org/10.1080/14616680600585513">10.1080/14616680600585513</a></li>
<li>Eintrag „Zypern - gesamt“ in Munzinger Online/Länder - Internationales Handbuch, URL: <a class="rawURL" href="https://www.munzinger.de/document/03000CYP000">www.munzinger.de/document/03000CYP000</a> (abgerufen von Münchner Stadtbibliothek am 23. Januar 2019)</li>
<li>Seite „Zypernkonflikt“. In: Wikipedia, Die freie Enzyklopädie. Bearbeitungsstand: 15. Januar 2019, 21:08 UTC. URL: <a class="rawURL" href="https://de.wikipedia.org/w/index.php?title=Zypernkonflikt&oldid=184767377">de.wikipedia.org/w/index.php?title=Zypernkonflikt&oldid=184767377</a> (Abgerufen: 23. Januar 2019, 14:54 UTC)</li>
</ul>