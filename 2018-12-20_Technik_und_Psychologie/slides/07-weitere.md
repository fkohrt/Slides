## Weitere Prinzipien

<ul>
    <li class="fragment fade-in-then-semi-out">Loss Aversion: Fear of missing out</li>
    <li class="fragment fade-in-then-semi-out">social approval: sehr mächtig, z. B. Freude, wenn ein Computer lobt (media equation)</li>
    <li class="fragment fade-in-then-semi-out">delay discounting</li>
    <li class="fragment fade-in-then-semi-out">social reciprocity: nichtige Aktionen erfordern subjektiv eine Reaktion</li>
</ul>

Note:
- social approval: wirkt auch, wenn man Lob erwürfelt