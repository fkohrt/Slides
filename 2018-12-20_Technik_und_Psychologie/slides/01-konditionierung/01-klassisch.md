### Klassische Konditionierung

<img src="images/Pawlowscher-Hund.svg" style="width:90%;" />

<div class="footnote">Konditionierung des Pawlowschen Hunds (MagentaGreen, <a class="rawURL" href="https://commons.wikimedia.org/wiki/File:Pavlov%27s_dog.svg">commons.wikimedia.org</a>, <a href="https://creativecommons.org/licenses/by-sa/3.0/">CC BY-SA 3.0</a>)</div>

Note:
ein zunächst neutraler Reiz führt nach dem Lernen zu einer physiologischen Reaktion