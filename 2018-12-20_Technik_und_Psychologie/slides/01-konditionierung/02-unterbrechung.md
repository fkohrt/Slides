### Unterbrechung!

<audio data-autoplay src="media/iphone-ding-sound.mp3"></audio>

<img src="images/Facebook-True-costs-1.png" />

Note:
- klassische Konditionierung: Verknüpfung des CS (Ton) mit UCS (soziale Anerkennung)
- harte Unterbrechung führt zu oberflächlicher Verarbeitung des Reizes
  - das heißt, es werden Heuristiken für weitere Handlung herangezogen
  - da der Reiz schon positive Gefühle ausgelöst hat, wird man vermutlich nachsehen (auch, wenn man gerade eigentlich beschäftigt ist)
- hoher Schaden: man braucht bis zu 23 Minuten, um wieder bei der Sache zu sein (Quelle: Gloria Mark u. a. „The Cost of Interrupted Work: More Speed and Stress“. Proceeding of the Twenty-Sixth Annual CHI Conference on Human Factors in Computing Systems  - CHI ’08, ACM Press, 2008, S. 107. DOI: [10.1145/1357054.1357072](https://dx.doi.org/10.1145/1357054.1357072))

Weitere Lektüre:
- Daniel Kahneman. _Thinking, fast and slow._ 1st pbk. ed, Farrar, Straus and Giroux, 2013.