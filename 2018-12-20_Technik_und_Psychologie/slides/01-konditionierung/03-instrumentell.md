### Instrumentelle Konditionierung

<img src="images/Skinner-Box.png" style="width:50%;" />

<div class="footnote">Skinner-Box (V1nzorg, <a class="rawURL" href="https://commons.wikimedia.org/wiki/File:Skinner_box_de.png">commons.wikimedia.org</a>, <a href="https://www.gnu.org/copyleft/fdl.html">GFDL</a>)</div>

Note:
Verhalten wird durch einen Verstärker wahrscheinlicher gemacht