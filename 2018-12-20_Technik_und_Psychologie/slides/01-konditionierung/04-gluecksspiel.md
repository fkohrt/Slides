### Glücksspielautomaten

<div class="row">
<div class="fragment column-3">
<img src="images/Glücksspielautomat.jpg" />

<div class="footnote">Glücksspielautomat (PMOrchidReader, <a class="rawURL" href="https://pixabay.com/de/kasino-steckplatz-gl%C3%BCcksspiel-632862/">pixabay.com</a>, <a href="https://creativecommons.org/publicdomain/zero/1.0/deed.de">CC0</a>)</div>
</div>
<div class="fragment column-3">
<img src="images/Pull-to-refresh.png" />

<div class="footnote">E-Mail-Glücksspiel (<a class="rawURL" href="https://www.tapsmart.com/tips-and-tricks/pullrefresh/">www.tapsmart.com</a>)</div>
</div>
<div class="fragment column-3">
<img src="images/Facebook-Feed.jpg" />

<div class="footnote">Facebook-Feed (<a class="rawURL" href="https://www.adweek.com/digital/facebook-really-wants-people-to-use-stories/">www.adweek.com</a>)</div>
</div>
</div>