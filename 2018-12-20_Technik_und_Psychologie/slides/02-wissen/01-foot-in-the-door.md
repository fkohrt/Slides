### Foot-in-the-door

<div class="fragment">
<img src="images/Facebook-True-costs-2.png" />

<div class="footnote">Die wahren Kosten eines Klicks (Tristan Harris, <a class="rawURL" href="http://www.tristanharris.com/2016/05/how-technology-hijacks-peoples-minds%E2%80%8A-%E2%80%8Afrom-a-magician-and-googles-design-ethicist/">www.tristanharris.com</a>)</div>
</div>

Note:
- Wir nutzen unser Verhalten, um Rückschlüsse auf unsere Einstellungen zu ziehen
- Foot-in-the-door
  - man erfüllt eine kleine Bitte
  - man hält sich für hilfsbereit
  - man erfüllt eine große Bitte
- Foot-in-the-door-Strategie kann genutzt werden, um nach Dates zu fragen (Quelle: Nicolas Guéguen u. a. „Foot-in-the-Door Technique Using a Courtship Request: A Field Experiment“. Psychological Reports, Bd. 103, Nr. 2, Oktober 2008, S. 529–34. DOI: [10.2466/pr0.103.2.529-534](https://dx.doi.org/10.2466/pr0.103.2.529-534).)
- bei Facebook: man kann nur Posten, wenn man auch den News-Feed sieht
  - man postet etwas und nutzt damit Facebook
  - „Ich nutze Facebook, also gefällt mir Facebook wahrscheinlich.“
  - man bleibt noch ein wenig
- auch körperliche Erregung hat Einfluss auf Bewertung einer Situation (Quelle: Donald G. Dutton und Arthur P. Aron. „Some Evidence for Heightened Sexual Attraction under Conditions of High Anxiety.“ Journal of Personality and Social Psychology, Bd. 30, Nr. 4, 1974, S. 510–17. DOI: [10.1037/h0037031](https://dx.doi.org/10.1037/h0037031).)

Weitere Lektüre:
- Cialdini, Robert B. _Influence: science and practice._ 5th ed, Pearson Education, 2009.