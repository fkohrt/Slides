### Bottomless bowls

<img src="images/Bottomless-bowls.jpg" style="width:35%;" />

<div class="footnote">Biased visual cues provided by self-refilling soup bowls increased intake but not the perception of intake (Brian Wansink et al. (2005), DOI: [10.1038/oby.2005.12](https://dx.doi.org/10.1038/oby.2005.12))</div>

Note:
- Studie an der Cornell-Universität: mit bodenlosen Schüsseln nehmen Menschen 73% mehr Kalorien zu sich (Quelle: Brian Wansink u. a. „Bottomless Bowls: Why Visual Cues of Portion Size May Influence Intake“. Obesity Research, Bd. 13, Nr. 1, Januar 2005, S. 93–100. DOI: [10.1038/oby.2005.12](https://dx.doi.org/10.1038/oby.2005.12)</a>.)