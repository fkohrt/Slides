### Suchmaschine

<div class="row">
<div class="column-3 fragment">
<img src="images/Safari-iPhone.jpg" />

<div class="footnote">Safari (<a class="rawURL" href="https://www.imore.com/how-use-smart-search-bar-safari-iphone-and-ipad">www.imore.com</a>)</div>
</div>
<div class="column-3 fragment">
<img src="images/Siri.png" />

<div class="footnote">Siri (<a class="rawURL" href="https://support.apple.com/de-de/HT206993">support.apple.com</a>)</div>
</div>
<div class="column-3 fragment">
<img src="images/Firefox.svg" />

<div class="footnote">Firefox (The Mozilla Foundation, <a class="rawURL" href="https://commons.wikimedia.org/wiki/File:Firefox_Logo,_2017.svg">commons.wikimedia.org</a>, <a href="https://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a>)</div>
</div>
</div>

<div class="fragment">
Googles TAC lagen 2017 bei 22 Mrd. USD

<div class="annotation">TAC: traffic acquisition cost</div>

<div class="annotation">(Quelle: Alphabet Annual Report 2017 auf <a class="rawURL" href="https://abc.xyz/investor/pdf/20171231_alphabet_10K.pdf">abc.xyz</a>, S. 35)</div>
</div>

Note:
- ein Default ist eine Annahme, die geändert werden kann
- aber in der Regel nicht geändert wird
- und der man sich häufig nicht bewusst ist

- **daher**: Wahl des Defaults ist Ausdruck von Macht und Einfluss
- und bedingt die spätere Nutzung des Systems

- **Beispiele**
  - Wird Anonymität angenommen?
  - Maximieren die Defaults das Teilen von Inhalten oder Privatsphäre?
  - Laufen die Regeln nach einer bestimmten Zeit ab oder erneuern sie sich?
  - Opt-in oder Opt-out?

- **Beispiel Stromnetz**
  - etwa leichtes Hinzufügen von dafür unsicheren Innovationen oder schwieriger zu ändern, dafür sicherer
  - nimmt es ungewöhnliche Stromquellen gut auf?
  - zentralisiert oder dezentralisiert?

- Technik ist nicht neutral, sondern kommt mit natürlichen Ausrichtungen (biases)!
- Da Defaults vor allem dann wirken, wenn man nichts ändert, wird klar: Nichts tun ist nicht neutral, weil die Ausrichtung beibehalten wird! Auch nichts wählen ist damit eine Wahl!
- Quelle: [Triumph of the Default](https://kk.org/thetechnium/triumph-of-the/)

Alphabets Ausgaben für Einrichtung von Google als Standardsuchmaschine
- Tendenz steigend (2015: 14 Mrd., 2016: 17 Mrd., für 2018 erneuter Anstieg absehbar)

außerdem:
- Hersteller mussten bei Verwendung von Android Google-Suche und Chrome einrichten (Quelle: [heise.de/-4113754](https://heise.de/-4113754))