### E-Mail-Signatur

<img src="images/E-Mail-Signatur-iPhone.jpeg" />

<div class="footnote">„Sent from my iPhone“ (<a class="rawURL" href="https://www.imore.com/how-to-set-custom-email-signatures">www.imore.com</a>)</div>

Note:
außerdem:
- „Acceptable Ads“ bei Adblock Plus
  - 2016 schalten 90 Prozent die Einstellung nicht ab (Quelle: [archiviert von eyeo.com](https://web.archive.org/web/20160922091256/https://eyeo.com/press/releases/en/2016-09-13-acceptable-ads-platform.pdf))
  - verlangen 30 Prozent des daurch erzielten Umsatzes (Quelle: [heise.de/-3046033](https://heise.de/-3046033))
  - expandieren weiter (Quelle: [heise.de/-4141041](https://heise.de/-4141041))
- sehr problematisch etwa bei Passwörtern: werden beibehalten
- abseits der Technik: Organspenden in Deutschland, Abonnement verlängert sich automatisch
- selbstlernende Systeme (etwa basierend auf neuronalen Netzen) berauben uns der Wahlmöglichkeit

Quellen sowie weiterer Lese- und Filmstoff:
- David Barton: [The Tyranny of the Default](https://pagefair.com/blog/2015/the-tyranny-of-the-default/)
  - [Triumph of the Default](https://kk.org/thetechnium/triumph-of-the/)
    - Dan Ariely: [Are we in control of our own decisions?](https://www.ted.com/talks/dan_ariely_asks_are_we_in_control_of_our_own_decisions)
  - Studie (DOI: [10.1126/science.1091721](https://dx.doi.org/10.1126/science.1091721))
- VonBergen, Clarence W.; Kernek, Courtney; Bressler, Martin S.; and Silver, Lawrence S. (2016) _Cueing the Customer Using Nudges and Negative Option Marketing,_ Atlantic Marketing Journal: Vol. 5: No. 2, Article 12. Abrufbar bei [digitalcommons.kennesaw.edu/amj/vol5/iss2/12](https://digitalcommons.kennesaw.edu/amj/vol5/iss2/12)
- Willis, Lauren E. (2013). _When Nudges Fail: Slippery Defaults._ The University of Chicago Law Review, 80(3), 1155-1229. Abrufbar bei [www.jstor.org/stable/23594878](https://www.jstor.org/stable/23594878)