## Gamification

<img src="images/Snapchat.jpeg" style="width:50%;" />

<div class="footnote">Snapchat-Streaks (Jeff Higgins, <a class="rawURL" href="https://hackernoon.com/between-bots-gen-z-the-new-instagram-and-snapchat-usage-metrics-are-meaningless-8c329a343874">hackernoon.com</a>, <a href="https://creativecommons.org/licenses/by-nd/4.0/">CC BY-ND 4.0</a>)</div>

Note:
- Snapchat-Streaks verbinden Gamification mit dem Wunsch nach sozialer Anerkennung
- WhatsApp arbeitet ebenfalls mit sozialer Anerkennung
  - zuletzt online
  - Nachricht gelesen?

außerdem:
- außerhalb der Technik: To-Do-Liste

Mehr:
- Snapchat: [Flammen](https://support.snapchat.com/de-DE/article/snapstreaks)