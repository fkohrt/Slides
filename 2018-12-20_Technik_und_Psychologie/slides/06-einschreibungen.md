## Exkurs: Einschreibungen

<img src="images/Scanner-Flughafen.jpg" style="width:70%;" />

<div class="footnote">Scanner am Flughafen (Pacific Northwest National Laboratory, <a class="rawURL" href="https://www.flickr.com/photos/pnnl/39682091582">www.flickr.com</a>, <a href="https://creativecommons.org/licenses/by-nc-sa/2.0/">CC BY-NC-SA 2.0</a>)</div>

Note:
- Einschreibungen mehr als Defaults, da keine Änderung möglich

Einschreibungen beim Scanner
- offensichtlich: Mensch muss auf zwei Beinen hineingehen (nicht für Rollstuhlfahler)
- weniger offensichtlich: bestimmte Körperproportionen

außerdem:
- Form von Handwerkzeugen sowie frühe Autos nahmen aufgrund der Sitzposition männliche Fahrer an (Quelle: [Triumph of the Default](https://kk.org/thetechnium/triumph-of-the/))
- viele Werkzeuge nur für Rechtshänder
- noch heute vielfach nur Auswahl von Mann oder Frau möglich
- Technik ist nicht neutral! (siehe dazu auch Anil Dash: [12 Things Everyone Should Understand About Tech](https://medium.com/humane-tech/12-things-everyone-should-understand-about-tech-d158f5a26411))

Quelle:
- Vortrag auf der FIfFKon 2018 von Daniel Guagnin und Jörg Pohle: [Welt -> Modell -> Einschreibung -> Welt'](https://media.ccc.de/v/fiffkon18-10-welt_-_modell_-_einschreibung_-_welt)