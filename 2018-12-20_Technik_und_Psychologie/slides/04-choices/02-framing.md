### Framing

<img src="images/Yelp.png" />

<div class="footnote">Yelp (Tristan Harris, <a class="rawURL" href="http://www.tristanharris.com/2016/05/how-technology-hijacks-peoples-minds%E2%80%8A-%E2%80%8Afrom-a-magician-and-googles-design-ethicist/">www.tristanharris.com</a>)</div>

Note:
- Eine Gruppe möchte sich abends noch unterhalten.
- Man öffnet Yelp, um sich Empfehlungen in der Nähe anzusehen
- und bekommt eine Reihe von Bars angezeigt.
- Jeder vergleicht auf einmal Bars auf seinem Handy.

- Die ursprüngliche Frage: Wo kann man sich noch gut unterhalten?
- Wurde reframed nach: Welche Bar hat gute Cocktail-Fotos?

- Man denkt, Yelp präsentiert alle Optionen
- und sieht nicht mehr den Park mit der Live-Band oder die Pop-up-Gallerie, die Crepes und Kaffee anbietet.

- Je mehr Auswahlmöglichkeiten uns Handys in jeder Lebenssituation geben (Informationen, Events, Orte, Freunde, Dating, Beruf),
- umso mehr nehmen wir an, dass man dabei die besten und nützlichsten Optionen präsentiert bekommt.

außerdem:
- Supermarkt platziert beliebte Sachen ganz hinten und teure Sachen auf Augenhöhe
- Eine Ratte, der eine vielfältige Auswahl angeboten wird, isst mehr (Quelle: Toates, Frederick M. (1986). Motivational systems. Cambridge Cambridgeshire New York: Cambridge University Press. ISBN: 9780521318945, S. 40. Abrufbar bei [books.google.de](https://books.google.de/books?id=JVg7AAAAIAAJ&hl=de&pg=PA40))