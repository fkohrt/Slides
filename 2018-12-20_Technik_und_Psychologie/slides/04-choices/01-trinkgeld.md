### Trinkgeld

<div class="row">
<div class="half-half-left fragment">
<img src="images/MyTaxi-Trinkgeld-1.jpg" style="width:60%;" />

<div class="footnote">myTaxi Bezahl-Dialog Spanisch (<a class="rawURL" href="https://mytaxi.de.softonic.com/android">mytaxi.de.softonic.com</a>)</div>
</div>
<div class="half-half-right fragment">
<img src="images/MyTaxi-Trinkgeld-2_edited.jpg" style="width:60%;" />

<div class="footnote">myTaxi Bezahl-Dialog Deutsch (<a class="rawURL" href="https://curved.de/news/wie-apple-pay-mytaxi-laesst-per-fingerabdruck-bezahlen-148837">curved.de</a>)</div>
</div>
</div>

Note:

- Wikipedia: [Warum sehen die Spendenbanner so aus wie sie aussehen?](https://de.wikipedia.org/wiki/Wikipedia:Fundraiser-Portal/Fragen_und_Antworten#Warum_sehen_die_Spendenbanner_so_aus_wie_sie_aussehen?)