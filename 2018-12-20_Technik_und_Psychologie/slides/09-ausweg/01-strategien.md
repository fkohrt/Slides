### Strategien

<ul>
<li class="fragment fade-in-then-semi-out">Benachrichtigungen für alles außer Menschen abstellen</li>
<li class="fragment fade-in-then-semi-out">Graustufen aktivieren</li>
<li class="fragment fade-in-then-semi-out">Nur Tools auf dem Homescreen</li>
<li class="fragment fade-in-then-semi-out">andere Apps durch Suche starten</li>
<li class="fragment fade-in-then-semi-out">Handy außerhalb des Schlafzimmers laden</li>
<li class="fragment fade-in-then-semi-out">Social-Media-Apps löschen</li>
</ul>

Note:
Quelle: [humanetech.com/take-control/](https://humanetech.com/take-control/)