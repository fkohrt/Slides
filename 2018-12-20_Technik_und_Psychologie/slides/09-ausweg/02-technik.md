### Technik?

<div class="row">
<div class="half-half-left">
<img src="images/iOS-Screentime.jpg" style="width:80%;" />

<div class="footnote">Screen Time auf iPhones (Apple, <a class="rawURL" href="https://www.apple.com/de/newsroom/2018/06/ios-12-introduces-new-features-to-reduce-interruptions-and-manage-screen-time/">www.apple.com</a>)</div>
</div>
<div class="half-half-left">
<img src="images/Android-Digital-Wellbeing.gif" style="width:50%;" />

<div class="footnote">Digital Wellbeing auf Android-Geräten (Google, <a class="rawURL" href="https://germany.googleblog.com/2018/08/android-9-pie.html">germany.googleblog.com</a>)</div>
</div>
</div>

Note:
- auch Facebook