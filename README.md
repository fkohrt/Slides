# Slides

## Presentations

- [Looks aren't everything](https://fkohrt.gitlab.io/Slides/2018-06-05_englishpresentation/2018-06-05_englishpresentation.html) (mit [xaringan](https://bookdown.org/yihui/rmarkdown/xaringan.html))
- [Current approaches in measuring personality based on online data](https://fkohrt.gitlab.io/Slides/2018-07-05_Cyberpsychology.pdf) (mit [slides.com](https://slides.com/fkohrt/cyberpsychology))
- [Silicon Valley Culture](https://fkohrt.gitlab.io/Slides/2018-07-10_SiliconValleyCulture.pdf) (mit [slides.com](https://slides.com/fkohrt/silicon-valley-culture))
- [Über die Ästhetisierung von Terroranschlägen in den Medien](https://fkohrt.gitlab.io/Slides/2018-08-23_Ästhetisierung_von_Terroranschlägen/build/index.html) (mit [prez](https://github.com/byteclubfr/prez))
- [Methoden der Entwicklungspsychologie](https://fkohrt.gitlab.io/Slides/2018-10-30_Methoden_der_Entwicklungspsychologie/build/index.html) (mit [prez](https://github.com/byteclubfr/prez))
- [Technik und Psychologie](https://fkohrt.gitlab.io/Slides/2018-12-20_Technik_und_Psychologie/build/index.html) (mit [prez](https://github.com/byteclubfr/prez))
- [Alkoholmissbrauch im Jugendalter  – Ursachen und Entstehungsbedingungen, Verbreitung](https://fkohrt.gitlab.io/Slides/2019-01-20_Alkoholmissbrauch_im_Jugendalter/Alkoholmissbrauchsreferat_ohneBilder.pdf) (mit [LibreOffice](https://www.libreoffice.org/))
- [Imagined intergroup contact](https://fkohrt.gitlab.io/Slides/2019-01-23_Imagined_Intergroup_Contact/build/index.html) (mit [prez](https://github.com/byteclubfr/prez))
- [Teknophagie in der griechischen Mythologie](https://fkohrt.gitlab.io/Slides/2019-04-10_Teknophagie/index.html) (mit [slidewiki.org](https://slidewiki.org/deck/127345/teknophagie-in-der-griechischen-mythologie))
- [Technophobie im globalisierten Alltag](https://fkohrt.gitlab.io/Slides/2019-04-10_Technophobie/index.html) (mit [slidewiki.org](https://slidewiki.org/deck/127344/technophobie-im-globalisierten-alltag))
- [Social emotions](https://fkohrt.gitlab.io/Slides/2019-07-17_Social_Emotions.pdf) (mit [Google Docs](https://drive.google.com/open?id=1TAQkBBlN4V2mdmj6B5tI0EcgXwe3BC8ZU9kyLqH4Qxw))
- [The misalignment paradigm](https://fkohrt.gitlab.io/Slides/2019-09-05_Alsmith2017.pdf) (mit [slides.com](https://slides.com/fkohrt/misalignment))
- [Syntax](https://fkohrt.gitlab.io/Slides/2019-11-06_Syntax/Syntax.html) (mit [Marp](https://github.com/marp-team/marp))
- Commoning: [2019-11-09](https://fkohrt.gitlab.io/Slides/2020-01-14_Commoning/v1/public/commons-deck.html), [2020-01-14](https://fkohrt.gitlab.io/Slides/2020-01-14_Commoning/v2/public/commons-deck.html) (mit [decker](https://gitlab2.informatik.uni-wuerzburg.de/decker/decker))
- [Freie Lizenzen](https://fkohrt.gitlab.io/Slides/2020-05-20_Freie_Lizenzen/freie_lizenzen.html) (with [Xaringan](https://cran.r-project.org/package=xaringan) and [ninja theme](https://github.com/emitanaka/ninja-theme))
- [Visuelle Indoktrination von Kindern im Nationalsozialismus](https://fkohrt.gitlab.io/Slides/2021-06-03_visuelle-indoktrination-kinder-ns/content/index.revealjs.html) (with [make-pandoc](https://gitlab.com/fkohrt/make-pandoc))
- Reproducible Data Analysis: [2021-11-23](https://fkohrt.gitlab.io/Slides/2021-11-23_reproducible-data-analysis/v1/content/index.beamer.pdf), [2022-01-27](https://fkohrt.gitlab.io/Slides/2021-11-23_reproducible-data-analysis/v2/content/index.beamer.pdf) (with [make-pandoc](https://gitlab.com/fkohrt/make-pandoc))

## Tools

### Create presentations with R

- package: [xaringan](https://github.com/yihui/xaringan)
  - [RStudio Cloud](https://rstudio.cloud/) workflow:
    - download zipped [yihui/xaringan](https://github.com/yihui/xaringan), upload it to project
    - trying to install with `install.packages("xaringan-master", repos = NULL, type="source")` fails
    - install missing dependencies (`htmltools`, `knitr`, `servr`, `xfun` and `rmarkdown`) with `install.packages("<package>")`
    - retry install of yihui/xaringan (see above)
    - attempt to open new R Markdown file
    - install outdated / missing dependencies `caTools` and `bitops` (see above)
  - export to pdf with [DeckTape](https://github.com/astefanutti/decktape) after download
  - opinion: not beginner friendly, missing many features of established WYSIWYG programs, no support for Markdown tables
- package: [revealjs](https://bookdown.org/yihui/rmarkdown/revealjs.html)

### Create presentations with Slides.com

- [Slides.com](https://slides.com/) (uses [reveal.js](https://github.com/hakimel/reveal.js))
- export to pdf with [DeckTape](https://github.com/astefanutti/decktape) using [Docker](https://www.docker.com/) as explained [here](https://hub.docker.com/r/astefanutti/decktape/):
  - `docker run astefanutti/decktape --screenshots --screenshots-directory . --screenshots-format jpg https://slides.com/fkohrt/cyberpsychology/live?fragments=true slides.pdf`
  - `docker ps -lq`
  - `docker cp <output above>:slides .`
  - prepend zero for single digit slides by hand
  - `convert slides/*.jpg Cyberpsychology.pdf` using [ImageMagick](https://www.imagemagick.org/)
  - `docker stop $(docker ps -a -q)` stop...
  - `docker rm $(docker ps -a -q)` ...and remove all Docker containers
- opinion: easy to use, embed any website, no free export option

### Create presentations with prez

- [prez](https://github.com/byteclubfr/prez) (uses [reveal.js](https://github.com/hakimel/reveal.js))
- workflow
  - `prez --init`
  - `prez --serve --watch --no-open-browser`
  - `prez --title=<title> --author=<author> --build`
- export with Chrome as explained [here](https://github.com/hakimel/reveal.js#pdf-export)
  - wíthout notes, without fragments: `?print-pdf&showNotes=false&controls=false&controlsTutorial=false&progress=false&slideNumber=true&pdfSeparateFragments=false&fragments=false&preloadIframes=true&viewDistance=1000`
  - with notes, without fragments: `?print-pdf&showNotes=separate-page&controls=false&controlsTutorial=false&progress=false&slideNumber=true&pdfSeparateFragments=false&fragments=false&preloadIframes=true&viewDistance=1000`
  - with notes, with fragments: `?print-pdf&showNotes=separate-page&controls=false&controlsTutorial=false&progress=false&slideNumber=true&fragments=true&preloadIframes=true&viewDistance=1000`
  - without notes, with fragments: `?print-pdf&showNotes=false&controls=false&controlsTutorial=false&progress=false&slideNumber=true&fragments=true&preloadIframes=true&viewDistance=1000`
- features
  - Navigate forward with <kbd>Space</kbd> or <kbd>N</kbd> (for **n**ext)
    - The arrow keys <kbd>→</kbd> and <kbd>↓</kbd> are specifically for navigating through [vertical slides](https://revealjs.com/vertical-slides/).
  - Navigate backward with <kbd>Shift</kbd> + <kbd>Space</kbd> or <kbd>P</kbd> (for **p**revious)
    - The arrow keys <kbd>←</kbd> and <kbd>↑</kbd> are specifically for navigating through [vertical slides](https://revealjs.com/vertical-slides/).
  - Press <kbd>F</kbd> to switch to **f**ullscreen
  - Press <kbd>O</kbd> or <kbd>Esc</kbd> to show the slide **o**verview
  - Press <kbd>B</kbd> or <kbd>.</kbd> to pause (**b**lacken) the screen
  - Press <kbd>G</kbd> to **g**o to any slide by number
  - Press <kbd>S</kbd> to open the **s**peaker notes (if [speaker notes plugin](https://github.com/hakimel/reveal.js/tree/master/plugin/notes) enabled)
  - Press <kbd>M</kbd> to toggle a side **m**enu, possibly including a table of contents, a theme chooser and a transition chooser (if [menu plugin](https://github.com/denehyg/reveal.js-menu) enabled)
  - Press <kbd>Ctrl</kbd> + click on the slide to zoom in (if [zoom plugin](https://github.com/hakimel/reveal.js/tree/master/plugin/zoom) enabled)
  - Press <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>F</kbd> to activate search (if [search plugin](https://github.com/hakimel/reveal.js/tree/master/plugin/search) enabled)
  - Press <kbd>?</kbd> to show the keyboard help
  - Press <kbd>Q</kbd> to toggle pointer (if [pointer plugin](https://github.com/burnpiro/reveal-pointer) enabled)
  - Press <kbd>A</kbd> to toggle audio (if [audio slideshow plugin](https://github.com/rajgoel/reveal.js-plugins/tree/master/audio-slideshow) enabled)
  - attach `?theme=<theme>` to choose a different theme
  - reveal.js config can be chosen through the query string: e.g. `controls`, `slideNumber`, `hashOneBasedIndex`, `loop`, `navigationMode`, `shuffle`, `fragments`, `showNotes`, `autoSlide`, `defaultTiming`, `previewLinks`, `transition`, [more here](https://revealjs.com/config/) or inside [`js/reveal.js`](https://github.com/hakimel/reveal.js/blob/master/js/reveal.js)
  - configuring [marked](https://marked.js.org/#/USING_ADVANCED.md#options) is also possible
  - opinion: very powerful, one file for each slide makes it a little bit cumbersome
  - [other plugins](https://revealjs.com/plugins/): [syntax highlighting](https://github.com/hakimel/reveal.js/tree/master/plugin/highlight), [markdown renderer](https://github.com/hakimel/reveal.js/tree/master/plugin/markdown), [math renderer](https://github.com/hakimel/reveal.js/tree/master/plugin/math), [fullscreen slides](https://github.com/rajgoel/reveal.js-plugins/tree/master/fullscreen), [jump to slide](https://github.com/SethosII/reveal.js-jump-plugin), [TOC-Progress](https://github.com/e-gor/Reveal.js-TOC-Progress), [localization/internationalization](https://github.com/Martinomagnifico/reveal.js-internation), [show subset of slides by tag](https://github.com/Martinomagnifico/reveal.js-tagteam), [chalkboard](https://github.com/rajgoel/reveal.js-plugins/tree/master/chalkboard), [simplemenu](https://github.com/martinomagnifico/reveal.js-simplemenu), [attribution](https://github.com/rschmehl/reveal-plugins/tree/main/attribution), [improve accessibility](https://github.com/marcysutton/reveal-a11y), [toolbar](https://github.com/denehyg/reveal.js-toolbar)

### Create presentations with marp

- [Marp.app](https://marp.app/), [Marp CLI](https://github.com/marp-team/marp-cli), [Marpit](https://marpit.marp.app/)
- preview with server: `npx marp --server --allow-local-files --input-dir .`
- convert with: `npx marp --allow-local-files <File.md>`
