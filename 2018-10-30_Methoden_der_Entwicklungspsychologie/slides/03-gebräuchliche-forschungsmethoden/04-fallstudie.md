### Fallstudie

<div class="row">
<div class="column-3 fragment">
    <img alt="Cover des Buches „Der Mann, der seine Frau mit einem Hut verwechselte“ von Oliver Sacks" src="images/300_9783499187803.jpg" />
    <div class="footnote">
        Patient von Oliver Sacks, <a class="rawURL" href="https://www.rowohlt.de/taschenbuch/oliver-sacks-der-mann-der-seine-frau-mit-einem-hut-verwechselte.html">www.rowohlt.de</a>
    </div>
</div>
<div class="column-3 fragment">
    <img alt="Porträt von Phineas Gage" src="images/Phineas_Gage_GageMillerPhoto2010-02-17_Unretouched_Color_Cropped.jpg" />
    <div class="footnote fragment">
        Phineas Gage, <a class="rawURL" href="https://commons.wikimedia.org/wiki/File:Phineas_Gage_GageMillerPhoto2010-02-17_Unretouched_Color_Cropped.jpg?uselang=de">commons.wikimedia.org</a>
    </div>
</div>
<div class="column-3 fragment">
    <img alt="Porträt von Patient HM" src="images/Henry_Gustav_1.jpg" />
    <div class="footnote fragment">
        Patient HM, <a class="rawURL" href="https://en.wikipedia.org/wiki/File:Henry_Gustav_1.jpg">en.wikipedia.org</a>
    </div>
</div>
</div>

Note:
- Patient von Oliver Sacks: visuelle Agnosie
- Phineas Gage: Persönlichkeitsveränderung
- Henry Molaison: Erinnerungsschädigung, kann keine neuen Erinnerungen bilden
- weitere: Patientin DF (visuelle Agnosie), Patientin RV (optische Ataxie), in der Entwicklungspsychologie Wunderkinder

**Fallstudie** (auch: klinische Methode)
- umfasst Interviews, Beobachtungen, Testergebnisse
- Ziel: vollständiges Bild der psychischen Verfassung
- eignet sich für „Seltenes“

**Vor- und Nachteile**
- ⊕ umfassend und tiefgehend
- ⊖ beeinflusst von Neigungen der Wissenschaftler
- ⊖ Übertragbarkeit nicht gewährleistet