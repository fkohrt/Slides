### Kulturvergleich

<img class="fragment" data-fragment-index="1" alt="Farahnaz Ghafor (Mazar-i-Sharif) in „Boy“" src="images/Bacha-Posh_Boy.jpg" style="width:90%;" />
<div class="footnote fragment" data-fragment-index="1">
    Standfoto aus „Boy“ von Yalda Afsah und Ginan Seidl, <a class="rawURL" href="http://ginanseidl.net/boy/">ginanseidl.net/boy/</a>
</div>

Note:
**Kulturvergleich** (auch: Ethnografie)
  - (teilnehmende) Beobachtung, Selbstbeurteilung, Interpretation

**Beispiel**
- Bacha Posh in Afghanistan
- Mädchen übernimmt für einige Zeit Rechte und Pflichten eines Jungen, damit Familie auch ohne männliches Mitglied weiter funktionieren kann

**Nachteile**
- ⊖ Forscher verändern Situation der Untersuchten
- ⊖ eigene Vorstellungen beeinflussen Interpretation
- ⊖ Verallgemeinerung problematisch