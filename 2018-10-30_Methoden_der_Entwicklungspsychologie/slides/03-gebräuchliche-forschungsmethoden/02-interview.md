### Klinisches Interview

<blockquote class="fragment" data-fragment-index="1">_Wo kommt der Traum her?_ – **Ich glaube, du schläfst so tief, dass du träumst.** – _Kommt der Traum aus uns selbst oder von außen?_ – **Von außen.** – _Womit träumen wir?_ – **Ich weiß nicht.** – **Mit den Händen?... Mit gar nichts?** – **Ja, mit gar nichts.** – _Wenn du im Bett liegst und träumst, wo ist dann der Traum?_ – **In meinem Bett, unter der Decke. Ich bin mir nicht sicher. Wenn er in meinem Bauch wäre, dann wären ja die Knochen im Weg und ich könnte ihn nicht sehen.** – _Ist der Traum da, wenn du schläfst?_ – **Ja, er ist neben mir in Bett.** (Piaget, 1926/1930, S. 97 f.)</blockquote>

<div class="footnote fragment" data-fragment-index="1">
    Text: Berk & Schönpflug, 2011, S. 35
</div>

Note:
**Vor- und Nachteile**
- ⊕ Gedanken ausdrücken wie im echten Leben
- ⊕ viele Informationen in kurzer Zeit
- ⊖ Genauigkeit leidet
  - Erfindungen
  - Lücken
  - mangelnde verbale Ausdrucksfähigkeit
- ⊖ hohe Variabilität der Frageformulierungen