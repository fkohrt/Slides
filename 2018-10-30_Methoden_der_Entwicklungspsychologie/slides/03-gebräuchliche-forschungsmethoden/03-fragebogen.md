### Strukturiertes Interview

<img src="images/PSCA-D_item2.png" style="width:80%;" />
<div class="footnote">
    Pictorial Scale of Perceived Competence and Social Acceptance for Young Children – deutsche Fassung (PSCA-D) von Asendorpf, J.B. & Aken, M.A.G.v. lizenziert unter <a class="license" href="https://creativecommons.org/licenses/by-nc-nd/3.0/deed.de">CC BY-NC-ND 3.0</a>, <a class="rawURL" href="https://www.testarchiv.eu/retrieval/PSYNDEXTests.php?id=9002593">www.testarchiv.eu</a>
</div>

Note:
- links: „Dieses Mädchen hat nicht viele Freundinnen zum Spielen“
- rechts: „Dieses Mädchen hat viele Freundinnen zum Spielen“
- „Welches Mädchen ist so wie Du? Also: Welches Mädchen ist Dir am ähnlichsten?“
- „Hast Du:
  - sehr viele Freundinnen oder
  - ziemlich viele zum Spielen“
- „Hast Du:
  - ein paar oder
  - fast gar keine Freundinnen zum Spielen“

**Frage**: Was sind die Vor- und Nachteile eines Fragebogens?
- ⊕ effizient
- ⊕ man kann Antwortmöglichkeiten ins Gedächtnis rufen, an die sonst vielleicht nicht gedacht würde
- ⊕ leicht vergleichbar
- ⊖ fehlende Vielfalt