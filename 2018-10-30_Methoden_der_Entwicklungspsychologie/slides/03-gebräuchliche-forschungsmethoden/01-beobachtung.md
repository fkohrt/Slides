### Systematische Beobachtung

![Fernglas Five Eyes](images/five-eyes.jpg)

<div class="footnote">
    Bild: Eric Lobbecke, <a class="rawURL" href="http://natoassociation.ca/inside-the-global-signals-intelligence-apparatus-an-overview-of-the-five-eyes-alliance/">natoassociation.ca</a>
</div>

Note:
**natürliche / teilnehmende Beobachtung** (in natürlicher Umwelt)
- ⊕ hohe externe Validität
- ⊖ variierende Gelegenheiten, Verhalten zu zeigen
- ⊖ Konfundierung mit SV leicht möglich

**strukturierte Beobachtung** (im Labor)
- ⊕ durch mehr Kontrolle gleiche Möglichkeiten, Verhalten zu zeigen
- ⊖ Beobachtungen evtl. untypisch für Alltagsleben

bei allen Beobachtungen: keine Kausalschlüsse möglich!