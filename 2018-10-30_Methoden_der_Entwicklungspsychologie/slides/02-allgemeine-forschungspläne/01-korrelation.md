### Korrelationsmethode

<div class="text-img-left">
    <ul class="img-annotation">
        <li class="fragment" data-fragment-index="2">Mögliche Ursachen
            <ul>
                <li class="fragment" data-fragment-index="2">A ⇒ B</li>
                <li class="fragment" data-fragment-index="2">A ⇐ B</li>
                <li class="fragment" data-fragment-index="2">C ⇒ (A ∧ B)</li>
                <li class="fragment">A ↔ B (z. B. Räuber-Beute-Beziehung)</li>
                <li class="fragment">A ⇒ C ⇒ B</li>
                <li class="fragment">Zufall (Geburtstagsparadoxon)</li>
            </ul></li>
        <li class="fragment">Relevanz<ul>
            <li>maschinelles Lernen</li>
            <li>Privatleben</li>
            </ul></li>
    </ul>
</div>
<div class="text-img-right fragment" data-fragment-index="1">
    <img alt="Correlation between Countries' Annual Per Capita Chocolate Consumption and the Number of Nobel Laureates per 10 Million Population." src="images/nejmon1211064_f1.jpeg" style="width:90%;" />
    <div class="footnote">
    Jährlicher Pro-Kopf-Schokoladenkonsum je Land vs. Zahl der Nobelpreisträger pro 10 Millionen Einwohner je Land (Messerli, 2012)
    </div>
</div>

Note:
**Korrelationsmethode**
- Untersuchung von Zusammenhängen zwischen erhobenen Daten

**mögliche Ursachen für Korrelation**
- drei wichtige: beide Richtungen und Drittvariable
- außerdem gegenseitige Beeinflussung: auf mehr Beute folgen mehr Räuber, darauf folgen weniger Beute und weniger Räuber
- indirekte Beeinflussung
- sowie Zufall: wenn man genügend Variablen betrachtet, werden irgendwelche schon korrelieren

**correlation does not imply causation**
- klar, als Wissenschaftler denkt man dran
- aber: einer künstlichen Intelligenz reicht schon die Korrelation
- auch im Privatleben kann man es leicht vergessen