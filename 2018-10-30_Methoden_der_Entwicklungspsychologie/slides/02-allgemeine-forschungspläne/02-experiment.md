### Experimentelle Methode

<div class="text-img-left fragment">
    <img alt="Sample photographs from videotape recordings of 2- to 3-week-old infants imitating (a) tongue protrusion, (b) mouth opening, and (c) lip protrusion demonstrated by an adult experimenter." src="images/Meltzoff1977_gestures.jpg" />
    <div class="footnote">
    links: _tongue protrusion_ (TP)<br>
    mittig: _mouth opening_ (MO)<br>
    rechts: _lip protrusion_ (LP)<br>
    nicht sichtbar: _sequential finger movement_ (SFM)
    </div>
</div>
<div class="text-img-right fragment">
    <img alt="Distribution of “yes” judgements as a function of the gesture shown to the infant during experiment 1." src="images/Meltzoff1977_results.png" />
    <div class="footnote">
    Verteilung von „Ja“-Urteilen in Abhängigkeit der gezeigten Geste (Meltzoff & Moore, 1977);<br>
    für Zusammenfassung siehe Slater & Quinn, 2012; für Kritik siehe Oostenbroek et al., 2016
    </div>
</div>

Note:
**Experiment**
- Konstrukt wird durch Variable operationalisiert
- Hypothese über Zusammenhang zwischen UV und AV wird überprüft
- dabei Minimierung von Störvariablen

**Beispiel**
- bekanntes Experiment der Entwicklungspsychologie: auch sehr junge Babys können Gesichtsgesten von Erwachsenen imitieren, genannt _active intermodal matching_ (AIM)
- in Grafik rechts: Studenten bewerten, welche Geste das Baby darstellt
- entgegen Jean Piagets Beobachtungen, dass dies erst im Alter von 8 bis 10 Monaten gehe
- geht einer Reihe von neuen Konzepten voran, darunter
  - Spiegelneurone (sind sowohl beim Beobachten als auch beim Ausführen einer Handlung aktiv)
  - angeborener Präferenz für Gesichter
  - früher sozialer Bewertung sowie
  - früher Erkennung von Intentionen
- AIM nicht unumstritten, wie neuere Artikel zeigen

**Frage**: Was ist hier Konstrukt, UV und AV?
- Konstrukt: Fähigkeit zum Spiegeln
- UV: dargebotene Geste
- AV: Anzahl der „Ja“-Urteile pro Gestenkategorie für die Reaktions-Geste auf dargebotene Geste