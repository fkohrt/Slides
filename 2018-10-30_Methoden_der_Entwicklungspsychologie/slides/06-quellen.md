## Quellen
<ul style="font-size:50%;">
    <li>Messerli, F. H. (2012). _Chocolate Consumption, Cognitive Function, and Nobel Laureates._ New England Journal of Medicine, 367(16), 1562–1564. DOI: <a class="rawURL" href="https://doi.org/10.1056/nejmon1211064">10.1056/nejmon1211064</a></li>
    <li>Meltzoff, A., & Moore, M. (1977). _Imitation of facial and manual gestures by human neonates._ Science, 198(4312), 74–78. DOI: <a class="rawURL" href="https://doi.org/10.1126/science.897687">10.1126/science.897687</a></li>
    <li>Slater, A. & Quinn, P. (2012). _Developmental psychology: revisiting the classic studies._ London Thousand Oaks, Calif: SAGE. ISBN: 978-0-85702-758-0</li>
    <li>Oostenbroek, J., Suddendorf, T., Nielsen, M., Redshaw, J., Kennedy-Costantini, S., Davis, J., … Slaughter, V. (2016). _Comprehensive Longitudinal Study Challenges the Existence of Neonatal Imitation in Humans._ Current Biology, 26(10), 1334–1338. DOI: <a class="rawURL" href="https://doi.org/10.1016/j.cub.2016.03.047">10.1016/j.cub.2016.03.047</a></li>
    <li>Berk, L. & Schönpflug, U. (2011). _Entwicklungspsychologie._ München: Pearson Studium. ISBN: 978-3-86894-049-7</li>
    <li>Hasselhorn, M. (2007). _Handbuch der Entwicklungspsychologie._ Göttingen: Hogrefe. ISBN: 978-3-80171-847-3</li>
</ul>