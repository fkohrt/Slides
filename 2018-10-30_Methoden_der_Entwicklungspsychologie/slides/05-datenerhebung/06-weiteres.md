### Weiteres

Note:
**Beobachtungen in nicht-experimentellen Kontexten**
- gesamte Verhaltenssequenz beobachten
- Stichprobe einer Verhaltenssequenz beobachten
  - Ereignisstichprobe
  - Zeitstichprobe

**Lösen von Datenerhebungsproblemen**
- bessere emotionale Anpassung: Aufwärmphase, Ersatzbindungsobjekte
- wahrgenommenes soziales Gefälle verringern: Sichtbarriere
- Förderung des Kommunikationsverhaltens: gestalten und handeln statt sprechen

**Probleme der Entwicklungspsychologie**
- unterschiedliche Eignung für Verfahren je nach Alter
- bei verschiedenen Verfahren: Messinstrument-Äquivalenz gegeben?