### Paradigma der verzögerten Nachahmung

Note:
- Lernphase: Zeigen einer Modellhandlung
- später Gelegenheit zur Wiederholung der Handlung
- dient der Gedächtnisforschung, erfordert aber bereits motorische Reproduktionskompetenzen