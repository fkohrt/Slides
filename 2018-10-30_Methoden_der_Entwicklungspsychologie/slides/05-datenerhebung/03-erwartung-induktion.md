### Erwartungs-Induktions-Paradigma

Note:
- Lernphase: Stimuli mit fester Relation
- dann Reaktion auf Ausbleiben der Relation
- dient dem Nachweis von Kontingenzwahrnehmung (Antizipation)