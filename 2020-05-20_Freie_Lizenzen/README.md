# RMarkdown Templates

## Casual Writing

Beware! As this document embeds XMP metadata, the metadata fields `title`, `author`, `work_type`, `work_url`, `author_url` and `license` are sent to the Creative Commons XMP API to allow for easy XMP file creation.

### Features

#### Font choices

Fonts have been carefully selected to be both appealing to the eye and having advanced (rendering) features.

- General principles
  - Serif, sans-serif, monospace and math font should fit to each other (see DANTE's [TeXnische Komödie](https://www.dante.de/dtk/bisher-erschienen/) for font choices, or the flowchart [So You Need a Typeface](https://graphicdesign.stackexchange.com/a/526))
  - Fonts should be actively maintained
  - Fonts should be present in OpenType format (very expressive font format, successor of TrueType)
- Regarding microtype
  - a microtype configuration file for the serif font should exist, see [microtype.pdf](http://mirrors.ctan.org/macros/latex/contrib/microtype/microtype.pdf), section 5.7 ([some fonts bring their own cfg files](https://tex.stackexchange.com/a/237220/214609))
  - the improvements gained beyond the generic configuration are very subtle, though
  - as of microtype v2.7d, the adjustment of interword spacing and of kerning only works with pdfTeX
- Math font
  - Math font should support `unicode-math` (for a list of supported fonts, see [The UNICODE-MATH package](https://wspr.io/unicode-math/))
  - Math font should support the OpenType MATH extension (for a list of supported fonts, see [MDN: Fonts with a MATH table](https://developer.mozilla.org/en-US/docs/Mozilla/MathML_Project/Fonts#MATH_fonts))
  - see also [Philippe Goutet's answer](https://tex.stackexchange.com/a/47300/214609) on choosing a math font
  - for an overview, see the [BachoTeX](http://www.gust.org.pl/bachotex?set_language=en) talks by Ulrik Vieth (e. g. [2017](http://www.gust.org.pl/bachotex/2017-pl/presentations/uvieth-1-2017.pdf), [2019](http://www.gust.org.pl/bachotex/2019-pl/presentations/uvieth-1-2019.pdf))


#### Annotation of all options

All non-trivial choices are made transparent in the source document, so you know if it's safe to remove some option. The source document also contains various `@` references, that connect scattered settings that belong together. Removing a single one of the group may affect the functioning of all of them, all references are explained in this README.

#### Automatic download of citation styles and pandoc filters @automatic

Some citation files and necessary pandoc filters are downloaded automatically so you'll always have the newest version.

#### Last compile time

The document includes a text indicating when the document has last been compiled. This allows for easy estimation of its age, especially if printed.

#### Link to current version

The parameter `work_url` defines a URL where the newest version of the document can be obtained. This is to hinder outdated versions from being used long after they become obsolete, especially if printed.

#### Version number

The parameter `version` defines a [semantic version number](https://semver.org/) to make it easy to tell if different output formats have the same content and if two instances of the document share the same content. 

#### License @metadata

The parameter `license` defines a [SPDX identifier](https://spdx.org/licenses/). Setting this will include textual and visual indicators and metadata will be set accordingly. Currently the only supported licenses are Creative Commons 4.0 licenses.

#### Metadata @metadata

Aside from the standard metadata included in the document information dictionary (such as title, author, subject and keywords; see ISO 32000-1), PDFs will contain embedded [XMP metadata](https://wiki.creativecommons.org/wiki/XMP) including the title, author and license information. HTML output will store license information through [RDFa](https://wiki.creativecommons.org/wiki/RDFa).

#### Localization @quotes

PDFs (through the package `csquotes` and metadata field `csquotes`) as well as HTML output (through the Pandoc filter [`pandoc-quotes.lua`](https://github.com/pandoc/lua-filters/tree/master/pandoc-quotes.lua)) will have typewriter quotes `""` turned into typographic quotes `„“`, honoring the language metadata field `lang`.

#### UTF-8 support

PDF creation is done by `LuaLaTeX`, which natively supports UTF-8 file encodings.

#### European typographic conventions

PDF creation uses the KOMA-Script documentclass, which is shipped with european typographic conventions by default. The layout may be changed with the `papersize` metadata field (or, if this has no effect for own documentclasses, providing options to the metadata field `classoption` may work).

#### Citation Typing Ontology

Citations may be enhanced with the [Citation Typing Ontology](https://sparontologies.github.io/cito/current/cito.html) (CiTO) to provide more context on why you are citing a work. The Pandoc filter [`cito.lua`](https://github.com/pandoc/lua-filters/tree/master/cito) converts the enhanced citations into standard citations so pandoc can process them. When choosing the output format `md_document`, the output document will contain a `cito_cites` metadata field grouping the citations by category.

#### Fixed figure locations

LaTeX was made to allow focusing on the content, whithout having to care about the layout. Sometimes, however, you want a images inside PDFs to appear where you inserted them. Therefore, PDF include the `float` package which allows for the chunk setting `fig.pos = "!H"`.

#### Print support @print

When planning for PDF print publication, one may enable the params `show_urls_in_footnotes` (which should be used instead of Pandoc's metadata field `links-as-notes`) and/or `show_url_break_symbols`. This is implemented by the chunks `show_url_break_symbols`, `footnotes_and_symbols`, `only_symbols` and `only_footnotes`.

#### Debugging ready @debug

Turn those swiches and look through all intermediary files.

#### Code listings @code

Code listings are set up with automatic line breaks.

#### Typographic improvements

- `microtype` for extra [Kerning](https://xkcd.com/1015/) (implemented by Pandoc template)
- `booktabs` for nice tables (implemented by Pandoc template)

#### Other features

- Bibliography
- All those other fancy `pandoc`, `knitr`, `rmarkdown` and `bookdown` features. Yummy!

### Customization

- Have a look at:
  - [What packages do people load by default in LaTeX?](https://tex.stackexchange.com/q/553)
  - [Techniques and packages to keep up with good practices](https://tex.stackexchange.com/q/19264/214609)
- other documentclasses:
  - [typewriter](https://www.ctan.org/pkg/typewriter) _Typeset with a randomly variable monospace font_
  - [savetrees](https://www.ctan.org/pkg/savetrees) _Optimise the use of each page of a LaTeX document_

#### Different fonts

##### Garamond + Latin Modern Mono

- recommendations
  - [stone-zeng](https://tex.stackexchange.com/a/467648/214609) (Latin Modern Mono Light)
  - [TeXnician](https://tex.stackexchange.com/a/446826/214609) (Ysabeau, formerly Eau de Garamond)

```yaml
mainfont: EB Garamond # alternative font: Cormorant Garamond (!), Garamond Libre
sansfont: Ysabeau # source: https://github.com/CatharsisFonts/Ysabeau
monofont: Latin Modern Mono Light
mathfont: Garamond Math
mathfontoptions: # unicode-math setting according to ISO 80000-2
  - math-style=ISO
  - bold-style=ISO
  - sans-style=italic
  - nabla=upright
  - partial=upright
```

##### Palatino + Optima + Inconsolata

- recommendations
  - [Henrik Hansen](https://tex.stackexchange.com/a/50812/214609) (Inconsolata)
  - [Alan Gilbertson](https://graphicdesign.stackexchange.com/a/45195) (Optima)
  - [Davislor 1](https://tex.stackexchange.com/a/424518/214609), [Davislor 2](https://tex.stackexchange.com/a/449375/214609) (Optima / URW Classico / Gillius No2)

```yaml
mainfont: TeX Gyre Pagella # TeX Gyre Pagella is a clone of Palatino (as is Asana); microtype configuration file exists; alternative: package newpx
sansfont: URW Classico # URW Classico is a clone of Optima
monofont: InconsolataN
mathfont: TeX Gyre Pagella Math # or Asana Math or Euler Math
mathfontoptions: # unicode-math setting according to ISO 80000-2
  - math-style=ISO
  - bold-style=ISO
  - sans-style=italic
  - nabla=upright
  - partial=upright
```

##### Palatino + Vera

- recommendations
  - [l2tabu.pdf](http://mirrors.ctan.org/info/l2tabu/german/l2tabu.pdf), section 2.3.4
  - [mforbes](https://tex.stackexchange.com/a/114166/214609)
- Serif: [TeX Gyre Pagella](http://www.gust.org.pl/projects/e-foundry/tex-gyre/pagella/index_html) ([download](https://www.ctan.org/pkg/tex-gyre-pagella))
- Sans-Serif: [Bitstream Vera](https://www.gnome.org/fonts/)
- Mono: Vera Mono (siehe Vera)
- Math: [TeX Gyre Pagella Math](http://www.gust.org.pl/projects/e-foundry/tg-math/index_html) ([download](https://www.ctan.org/pkg/tex-gyre-math))

```yaml
mainfont: TeX Gyre Pagella
sansfont: Vera
sansfontoptions:
  - Extension=.ttf
  - UprightFont=*
  - BoldFont=*Bd
  - ItalicFont=*It
  - BoldItalicFont=*BI
  - Scale=0.95
monofont: Vera Mo
monofontoptions:
  - Extension=.ttf
  - UprightFont=*no
  - BoldFont=*Bd
  - ItalicFont=*It
  - BoldItalicFont=*BI
mathfont: TeX Gyre Pagella Math
```

### Internationalization

- metadata fields
  - `date` / `date-meta`
  - `lang`
  - `documentclass`
  - `papersize`
  - `csl`
- heading "Literaturverzeichnis" (footer)
- heading "Über dieses Dokument" (footer)
- text for document version and timestamp (footer)
- text for license info (footer)

### What about that `default.latex` template?

#### hidelinks @template

Adjusted [`default.latex`](https://github.com/jgm/pandoc-templates/blob/45a2075/default.latex) to honor `hidelinks` as metadata field.

#### XMP data

This template uses the package `xmpincl` to embed XMP files into PDF documents. Alternatively, one could use the package `hyperxmp`. To do so

- load it before the `hyperref` package inside `default.latex`: `\usepackage{hyperxmp}`
- don't use KOMA-Script's `twocolumn` classoption (found to be clashing)
- add the `hyperrefoptions` of your choice (see [hyperxmp's package documentation](https://www.ctan.org/pkg/hyperxmp))
- when using `pdflicenseurl` add `md_extensions: -autolink_bare_uris`

### Todo

- All output formats should honor the `mainfont` metadata field.
- HTML themes
- guide to command line compiling
- offline XMP file creation
- [CC0](https://creativecommons.org/choose/zero/) support